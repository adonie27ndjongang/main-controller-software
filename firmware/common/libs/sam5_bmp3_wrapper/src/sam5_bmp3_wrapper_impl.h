/*
* sam5_bmp3_wrapper_impl.h
*
* Created: 27.01.2023 00:01:01
*  Author: Michael Ratzel
*/

#ifndef SAM5_BMP3_WRAPPER_IMPL_H_
#define SAM5_BMP3_WRAPPER_IMPL_H_

#include "sam5_bmp3_wrapper.h"
#include "simple_state_machine.h"

#if BMP3_CALIBRATION == 0
#include "bmp3_calibration_00.h"
#elif BMP3_CALIBRATION == 1
#include "bmp3_calibration_01.h"
#elif BMP3_CALIBRATION == 2
#include "bmp3_calibration_02.h"
#elif BMP3_CALIBRATION == 3
#include "bmp3_calibration_03.h"
#elif BMP3_CALIBRATION == 4
#include "bmp3_calibration_04.h"
#elif BMP3_CALIBRATION == 5
#include "bmp3_calibration_05.h"
#elif BMP3_CALIBRATION == 6
#include "bmp3_calibration_06.h"
#elif BMP3_CALIBRATION == 7
#include "bmp3_calibration_07.h"
#elif BMP3_CALIBRATION == 8
#include "bmp3_calibration_08.h"
#elif BMP3_CALIBRATION == 9
#include "bmp3_calibration_09.h"
#endif

#include <peripheral_clk_config.h>

// SCL PAD PA22
// SDL PAD PA23
// SERCOM5 Unit

#define MULTIPLEXER_I2C_ADDRESS 0x70	///< Multiplexer Address

// Some macros
#define SAM5_BMP3_BUSY_WAIT_US(microseconds) do{					\
	for(uint16_t i = 0; i < microseconds; ++i){						\
		if(sam5_bmp3_readout_status){								\
			break;													\
		}															\
		delay_us(1);												\
	}																\
	if(sam5_bmp3_readout_status != SAM5_BMP3_READOUT_SUCCESS){		\
		sam5_bmp3_readout_status = SAM5_BMP3_READOUT_BUSY;			\
		return BMP3_E_COMM_FAIL;									\
	}																\
	sam5_bmp3_readout_status = SAM5_BMP3_READOUT_BUSY;				\
} while(0)

#if CONF_CPU_FREQUENCY != 100000000 && CONF_CPU_FREQUENCY != 120000000
#warning "SAM5 BMP3 wrapper expects cpu frequency of 100 MHz or 120 MHz to work properly"
#endif

// 200 Hz Output data rate
#if  BMP3_READOUT_FREQUENCY == BMP3_ODR_200_HZ
#define SAM5_BMP3_TC_PRESCALER	0x3							//16
#define SAM5_BMP3_TC_CC0		(CONF_CPU_FREQUENCY / 3200) //200 * 16
// 100 Hz Output data rate
#elif BMP3_READOUT_FREQUENCY == BMP3_ODR_100_HZ
#define SAM5_BMP3_TC_PRESCALER	0x5							//64
#define SAM5_BMP3_TC_CC0		(CONF_CPU_FREQUENCY / 6400)	//100 * 16
// 50 Hz Output data rate
#elif BMP3_READOUT_FREQUENCY == BMP3_ODR_50_HZ
#define SAM5_BMP3_TC_PRESCALER	0x6							//64
#define SAM5_BMP3_TC_CC0		(CONF_CPU_FREQUENCY / 3200) //50 * 64
// 25 Hz Output data rate
#elif BMP3_READOUT_FREQUENCY == BMP3_ODR_25_HZ
#define SAM5_BMP3_TC_PRESCALER	0x6							//256
#define SAM5_BMP3_TC_CC0		(CONF_CPU_FREQUENCY / 6400)	//25 * 256
// 12.5 Hz Output data rate
#elif BMP3_READOUT_FREQUENCY == BMP3_ODR_12_5_HZ
#define SAM5_BMP3_TC_PRESCALER	0x7							//256
#define SAM5_BMP3_TC_CC0		(CONF_CPU_FREQUENCY / 3200) //12.5 * 256
// 6.25 Hz Output data rate
#elif BMP3_READOUT_FREQUENCY == BMP3_ODR_6_25_HZ
#define SAM5_BMP3_TC_PRESCALER	0x7							//1024
#define SAM5_BMP3_TC_CC0		(CONF_CPU_FREQUENCY / 6400)	// 1024 * 6.25
// 3.125 Hz Output data rate
#elif BMP3_READOUT_FREQUENCY == BMP3_ODR_3_1_HZ
#define SAM5_BMP3_TC_PRESCALER	0x7							//1024
#define SAM5_BMP3_TC_CC0		(CONF_CPU_FREQUENCY / 3200)	// 1024 * 3.125
#else
#error "This software does not work with output data rates slower than 3.125 Hz"
#endif

#define LED_UNUSED UINT32_MAX


typedef struct {
	simple_state_machine_t state_machine;
	uint8_t slave_address;
	uint8_t direction;
	uint8_t* transfer_buffer;
	uint8_t processed_bytes;
	uint8_t num_bytes_to_transfer;
	uint8_t multiplexer_channel_number;
	uint8_t finish_data;
	uint8_t default_flag;
	uint8_t use_multiplexer_if_available;
	
	void(*finish_action)(uint8_t);
	
	void(*callback)(uint8_t success);
} bmp3_i2c_driver_t;

void bmp3_i2c_driver_initialize_peripherals();
void bmp3_i2c_driver_initialize_state_machine();

BMP3_INTF_RET_TYPE bmp3_i2c_driver_write_wrapper(uint8_t reg_addr, const uint8_t* reg_data, uint32_t len, void* intf_ptr);
BMP3_INTF_RET_TYPE bmp3_i2c_driver_read_wrapper(uint8_t reg_addr, uint8_t* reg_data, uint32_t len, void* intf_ptr);

BMP3_INTF_RET_TYPE bmp3_i2c_driver_write_async(const uint8_t* reg_data, uint32_t len, bmp3_interface_desc_t* intf, void (*callback)(uint8_t success), uint8_t use_multiplexer_if_available);
BMP3_INTF_RET_TYPE  bmp3_i2c_driver_read_async(uint8_t* reg_data, uint32_t len, bmp3_interface_desc_t* intf, void (*callback)(uint8_t success), uint8_t use_multiplexer_if_available);

void bmp3_i2c_driver_timeout();

#endif /* SAM5_BMP3_WRAPPER_IMPL_H_ */