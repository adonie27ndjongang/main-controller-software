#include "sam5_bmp3_wrapper_impl.h"
#include "hardware_defines.h"
#include "simple_state_machine.h"
#include <string.h>
#include <peripheral_clk_config.h>
#include <stdio.h>

static bmp3_i2c_driver_t g_bmp3_i2c_driver;

#define BMP3_I2C_DRIVER_INSERT_SERCOM_UNIT_(pre, unit, post)	pre ## unit ## post									///< This is necessary for C preprocessor macro expansion
#define BMP3_I2C_DRIVER_INSERT_SERCOM_UNIT(pre, unit, post)	BMP3_I2C_DRIVER_INSERT_SERCOM_UNIT_(pre, unit, post)		///< This is necessary for C preprocessor macro expansion
#define BMP3_I2C_DRIVER_SERCOM BMP3_I2C_DRIVER_INSERT_SERCOM_UNIT(SERCOM, BMP3_I2C_DRIVER_SERCOM_UNIT,)

#define DEBUG_PRINT(...) do{															\
	uint16_t status = hri_sercomi2cm_get_STATUS_reg(BMP3_I2C_DRIVER_SERCOM, 0xFFFF);	\
	printf("%x  ", status);																\
	printf(__VA_ARGS__);																\
}while(0)
#undef DEBUG_PRINT
#define DEBUG_PRINT(...)

/**
* \brief Definition of the events that can be dispatched to the I2C master state machine
*/
enum{
	BMP3_I2C_DRIVER_EVT_SIG_START = EVENT_BASE,	///< The I2C master starts a transmission
	BMP3_I2C_DRIVER_EVT_SIG_SB,					///< The I2C slave has send a byte
	BMP3_I2C_DRIVER_EVT_SIG_ACK,				///< The I2C master has send byte and received an ACK
	BMP3_I2C_DRIVER_EVT_SIG_NACK,				///< The I2C master has send byte and received a NACK
	BMP3_I2C_DRIVER_EVT_SIG_TIMEOUT,			///< The I2C master has detected a timeout
	BMP3_I2C_DRIVER_EVT_SIG_ERROR,				///< The I2C master detected an error
};
enum{
	BMP3_I2C_DRIVER_DIR_MASTER_TO_SLAVE = 0,		///< Must be 0 as this is the I2C direction value for Master to Slave
	BMP3_I2C_DRIVER_DIR_SLAVE_TO_MASTER = 1,		///< Must be 1 as this is the I2C direction value for Slave to Master
	BMP3_I2C_DRIVER_DIR_INACTIVE = 2,
};


#define BMP3_I2C_DRIVER_MB_HANDLER			BMP3_I2C_DRIVER_INSERT_SERCOM_UNIT(SERCOM, BMP3_I2C_DRIVER_SERCOM_UNIT, _0_Handler)
#define BMP3_I2C_DRIVER_MB_IRQn				BMP3_I2C_DRIVER_INSERT_SERCOM_UNIT(SERCOM, BMP3_I2C_DRIVER_SERCOM_UNIT, _0_IRQn)
#define BMP3_I2C_DRIVER_SB_HANDLER			BMP3_I2C_DRIVER_INSERT_SERCOM_UNIT(SERCOM, BMP3_I2C_DRIVER_SERCOM_UNIT, _1_Handler)
#define BMP3_I2C_DRIVER_SB_IRQn				BMP3_I2C_DRIVER_INSERT_SERCOM_UNIT(SERCOM, BMP3_I2C_DRIVER_SERCOM_UNIT, _1_IRQn)
#define BMP3_I2C_DRIVER_ERROR_HANDLER		BMP3_I2C_DRIVER_INSERT_SERCOM_UNIT(SERCOM, BMP3_I2C_DRIVER_SERCOM_UNIT, _3_Handler)
#define BMP3_I2C_DRIVER_ERROR_IRQn			BMP3_I2C_DRIVER_INSERT_SERCOM_UNIT(SERCOM, BMP3_I2C_DRIVER_SERCOM_UNIT, _3_IRQn)

static uint8_t last_transmit_was_success = 0;

static void bmp3_i2c_driver_state_idle(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void * user_pointer);
#if BMP3_MULTIPLEXER_AVAILABLE
static void bmp3_i2c_driver_state_multiplexer_addressed(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void * user_pointer);
static void bmp3_i2c_driver_state_multiplexer_set(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void * user_pointer);
#endif // BMP3_MULTIPLEXER_AVAILABLE
static void bmp3_i2c_driver_state_sending(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void * user_pointer);
static void bmp3_i2c_driver_state_receiving(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void * user_pointer);


static void bmp3_i2c_driver_finish_action_command(uint8_t command);
static void bmp3_i2c_driver_finish_action_command_ack(uint8_t command);
static void bmp3_i2c_driver_finish_action_command_nack(uint8_t command);
static void bmp3_i2c_driver_finish_action_data(uint8_t data);
static void bmp3_i2c_driver_finish_action_address(uint8_t data);
static void bmp3_i2c_driver_register_finish_action(void(*volatile finish_action)(uint8_t data), uint8_t data);
static void bmp3_i2c_driver_execute_finish_action();

static void bmp3_i2c_driver_transmission_success();
static void bmp3_i2c_driver_transmission_nosuccess();

#if BMP3_MULTIPLEXER_AVAILABLE
static void bmp3_i2c_driver_reset_multiplexer_if_possible();
#endif

void bmp3_i2c_driver_initialize_peripherals(void)
{
	#if BMP3_MULTIPLEXER_AVAILABLE
	///* Reset the multiplexer
	bmp3_i2c_driver_reset_multiplexer_if_possible();
	#endif
	
	///* Initialize pins
	///* Deactivate GPIO_PULL_UP and GPIO_PULL_DOWN on the SCL pin
	gpio_set_pin_pull_mode(BMP3_I2C_DRIVER_SCL_PIN, GPIO_PULL_OFF);
	///* Deactivate GPIO_PULL_UP and GPIO_PULL_DOWN on the SDA pin
	gpio_set_pin_pull_mode(BMP3_I2C_DRIVER_SDA_PIN, GPIO_PULL_OFF);
	///* Set function of BMP3_I2C_DRIVER_SCL_PIN to BMP3_I2C_DRIVER_SCL_PIN_FUNCTION
	gpio_set_pin_function(BMP3_I2C_DRIVER_SCL_PIN, BMP3_I2C_DRIVER_SCL_PIN_FUNCTION);
	///* Set function of BMP3_I2C_DRIVER_SDA_PIN to BMP3_I2C_DRIVER_SDA_PIN_FUNCTION
	gpio_set_pin_function(BMP3_I2C_DRIVER_SDA_PIN, BMP3_I2C_DRIVER_SDA_PIN_FUNCTION);
	
	///* Clock settings
	uint8_t core_clock_id;
	uint8_t slow_clock_id;
	switch(BMP3_I2C_DRIVER_SERCOM_UNIT){
		case 0:
		hri_mclk_set_APBAMASK_SERCOM0_bit(MCLK);
		core_clock_id = SERCOM0_GCLK_ID_CORE;
		slow_clock_id = SERCOM0_GCLK_ID_SLOW;
		break;
		case 1:
		hri_mclk_set_APBAMASK_SERCOM1_bit(MCLK);
		core_clock_id = SERCOM1_GCLK_ID_CORE;
		slow_clock_id = SERCOM1_GCLK_ID_SLOW;
		break;
		case 2:
		hri_mclk_set_APBBMASK_SERCOM2_bit(MCLK);
		core_clock_id = SERCOM2_GCLK_ID_CORE;
		slow_clock_id = SERCOM2_GCLK_ID_SLOW;
		break;
		case 3:
		hri_mclk_set_APBBMASK_SERCOM3_bit(MCLK);
		core_clock_id = SERCOM3_GCLK_ID_CORE;
		slow_clock_id = SERCOM3_GCLK_ID_SLOW;
		break;
		case 4:
		hri_mclk_set_APBDMASK_SERCOM4_bit(MCLK);
		core_clock_id = SERCOM4_GCLK_ID_CORE;
		slow_clock_id = SERCOM4_GCLK_ID_SLOW;
		break;
		case 5:
		hri_mclk_set_APBDMASK_SERCOM5_bit(MCLK);
		core_clock_id = SERCOM5_GCLK_ID_CORE;
		slow_clock_id = SERCOM5_GCLK_ID_SLOW;
		break;
		#ifdef SERCOM6
		case 6:
		hri_mclk_set_APBDMASK_SERCOM6_bit(MCLK);
		core_clock_id = SERCOM6_GCLK_ID_CORE;
		slow_clock_id = SERCOM6_GCLK_ID_SLOW;
		break;
		#endif
		#ifdef SERCOM7
		case 7:
		hri_mclk_set_APBDMASK_SERCOM7_bit(MCLK);
		core_clock_id = SERCOM7_GCLK_ID_CORE;
		slow_clock_id = SERCOM7_GCLK_ID_SLOW;
		break;
		#endif
	}
	//* Set GCLKs
	hri_gclk_write_PCHCTRL_reg(GCLK, core_clock_id, GCLK_PCHCTRL_GEN_GCLK0_Val | (1 << GCLK_PCHCTRL_CHEN_Pos));
	hri_gclk_write_PCHCTRL_reg(GCLK, slow_clock_id, BMP3_I2C_DRIVER_GLCK_SLOW_GEN | (1 << GCLK_PCHCTRL_CHEN_Pos));
	
	///* Initialize BMP3_I2C_DRIVER_SERCOM as I2C master
	///* Check if no software reset of BMP3_I2C_DRIVER_SERCOM is in progress
	if (!hri_sercomi2cm_is_syncing(BMP3_I2C_DRIVER_SERCOM, SERCOM_I2CM_SYNCBUSY_SWRST))
	{
		///* Perform a software reset of BMP3_I2C_DRIVER_SERCOM
		hri_sercomi2cm_write_CTRLA_reg(BMP3_I2C_DRIVER_SERCOM, SERCOM_I2CM_CTRLA_SWRST);
	}
	///* Wait until the software reset is finished
	hri_sercomi2cm_wait_for_sync(BMP3_I2C_DRIVER_SERCOM, SERCOM_I2CM_SYNCBUSY_SWRST);
	
	hri_sercomi2cm_write_CTRLA_reg(BMP3_I2C_DRIVER_SERCOM,
	0 << SERCOM_I2CM_CTRLA_LOWTOUTEN_Pos    // Time-out disabled
	| 0x3 << SERCOM_I2CM_CTRLA_INACTOUT_Pos	// Inactive time-out set for 20-21�s for 1 MHz speed //TODO
	| 0 << SERCOM_I2CM_CTRLA_SCLSM_Pos      // SCL stretch before ACK bit for software interaction
	| 0x0 << SERCOM_I2CM_CTRLA_SPEED_Pos    // Fast-mode (Fm) up to 400 kHz
	| 0 << SERCOM_I2CM_CTRLA_SEXTTOEN_Pos   // Cumulative time-out disabled
	| 0 << SERCOM_I2CM_CTRLA_MEXTTOEN_Pos	// Host SCL Low Extended Time-Out disabled
	| 0x2 << SERCOM_I2CM_CTRLA_SDAHOLD_Pos  // For sending a start SDA is hold low for 300-600 ns before SCL gets pulled low
	| 0 << SERCOM_I2CM_CTRLA_PINOUT_Pos     // 4-wire operation disabled
	| 0 << SERCOM_I2CM_CTRLA_RUNSTDBY_Pos   // Run in standby disabled, all reception is dropped
	| 0x5 << SERCOM_I2CM_CTRLA_MODE_Pos		// Set SERCOM mode to I2C master
	| 0 );
	hri_sercomi2cm_write_CTRLB_reg(BMP3_I2C_DRIVER_SERCOM,
	0 << SERCOM_I2CM_CTRLB_QCEN_Pos			// Quick command mode is enabled -> direct sw interaction after ADDR ack from slave
	| 0 << SERCOM_I2CM_CTRLB_SMEN_Pos       // Smart mode is disabled, data is not acknowledged automatically when DATA.DATA is read
	| 0 );
	hri_sercomi2cm_write_CTRLC_reg(BMP3_I2C_DRIVER_SERCOM,
	0 << SERCOM_I2CM_CTRLC_DATA32B_Pos      // Data transaction to/from DATA are 8-bit in size
	| 0 );
	
	#define BAUD_RATE 100000U
	uint64_t baud_total = (((CONF_CPU_FREQUENCY - (BAUD_RATE * 10U) - (BMP3_I2C_DRIVER_TRISE * (BAUD_RATE / 100U) * (CONF_CPU_FREQUENCY / 10000U) / 1000U)) * 10U + 5 * BAUD_RATE) / (BAUD_RATE * 10U));
	uint32_t baud_reg = (baud_total + 1) / 2;
	
	hri_sercomi2cm_write_BAUD_reg(BMP3_I2C_DRIVER_SERCOM,
	baud_reg << SERCOM_I2CM_BAUD_BAUD_Pos
	| baud_reg << SERCOM_I2CM_BAUD_BAUDLOW_Pos
	| 0 );
	hri_sercomi2cm_write_INTEN_reg(BMP3_I2C_DRIVER_SERCOM,
	1 << SERCOM_I2CM_INTENSET_ERROR_Pos     // Error interrupt is enabled.
	| 1 << SERCOM_I2CM_INTENSET_MB_Pos		// The Master on Bus interrupt is enabled.
	| 1 << SERCOM_I2CM_INTENSET_SB_Pos		// The Slave on Bus interrupt is enabled.
	| 0 );
	hri_sercomi2cm_write_ADDR_reg(BMP3_I2C_DRIVER_SERCOM,
	0 << SERCOM_I2CM_ADDR_TENBITEN_Pos		// Disable 10-bit addressing
	| 0 );

	NVIC_SetPriority(BMP3_I2C_DRIVER_MB_IRQn, NVIC_PRIORITY_BMP3_SERCOM);
	NVIC_SetPriority(BMP3_I2C_DRIVER_SB_IRQn, NVIC_PRIORITY_BMP3_SERCOM);
	NVIC_SetPriority(BMP3_I2C_DRIVER_ERROR_IRQn, NVIC_PRIORITY_BMP3_SERCOM);
	
	///* Enable MB interrupt
	NVIC_DisableIRQ(BMP3_I2C_DRIVER_MB_IRQn);
	NVIC_ClearPendingIRQ(BMP3_I2C_DRIVER_MB_IRQn);
	NVIC_EnableIRQ(BMP3_I2C_DRIVER_MB_IRQn);
	///* Enable SB interrupt
	NVIC_DisableIRQ(BMP3_I2C_DRIVER_SB_IRQn);
	NVIC_ClearPendingIRQ(BMP3_I2C_DRIVER_SB_IRQn);
	NVIC_EnableIRQ(BMP3_I2C_DRIVER_SB_IRQn);
	///* Enable ERROR interrupt
	NVIC_DisableIRQ(BMP3_I2C_DRIVER_ERROR_IRQn);
	NVIC_ClearPendingIRQ(BMP3_I2C_DRIVER_ERROR_IRQn);
	NVIC_EnableIRQ(BMP3_I2C_DRIVER_ERROR_IRQn);
	
	hri_sercomi2cm_set_CTRLA_ENABLE_bit(BMP3_I2C_DRIVER_SERCOM);
}

void bmp3_i2c_driver_initialize_state_machine(){
	simpleStateMachineInit_(&g_bmp3_i2c_driver.state_machine, bmp3_i2c_driver_state_idle, NULL);
}

static BMP3_INTF_RET_TYPE bmp3_i2c_driver_wait_for_transmission_finish(){
	for(uint16_t i = 0; i < 500; ++i){
		if(g_bmp3_i2c_driver.direction == BMP3_I2C_DRIVER_DIR_INACTIVE){
			return last_transmit_was_success ? BMP3_INTF_RET_SUCCESS : BMP3_E_COMM_FAIL;
		}
		delay_us(1);
	}
	DEBUG_PRINT("SYNC ");
	bmp3_i2c_driver_timeout();
	return BMP3_E_COMM_FAIL;
}

BMP3_INTF_RET_TYPE bmp3_i2c_driver_write_wrapper(uint8_t reg_addr, const uint8_t* reg_data, uint32_t len, void* intf_ptr){
	uint8_t buffer[255];
	buffer[0] = reg_addr;
	memcpy(&buffer[1], reg_data, len);
	BMP3_INTF_RET_TYPE ret = bmp3_i2c_driver_write_async(buffer, len + 1, (bmp3_interface_desc_t*) intf_ptr, NULL, 1);
	if(ret != BMP3_INTF_RET_SUCCESS){
		return ret;
	}
	return bmp3_i2c_driver_wait_for_transmission_finish();
}

BMP3_INTF_RET_TYPE bmp3_i2c_driver_read_wrapper(uint8_t reg_addr, uint8_t* reg_data, uint32_t len, void* intf_ptr){
	BMP3_INTF_RET_TYPE ret = bmp3_i2c_driver_write_async(&reg_addr, 1, (bmp3_interface_desc_t*) intf_ptr, NULL, 1);
	if(ret != BMP3_INTF_RET_SUCCESS){
		return ret;
	}
	ret = bmp3_i2c_driver_wait_for_transmission_finish();
	if(ret != BMP3_INTF_RET_SUCCESS){
		return ret;
	}	
	ret = bmp3_i2c_driver_read_async(reg_data, len, (bmp3_interface_desc_t*) intf_ptr, NULL, 0);
	if(ret != BMP3_INTF_RET_SUCCESS){
		return ret;
	}
	return bmp3_i2c_driver_wait_for_transmission_finish();
}

BMP3_INTF_RET_TYPE bmp3_i2c_driver_write_async(const uint8_t* reg_data, uint32_t len, bmp3_interface_desc_t* intf, void (*callback)(uint8_t success), uint8_t use_multiplexer_if_available){
	CRITICAL_SECTION_ENTER()
	if(g_bmp3_i2c_driver.direction != BMP3_I2C_DRIVER_DIR_INACTIVE){
		atomic_leave_critical(&__atomic);
		return BMP3_E_COMM_FAIL;
	}
	g_bmp3_i2c_driver.slave_address = intf->bmp3_i2c_address;
	g_bmp3_i2c_driver.multiplexer_channel_number = intf->multiplexer_channel_number;
	g_bmp3_i2c_driver.direction = BMP3_I2C_DRIVER_DIR_MASTER_TO_SLAVE;
	g_bmp3_i2c_driver.transfer_buffer = (uint8_t *) reg_data;
	g_bmp3_i2c_driver.num_bytes_to_transfer = len;
	g_bmp3_i2c_driver.callback = callback;
	g_bmp3_i2c_driver.use_multiplexer_if_available = use_multiplexer_if_available;
	const simple_state_machine_event_t event = {
		.signal_ = BMP3_I2C_DRIVER_EVT_SIG_START
	};
	simpleStateMachineDispatch_(&g_bmp3_i2c_driver.state_machine, &event, NULL);
	CRITICAL_SECTION_LEAVE()
	bmp3_i2c_driver_execute_finish_action();
	return BMP3_INTF_RET_SUCCESS;
}
BMP3_INTF_RET_TYPE  bmp3_i2c_driver_read_async(uint8_t* reg_data, uint32_t len, bmp3_interface_desc_t* intf, void (*callback)(uint8_t success), uint8_t use_multiplexer_if_available){
	CRITICAL_SECTION_ENTER()
	if(g_bmp3_i2c_driver.direction != BMP3_I2C_DRIVER_DIR_INACTIVE){
		atomic_leave_critical(&__atomic);
		return BMP3_E_COMM_FAIL;
	}
	g_bmp3_i2c_driver.slave_address = intf->bmp3_i2c_address;
	g_bmp3_i2c_driver.multiplexer_channel_number = intf->multiplexer_channel_number;
	g_bmp3_i2c_driver.direction = BMP3_I2C_DRIVER_DIR_SLAVE_TO_MASTER;
	g_bmp3_i2c_driver.transfer_buffer = reg_data;
	g_bmp3_i2c_driver.num_bytes_to_transfer = len;
	g_bmp3_i2c_driver.callback = callback;
	g_bmp3_i2c_driver.use_multiplexer_if_available = use_multiplexer_if_available;
	const simple_state_machine_event_t event = {
		.signal_ = BMP3_I2C_DRIVER_EVT_SIG_START
	};
	simpleStateMachineDispatch_(&g_bmp3_i2c_driver.state_machine, &event, NULL);
	CRITICAL_SECTION_LEAVE()
	bmp3_i2c_driver_execute_finish_action();
	return BMP3_INTF_RET_SUCCESS;
}

void bmp3_i2c_driver_timeout(){
	const simple_state_machine_event_t this_event = {
		.signal_ = BMP3_I2C_DRIVER_EVT_SIG_TIMEOUT,
	};
	DEBUG_PRINT("TIME_OUT\r\n");
	simpleStateMachineDispatch_(&g_bmp3_i2c_driver.state_machine, &this_event, NULL);
}

void BMP3_I2C_DRIVER_MB_HANDLER(void)
{
	simple_state_machine_event_t this_event;
	uint32_t reg = hri_sercomi2cm_get_STATUS_reg(BMP3_I2C_DRIVER_SERCOM, SERCOM_I2CM_STATUS_BUSERR | SERCOM_I2CM_STATUS_ARBLOST);
	if(reg){
		this_event.signal_ = BMP3_I2C_DRIVER_EVT_SIG_ERROR;
		DEBUG_PRINT("MB-Error %ld\r\n", reg);
	}
	else if(hri_sercomi2cm_get_STATUS_RXNACK_bit(BMP3_I2C_DRIVER_SERCOM))
	{
		// Received NACK
		this_event.signal_ = BMP3_I2C_DRIVER_EVT_SIG_NACK;
		DEBUG_PRINT("MB-NACK\r\n");
	}
	else
	{
		// Received ACK
		this_event.signal_ = BMP3_I2C_DRIVER_EVT_SIG_ACK;
		DEBUG_PRINT("MB-ACK\r\n");
	}
	g_bmp3_i2c_driver.default_flag = SERCOM_I2CM_INTFLAG_MB;
	simpleStateMachineDispatch_(&g_bmp3_i2c_driver.state_machine, &this_event, NULL);
	bmp3_i2c_driver_execute_finish_action();
}

void BMP3_I2C_DRIVER_SB_HANDLER(void)
{
	simple_state_machine_event_t this_event = {
		.signal_ = BMP3_I2C_DRIVER_EVT_SIG_SB
	};
	DEBUG_PRINT("SB\r\n");
	g_bmp3_i2c_driver.default_flag = SERCOM_I2CM_INTFLAG_SB;
	simpleStateMachineDispatch_(&g_bmp3_i2c_driver.state_machine, &this_event, NULL);
	bmp3_i2c_driver_execute_finish_action();
}

void BMP3_I2C_DRIVER_ERROR_HANDLER(void)
{
	simple_state_machine_event_t this_event =
	{
		.signal_ = BMP3_I2C_DRIVER_EVT_SIG_ERROR
	};
	DEBUG_PRINT("ERROR\r\n");
	g_bmp3_i2c_driver.default_flag = SERCOM_I2CM_INTFLAG_ERROR;
	simpleStateMachineDispatch_(&g_bmp3_i2c_driver.state_machine, &this_event, NULL);
	bmp3_i2c_driver_execute_finish_action();
}

static void bmp3_i2c_driver_finish_action_command(uint8_t command){
	DEBUG_PRINT("command %d\r\n", command);
	hri_sercomi2cm_write_CTRLB_CMD_bf(BMP3_I2C_DRIVER_SERCOM, command);
}

static void bmp3_i2c_driver_finish_action_command_ack(uint8_t command){
	DEBUG_PRINT("command ack %d\r\n", command);
	hri_sercomi2cm_clear_CTRLB_ACKACT_bit(BMP3_I2C_DRIVER_SERCOM);
	hri_sercomi2cm_write_CTRLB_CMD_bf(BMP3_I2C_DRIVER_SERCOM, command);
}

static void bmp3_i2c_driver_finish_action_command_nack(uint8_t command){
	DEBUG_PRINT("command nack %d\r\n", command);
	hri_sercomi2cm_set_CTRLB_ACKACT_bit(BMP3_I2C_DRIVER_SERCOM);
	hri_sercomi2cm_write_CTRLB_CMD_bf(BMP3_I2C_DRIVER_SERCOM, command);
}

static void bmp3_i2c_driver_finish_action_data(uint8_t data){
	DEBUG_PRINT("data 0x%x\r\n", data);
	hri_sercomi2cm_write_DATA_DATA_bf(BMP3_I2C_DRIVER_SERCOM, data);
}

static void bmp3_i2c_driver_finish_action_address(uint8_t address){
	DEBUG_PRINT("address 0x%x %s\r\n", (0xFE & address) >> 1, address & 0x1 ? "read" : "write");
	hri_sercomi2cm_write_ADDR_ADDR_bf(BMP3_I2C_DRIVER_SERCOM, address);
}

static void bmp3_i2c_driver_register_finish_action(void(*volatile finish_action)(uint8_t data), uint8_t data){
	g_bmp3_i2c_driver.finish_action = finish_action;
	g_bmp3_i2c_driver.finish_data = data;
}

static void bmp3_i2c_driver_execute_finish_action(){
	if(g_bmp3_i2c_driver.finish_action == 0){
		if(g_bmp3_i2c_driver.default_flag != 0){
			DEBUG_PRINT("default %d\r\n", g_bmp3_i2c_driver.default_flag);
			((Sercom *)BMP3_I2C_DRIVER_SERCOM)->I2CM.INTFLAG.reg = g_bmp3_i2c_driver.default_flag;
			g_bmp3_i2c_driver.default_flag  = 0;
		}
	}
	else{
		g_bmp3_i2c_driver.finish_action(g_bmp3_i2c_driver.finish_data);
		g_bmp3_i2c_driver.finish_action = 0;
		g_bmp3_i2c_driver.default_flag = 0;
	}
	g_bmp3_i2c_driver.finish_data = 0;;
}

static void bmp3_i2c_driver_state_idle(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void * user_pointer)
{
	switch(event->signal_)
	{
		case ENTRY_SIG:
		{
			DEBUG_PRINT("Idle\r\n");
			g_bmp3_i2c_driver.processed_bytes = 0;
			g_bmp3_i2c_driver.num_bytes_to_transfer = 0;
			g_bmp3_i2c_driver.direction = BMP3_I2C_DRIVER_DIR_INACTIVE;
			break;
		}
		case BMP3_I2C_DRIVER_EVT_SIG_START:
		{
			#if BMP3_MULTIPLEXER_AVAILABLE
			if(g_bmp3_i2c_driver.use_multiplexer_if_available){
				bmp3_i2c_driver_register_finish_action(bmp3_i2c_driver_finish_action_address, MULTIPLEXER_I2C_ADDRESS << 1 | BMP3_I2C_DRIVER_DIR_MASTER_TO_SLAVE);
				simpleStateMachineTran_(fsm, bmp3_i2c_driver_state_multiplexer_addressed, NULL);
				break;
			}
			#endif //BMP3_MULTIPLEXER_AVAILABLE
			
			bmp3_i2c_driver_register_finish_action(bmp3_i2c_driver_finish_action_address, g_bmp3_i2c_driver.slave_address << 1 | g_bmp3_i2c_driver.direction);
			if (g_bmp3_i2c_driver.direction == BMP3_I2C_DRIVER_DIR_MASTER_TO_SLAVE){
				simpleStateMachineTran_(fsm, bmp3_i2c_driver_state_sending, NULL);
			}
			else{
				simpleStateMachineTran_(fsm, bmp3_i2c_driver_state_receiving, NULL);
			}
			break;
		}
		case EXIT_SIG:
		default:
		{
			break;
		}
	}
}

#if BMP3_MULTIPLEXER_AVAILABLE
static void bmp3_i2c_driver_state_multiplexer_addressed(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void * user_pointer)
{
	switch(event->signal_)
	{
		case ENTRY_SIG:
		{
			DEBUG_PRINT("Multiplexer Addressed\r\n");
			break;
		}
		case EXIT_SIG:
		{
			break;
		}
		case BMP3_I2C_DRIVER_EVT_SIG_ACK:
		{
			bmp3_i2c_driver_register_finish_action(bmp3_i2c_driver_finish_action_data, g_bmp3_i2c_driver.multiplexer_channel_number);
			simpleStateMachineTran_(fsm, bmp3_i2c_driver_state_multiplexer_set, NULL);
			break;
		}
		case BMP3_I2C_DRIVER_EVT_SIG_ERROR:
		{
			bmp3_i2c_driver_reset_multiplexer_if_possible();
			simpleStateMachineTran_(fsm, bmp3_i2c_driver_state_idle, NULL);
			bmp3_i2c_driver_transmission_nosuccess();
			break;
		}
		default:
		{
			// Send Stop and abort
			bmp3_i2c_driver_reset_multiplexer_if_possible();
			bmp3_i2c_driver_register_finish_action(bmp3_i2c_driver_finish_action_command, 0x3);
			simpleStateMachineTran_(fsm, bmp3_i2c_driver_state_idle, NULL);
			bmp3_i2c_driver_transmission_nosuccess();
			break;
		}
	}
}

static void bmp3_i2c_driver_state_multiplexer_set(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void * user_pointer)
{
	switch(event->signal_)
	{
		case ENTRY_SIG:
		{
			DEBUG_PRINT("Multiplexer Set\r\n");
			break;
		}
		case EXIT_SIG:
		{
			break;
		}
		case BMP3_I2C_DRIVER_EVT_SIG_ACK:
		{
			bmp3_i2c_driver_register_finish_action(bmp3_i2c_driver_finish_action_command, 0x3);
			bmp3_i2c_driver_execute_finish_action();
			delay_us(1);
			bmp3_i2c_driver_register_finish_action(bmp3_i2c_driver_finish_action_address, g_bmp3_i2c_driver.slave_address << 1 | g_bmp3_i2c_driver.direction);
			if (g_bmp3_i2c_driver.direction == BMP3_I2C_DRIVER_DIR_MASTER_TO_SLAVE){
				simpleStateMachineTran_(fsm, bmp3_i2c_driver_state_sending, NULL);
			}
			else{
				simpleStateMachineTran_(fsm, bmp3_i2c_driver_state_receiving, NULL);
			}
			break;
		}
		case BMP3_I2C_DRIVER_EVT_SIG_ERROR:
		{
			bmp3_i2c_driver_reset_multiplexer_if_possible();
			simpleStateMachineTran_(fsm, bmp3_i2c_driver_state_idle, NULL);
			bmp3_i2c_driver_transmission_nosuccess();
			break;
		}
		default:
		{
			// Send Stop and abort
			bmp3_i2c_driver_reset_multiplexer_if_possible();
			bmp3_i2c_driver_register_finish_action(bmp3_i2c_driver_finish_action_command, 0x3);
			simpleStateMachineTran_(fsm, bmp3_i2c_driver_state_idle, NULL);
			bmp3_i2c_driver_transmission_nosuccess();
			break;
		}
	}
}
#endif //BMP3_MULTIPLEXER_AVAILABLE

static void bmp3_i2c_driver_state_sending(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void * user_pointer){
	switch(event->signal_)
	{
		case ENTRY_SIG:
		{
			DEBUG_PRINT("Sending\r\n");
			break;
		}
		case EXIT_SIG:
		{
			break;
		}
		case BMP3_I2C_DRIVER_EVT_SIG_ACK:
		{
			if(g_bmp3_i2c_driver.processed_bytes < g_bmp3_i2c_driver.num_bytes_to_transfer){
				bmp3_i2c_driver_register_finish_action(bmp3_i2c_driver_finish_action_data, g_bmp3_i2c_driver.transfer_buffer[g_bmp3_i2c_driver.processed_bytes++]);
			}
			else{
				// Send Stop and Finish
				bmp3_i2c_driver_register_finish_action(bmp3_i2c_driver_finish_action_command, 0x3);
				simpleStateMachineTran_(fsm, bmp3_i2c_driver_state_idle, NULL);
				bmp3_i2c_driver_transmission_success();
			}
			break;
		}
		case BMP3_I2C_DRIVER_EVT_SIG_ERROR:
		{
			simpleStateMachineTran_(fsm, bmp3_i2c_driver_state_idle, NULL);
			bmp3_i2c_driver_transmission_nosuccess();
			break;
		}
		default:
		{
			// Send Stop and abort
			bmp3_i2c_driver_register_finish_action(bmp3_i2c_driver_finish_action_command, 0x3);
			simpleStateMachineTran_(fsm, bmp3_i2c_driver_state_idle, NULL);
			bmp3_i2c_driver_transmission_nosuccess();
			break;
		}
	}
}

static void bmp3_i2c_driver_state_receiving(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void * user_pointer){
	switch(event->signal_)
	{
		case ENTRY_SIG:
		{
			DEBUG_PRINT("Receiving\r\n");
			break;
		}
		case EXIT_SIG:
		{
			break;
		}
		case BMP3_I2C_DRIVER_EVT_SIG_SB:
		{
			if(g_bmp3_i2c_driver.processed_bytes < g_bmp3_i2c_driver.num_bytes_to_transfer - 1){
				// We received byte and want to get more
				// Read data, send ACK, and wait for next byte
				uint32_t tmp = (uint8_t) hri_sercomi2cm_read_DATA_DATA_bf(BMP3_I2C_DRIVER_SERCOM);
				g_bmp3_i2c_driver.transfer_buffer[g_bmp3_i2c_driver.processed_bytes++] = tmp;
				bmp3_i2c_driver_register_finish_action(bmp3_i2c_driver_finish_action_command_ack, 0x2);
			}
			else if (g_bmp3_i2c_driver.processed_bytes == g_bmp3_i2c_driver.num_bytes_to_transfer - 1){
				// We received byte and are done now
				// Read data, send NACK, and send Stop
				uint32_t tmp = (uint8_t) hri_sercomi2cm_read_DATA_DATA_bf(BMP3_I2C_DRIVER_SERCOM);
				g_bmp3_i2c_driver.transfer_buffer[g_bmp3_i2c_driver.processed_bytes++] = tmp;
				bmp3_i2c_driver_register_finish_action(bmp3_i2c_driver_finish_action_command_nack, 0x3);
				simpleStateMachineTran_(fsm, bmp3_i2c_driver_state_idle, NULL);
				bmp3_i2c_driver_transmission_success();
			}
			else{
				// This should be NEVER reached
				// Send NACK, Stop and abort
				bmp3_i2c_driver_register_finish_action(bmp3_i2c_driver_finish_action_command_nack, 0x3);
				simpleStateMachineTran_(fsm, bmp3_i2c_driver_state_idle, NULL);
				bmp3_i2c_driver_transmission_nosuccess();
			}
			break;
		}
		case BMP3_I2C_DRIVER_EVT_SIG_ERROR:
		{
			simpleStateMachineTran_(fsm, bmp3_i2c_driver_state_idle, NULL);
			bmp3_i2c_driver_transmission_nosuccess();
			break;
		}
		default:
		{
			// Send Stop and abort
			bmp3_i2c_driver_register_finish_action(bmp3_i2c_driver_finish_action_command, 0x3);
			simpleStateMachineTran_(fsm, bmp3_i2c_driver_state_idle, NULL);
			bmp3_i2c_driver_transmission_nosuccess();
			break;
		}
	}
}

static void bmp3_i2c_driver_transmission_success(){
	last_transmit_was_success = 1;
	bmp3_i2c_driver_execute_finish_action();
	if(g_bmp3_i2c_driver.callback != NULL){
		g_bmp3_i2c_driver.callback(1);
	}
}
static void bmp3_i2c_driver_transmission_nosuccess(){
	last_transmit_was_success = 0;
	bmp3_i2c_driver_execute_finish_action();
	if(g_bmp3_i2c_driver.callback != NULL){
		g_bmp3_i2c_driver.callback(0);
	}
}

#if BMP3_MULTIPLEXER_AVAILABLE
static void bmp3_i2c_driver_reset_multiplexer_if_possible(){
	///* Reset the multiplexer if possible
	#ifdef BMP3_I2C_DRIVER_MUX_RESET_PIN
	gpio_set_pin_function(BMP3_I2C_DRIVER_MUX_RESET_PIN, GPIO_PIN_FUNCTION_OFF);
	gpio_set_pin_direction(BMP3_I2C_DRIVER_MUX_RESET_PIN, GPIO_DIRECTION_OUT);
	gpio_set_pin_level(BMP3_I2C_DRIVER_MUX_RESET_PIN, 0);
	delay_ms(10);
	gpio_set_pin_level(BMP3_I2C_DRIVER_MUX_RESET_PIN, 1);
	delay_ms(10);
	#endif
}
#endif