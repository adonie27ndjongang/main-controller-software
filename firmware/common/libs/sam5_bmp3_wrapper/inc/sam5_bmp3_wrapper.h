 /**
 * \file sam5_bmp3_wrapper.h
 *
 * sam5_bmp3_wrapper for interfacing with the API of the family of Bosch BMP3XX sensors
 * via the TCA9548a multiplexer
 * Copyright (C) 2023 Michal Ciebielski, Malte Kaiser, Michael Ratzel

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */ 
 
 #ifndef SAM_BMP3_WRAPPER_H_
 #define SAM_BMP3_WRAPPER_H_
 
 #include "bmp3.h"

/**
* \mainpage sam1 BMP3 Wrapper
* The documentation of this library is split into two parts. The first part contains only the data structures relevant to users of this library.
* The second part is aimed at developers and contains the documentation of all the internal functions.
* A developer is also required to read the user documentation.
* - \subpage user_page "Using the Library"
* - \subpage developer_page "Working on the Library"
*/

/**
* \defgroup user
* This module contains all the data structures relevant to a user.
*/

/**
* \page user_page Using the Library
* This library is meant to interface between sensors in the BMP3XX family of Bosch sensors via a TCA9548a multiplexer in the sam or SAME5 family from Microchip.
* In order to use the library, the user has to modify the bmp3_sensor_t sensor_array in sam5_bmp3_wrapper.c according to their electronics setup.
* Lastly, the user must not use the peripherals which are used by the library.
*
* The following link leads to the module containing everything that is relevant for using the library:
* \ref user
*/

/**
* \defgroup developer
* This module contains all the data structures not relevant to the user.
*/

/**
* \page developer_page Working on the Library
* The following link leads to the module containing everything that is internal to the library:
* \ref developer
*/

#include "hardware_defines.h"

#include <atmel_start.h>
#include <arm_neon.h>

#ifndef BMP3_READOUT_FREQUENCY
#error "Please specify BMP3 readout frequency"
#endif

#if BMP3_MULTIPLEXER_AVAILABLE
#define SAM5_BMP3_MAX_NUMBER_SENSORS	16				///< Maximum number of sensors attached to the multiplexer
#else 
#define SAM5_BMP3_MAX_NUMBER_SENSORS	2				///< Maximum number of sensors attached to a single i2c bus
#endif 

#define SAM5_BMP3_FORCE_CONTACT 0.5f	///< [Newton] Minimum force detected by the sensors that is considered a contact
#define SAM5_BMP3_FORCE_MAX 14.f		///< [Newton] Maximum force detectabl by the sensors befor saturation

/**
* \ingroup developer
* \brief Struct containing the multiplexer channel and sensor address for one sensor.
*/

typedef struct bmp3_interface_desc_t
{
	uint8_t multiplexer_channel_number;		///< The number of the multiplexer channel between 0 and 7.
	uint8_t bmp3_i2c_address;				///< The 7 bit I2C address of the BMP sensor.
}bmp3_interface_desc_t;

/**
* \ingroup developer
* \brief Struct where data read from the sensor is stored.
*/
typedef struct 
{
	volatile bool available;
	bmp3_interface_desc_t bmp3_interface_desc;	
	struct bmp3_dev dev;
	uint8_t reg_data[BMP3_LEN_P_T_DATA];
	struct bmp3_uncomp_data uncomp_data;
	struct bmp3_data comp_data;
	volatile float32_t force_newton;
	// Conversion between pressure in pascale to force in Newton:
	// force = a0 + a1 * pressure + a2 * (pressure * pressure)
	const double pressure_newton_conv_a0;
	const double pressure_newton_conv_a1;
	const double pressure_newton_conv_a2;
	const uint32_t led_pin;
}bmp3_sensor_t;

extern bmp3_sensor_t sensor_array[SAM5_BMP3_MAX_NUMBER_SENSORS];	///< Global array of all sensor, used for storing the data read and iterating through each sensor.

void sam5_bmp3_wrapper_init();
void sam5_bmp3_wrapper_start();
void sam5_bmp3_wrapper_loop();
void sam5_bmp3_wrapper_request_search_sensors(void (*finished_callback)());
void sam5_bmp3_wrapper_request_print_status(void (*finished_callback)());


#endif /* SAM_BMP3_WRAPPER_H_ */