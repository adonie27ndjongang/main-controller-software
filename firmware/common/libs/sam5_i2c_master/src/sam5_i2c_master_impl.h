/*
 * sam5_i2c_master_impl.h
 *
 * Created: 12.12.2022 23:30:21
 *  Author: Michael Ratzel
 */ 


#ifndef SAM5_I2C_MASTER_IMPL_H_
#define SAM5_I2C_MASTER_IMPL_H_

#include "sam5_i2c_master.h"

#ifndef I2C_MASTER_SERCOM_UNIT
#error "I2C_MASTER_SERCOM_UNIT is undefined"
#endif
#ifndef I2C_MASTER_SCL_PIN
#error "I2C_MASTER_SCL_PIN is undefined"
#endif
#ifndef I2C_MASTER_SCL_PIN_FUNCTION
#error "I2C_MASTER_SCL_PIN_FUNCTION is undefined"
#endif
#ifndef I2C_MASTER_SDA_PIN
#error "I2C_MASTER_SDA_PIN is undefined"
#endif
#ifndef I2C_MASTER_SDA_PIN_FUNCTION
#error "I2C_MASTER_SDA_PIN_FUNCTION is undefined"
#endif
#ifndef I2C_MASTER_GLCK_SLOW_GEN
#error "I2C_MASTER_GLCK_SLOW_GEN is undefined"
#endif

#define I2C_MASTER_INSERT_SERCOM_UNIT_(pre, unit, post)	pre ## unit ## post									///< This is necessary for C preprocessor macro expansion
#define I2C_MASTER_INSERT_SERCOM_UNIT(pre, unit, post)	I2C_MASTER_INSERT_SERCOM_UNIT_(pre, unit, post)		///< This is necessary for C preprocessor macro expansion

/**
* Some macros to generate "#define I2C_MASTER_SERCOM SERCOMn" out of "#define I2C_MASTER_SERCOM_UNIT n"
*/
#define I2C_MASTER_SERCOM I2C_MASTER_INSERT_SERCOM_UNIT(SERCOM, I2C_MASTER_SERCOM_UNIT,)

/**
* Some macros to generate "#define I2C_MASTER_SERCOM hri_mclk_set_APBXMASK_SERCOMn_bit" out of "#define I2C_MASTER_SERCOM_UNIT n"
*/
#if I2C_MASTER_SERCOM_UNIT == 0 || I2C_MASTER_SERCOM_UNIT == 1
#define I2C_MASTER_APB_MCLK_MASK_SETTER I2C_MASTER_INSERT_SERCOM_UNIT(hri_mclk_set_APBAMASK_SERCOM, I2C_MASTER_SERCOM_UNIT, _bit)		///< SERCOM Unit on APBA Bus
#elif I2C_MASTER_SERCOM_UNIT == 2 || I2C_MASTER_SERCOM_UNIT == 3
#define I2C_MASTER_APB_MCLK_MASK_SETTER I2C_MASTER_INSERT_SERCOM_UNIT(hri_mclk_set_APBBMASK_SERCOM, I2C_MASTER_SERCOM_UNIT, _bit)		///< SERCOM Unit on APBB Bus
#elif I2C_MASTER_SERCOM_UNIT == 4 || I2C_MASTER_SERCOM_UNIT == 5 || I2C_MASTER_SERCOM_UNIT == 6 || I2C_MASTER_SERCOM_UNIT == 7
#define I2C_MASTER_APB_MCLK_MASK_SETTER I2C_MASTER_INSERT_SERCOM_UNIT(hri_mclk_set_APBDMASK_SERCOM, I2C_MASTER_SERCOM_UNIT, _bit)		///< SERCOM Unit on APBD Bus
#else
#error "Unknown I2C_MASTER_SERCOM_UNIT, it is not within [0..7]"
#endif

/**
* Some macros to generate more information out of "#define I2C_MASTER_SERCOM_UNIT n"
*/
#define I2C_MASTER_SERCOM_GCLK_ID_CORE I2C_MASTER_INSERT_SERCOM_UNIT(SERCOM, I2C_MASTER_SERCOM_UNIT,_GCLK_ID_CORE)
#define I2C_MASTER_SERCOM_GCLK_ID_SLOW I2C_MASTER_INSERT_SERCOM_UNIT(SERCOM, I2C_MASTER_SERCOM_UNIT,_GCLK_ID_SLOW)

#define I2C_MASTER_MB_HANDLER			I2C_MASTER_INSERT_SERCOM_UNIT(SERCOM, I2C_MASTER_SERCOM_UNIT, _0_Handler)
#define I2C_MASTER_MB_IRQn				I2C_MASTER_INSERT_SERCOM_UNIT(SERCOM, I2C_MASTER_SERCOM_UNIT, _0_IRQn)
#define I2C_MASTER_SB_HANDLER			I2C_MASTER_INSERT_SERCOM_UNIT(SERCOM, I2C_MASTER_SERCOM_UNIT, _1_Handler)
#define I2C_MASTER_SB_IRQn				I2C_MASTER_INSERT_SERCOM_UNIT(SERCOM, I2C_MASTER_SERCOM_UNIT, _1_IRQn)
#define I2C_MASTER_ERROR_HANDLER        I2C_MASTER_INSERT_SERCOM_UNIT(SERCOM, I2C_MASTER_SERCOM_UNIT, _3_Handler)
#define I2C_MASTER_ERROR_IRQn           I2C_MASTER_INSERT_SERCOM_UNIT(SERCOM, I2C_MASTER_SERCOM_UNIT, _3_IRQn)

/**
* Calculate the Baud Rate
* Basically: BAUDRATE = GLCK / (10 + BAUD_TOTAL + GLCK * TRISE) but solely with integer calculations
*/
#define I2C_MASTER_BAUD_TOTAL (((CONF_CPU_FREQUENCY - (I2C_MASTER_BAUDRATE * 10U) - (I2C_MASTER_TRISE * (I2C_MASTER_BAUDRATE / 100U) * (CONF_CPU_FREQUENCY / 10000U) / 1000U)) * 10U + 5 * I2C_MASTER_BAUDRATE) / (I2C_MASTER_BAUDRATE * 10U))
#if I2C_MASTER_BAUD_TOTAL > 381
#error "Calculated Baud rate is not expressable"
#endif
#if I2C_MASTER_BAUD_TOTAL <= 0
#error "Calculated Baud rate is not expressable"
#endif
#if (I2C_MASTER_BAUD_TOTAL % 3) == 1
#define I2C_MASTER_BAUD_TOTAL_ADAPTED (I2C_MASTER_BAUD_TOTAL - 1)
#elif (I2C_MASTER_BAUD_TOTAL % 3) == 2
#define I2C_MASTER_BAUD_TOTAL_ADAPTED (I2C_MASTER_BAUD_TOTAL + 1)
#else
#define I2C_MASTER_BAUD_TOTAL_ADAPTED (I2C_MASTER_BAUD_TOTAL)
#endif

#define I2C_MASTER_BAUD_HIGH (I2C_MASTER_BAUD_TOTAL_ADAPTED / 3)
#define I2C_MASTER_BAUD_LOW (2*I2C_MASTER_BAUD_HIGH)


/**
* \brief Initializes the peripherals needed by the i2c master
*/
void i2c_master_initialize_peripherals(void);

/**
* \brief Init state of the I2C master state machine
*/
void i2c_master_state_init(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void * user_pointer);

/**
* \brief Idle state of the I2C master state machine
*/
void i2c_master_state_idle(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void * user_pointer);

/**
* \brief Addressed state of the I2C master state machine
*/
void i2c_master_state_addressed(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void * user_pointer);

/**
* \brief Addressed state of the I2C master state machine
*/
void i2c_master_state_command_send(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void * user_pointer);

/**
* \brief Sending state of the I2C master state machine
*/
void i2c_master_state_sending(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void * user_pointer);

/**
* \brief Receiving state of the I2C master state machine
*/
void i2c_master_state_receiving(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void * user_pointer);

/**
* \brief Set last transmit as successfull
*/
void i2c_master_transmission_success();

/**
* \brief Set last transmit as unsuccessfull
*/
void i2c_master_transmission_nosuccess();

/**
* \brief Process the next communication job in the ring buffer
*/
void i2c_master_process_next_job();


void i2c_master_finish_action_command(uint8_t command);
void i2c_master_finish_action_command_ack(uint8_t command);
void i2c_master_finish_action_command_nack(uint8_t command);
void i2c_master_finish_action_data(uint8_t data);
void i2c_master_finish_action_address(uint8_t data);
void i2c_master_register_finish_action(void(*volatile finish_action)(uint8_t data), uint8_t data);
void i2c_master_execute_finish_action(uint8_t default_flag);


typedef struct {
	simple_state_machine_t state_machine;
	uint8_t command;
	uint8_t slave_address;
	uint8_t direction;
	uint8_t transfer_buffer[SAM5_CRC8_MAX_DATA_LENGTH + 1]; // Data bytes and one CRC byte
	uint8_t processed_bytes;
	uint8_t num_bytes_to_transfer;
	void(*finish_action)(uint8_t);
	uint8_t finish_data;
	
	void (*special_command_callback)(bool, const uint8_t*);
	const sam5_i2c_protocol_identification_t* protocol_id;
	uint8_t address_to_identify;
	uint8_t identification_success;
	
	sam5_i2c_master_com_job_t job_buffer[I2C_MASTER_MAX_NUMBER_JOBS];
	bool active_jobs[I2C_MASTER_MAX_NUMBER_JOBS];
	uint8_t num_active_jobs;
	uint8_t active_jobs_pos;
}i2c_master_t;

extern i2c_master_t g_i2c_master;

#endif /* SAM5_I2C_MASTER_IMPL_H_ */