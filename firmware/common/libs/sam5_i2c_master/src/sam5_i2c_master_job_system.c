#include "sam5_i2c_master_impl.h"
#include "simple_state_machine.h"
#include "job_system.h"

#include <string.h>
#include <stdio.h>


const simple_state_machine_event_t start_event = {
	.signal_ = I2C_MASTER_EVT_SIG_START
};

uint8_t sam5_i2c_master_is_available(){
	return g_i2c_master.direction == I2C_MASTER_DIR_INACTIVE;
}

uint8_t sam5_i2c_master_register_com_job(const sam5_i2c_master_com_job_t *job){
	if(g_i2c_master.num_active_jobs >= I2C_MASTER_MAX_NUMBER_JOBS){
		printf("OVERFLOW -  WE NEED MORE BUS SPEED!\r\n");
		return 1;
	}
	if(job->num_bytes_to_transfer > SAM5_CRC8_MAX_DATA_LENGTH){
		printf("Job too big\r\n");
		return 1;
	}
	if(job->slave_address <= 0x0F || job->slave_address >= 0x78){
		printf("Address out of bounds\r\n");
		return 1;
	}
	CRITICAL_SECTION_ENTER();
	for(uint8_t i = 0; i < I2C_MASTER_MAX_NUMBER_JOBS; ++i){
		if(!g_i2c_master.active_jobs[i]){
			g_i2c_master.active_jobs[i] = true;
			memcpy((uint8_t*) &g_i2c_master.job_buffer[i], job, sizeof(sam5_i2c_master_com_job_t));
			if(job->direction == I2C_MASTER_DIR_MASTER_TO_SLAVE){
				g_i2c_master.job_buffer[i].buffer[job->num_bytes_to_transfer] = sam5_sw_crc8_CRC(job->buffer, job->num_bytes_to_transfer);
			}
			g_i2c_master.job_buffer[i].last_executed = 0;
			++g_i2c_master.num_active_jobs;
			break;
		}
	}
	CRITICAL_SECTION_LEAVE();
	i2c_master_process_next_job();
	return 0;
}

void i2c_master_process_next_job(){
	volatile hal_atomic_t __atomic;
	atomic_enter_critical(&__atomic);
	if(!sam5_i2c_master_is_available()){
		atomic_leave_critical(&__atomic);
		return;
	}
	if(g_i2c_master.num_active_jobs == 0){
		atomic_leave_critical(&__atomic);
		return;
	}
	for(uint8_t i = 0; i < I2C_MASTER_MAX_NUMBER_JOBS; ++i){
		uint8_t pos = (((uint16_t)g_i2c_master.active_jobs_pos) + i) % I2C_MASTER_MAX_NUMBER_JOBS;
		volatile sam5_i2c_master_com_job_t * const job = g_i2c_master.job_buffer + pos;
		if(!g_i2c_master.active_jobs[pos]){
			continue;
		}
		if(job->period_ms != 0){
			if(job->last_executed == periodic_counter){
				continue;
			}
			if(job->last_executed + job->period_ms > periodic_counter){
				continue;
			}
			else{
				job->last_executed = periodic_counter - (periodic_counter % job->period_ms);
			}
		}
		else{
			job->last_executed = periodic_counter;
		}
		g_i2c_master.active_jobs_pos = pos;
		
		g_i2c_master.command = job->command;
		g_i2c_master.direction = job->direction;
		g_i2c_master.slave_address = job->slave_address;
		g_i2c_master.num_bytes_to_transfer = job->num_bytes_to_transfer;
		if(job->direction == I2C_MASTER_DIR_MASTER_TO_SLAVE){
			// Copy num_bytes_to_transfer + 1 to accomodate CRC calculated in sam5_i2c_master_register_com_job()
			memcpy((uint8_t*)g_i2c_master.transfer_buffer, (uint8_t*)job->buffer, job->num_bytes_to_transfer + 1);
		}
		atomic_leave_critical(&__atomic);
		simpleStateMachineDispatch_(&g_i2c_master.state_machine, &start_event, NULL);
		i2c_master_execute_finish_action(0);
		return;
	}
	atomic_leave_critical(&__atomic);
}


uint32_t count = 0;

void i2c_master_transmission_success(){
	++count;
	const sam5_i2c_master_com_job_t *job;
	CRITICAL_SECTION_ENTER();
	job = (const sam5_i2c_master_com_job_t *) g_i2c_master.job_buffer + g_i2c_master.active_jobs_pos;
	if(job->direction == I2C_MASTER_DIR_SLAVE_TO_MASTER){
		memcpy((uint8_t*)job->buffer, (uint8_t*)g_i2c_master.transfer_buffer, job->num_bytes_to_transfer);
	}
	if(job->period_ms == 0){
		g_i2c_master.active_jobs[g_i2c_master.active_jobs_pos] = false;
		--g_i2c_master.num_active_jobs;
	}
	CRITICAL_SECTION_LEAVE();
	if(job->callback != NULL){
		job->callback(true, job);
	}
}

void i2c_master_transmission_nosuccess(){
	++count;
	const sam5_i2c_master_com_job_t *job;
	CRITICAL_SECTION_ENTER();
	job = (const sam5_i2c_master_com_job_t *) g_i2c_master.job_buffer + g_i2c_master.active_jobs_pos;
	if(job->period_ms == 0){
		g_i2c_master.active_jobs[g_i2c_master.active_jobs_pos] = false;
		--g_i2c_master.num_active_jobs;
	}
	CRITICAL_SECTION_LEAVE();
	if(job->callback != NULL){
		job->callback(false, job);
	}
}
