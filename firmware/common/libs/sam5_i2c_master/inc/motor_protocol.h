#ifndef MOTOR_CONTROL_H_
#define MOTOR_CONTROL_H_

#include <arm_neon.h>

#define MOTOR_PROTOCOL_VERSION_MAJOR		3
#define MOTOR_PROTOCOL_VERSION_MINOR		2

#define MOTOR_PROTOCOL_ADDRESS_AT_STARTUP	49
#define MOTOR_PROTOCOL_BASE_ADDRESS			40

typedef enum {
	MOTOR_GEAR_RATIO_5,
	MOTOR_GEAR_RATIO_10,
	MOTOR_GEAR_RATIO_15,
	MOTOR_GEAR_RATIO_30,
	MOTOR_GEAR_RATIO_50,
	MOTOR_GEAR_RATIO_75,
	MOTOR_GEAR_RATIO_100,
	MOTOR_GEAR_RATIO_150,
	MOTOR_GEAR_RATIO_210,
	MOTOR_GEAR_RATIO_250,
	MOTOR_GEAR_RATIO_298,
	MOTOR_GEAR_RATIO_380,
	MOTOR_GEAR_RATIO_1000,
} motor_gear_ratio_e;

typedef enum {
	MOTOR_CM_STOPPED = 0,										///< No voltage is applied to the motor
	MOTOR_CM_CALIB_PREP_POS = 2,								///< Prepare calibration by moving to positive end of the movement range
	MOTOR_CM_CALIB_PREP_NEG = 4,								///< Prepare calibration by moving to negative end of the movement range
	MOTOR_CM_CALIBRATION = 5,									///< Run finger calibration
	MOTOR_CM_VOLTAGE = 6,										///< The voltage setpoint is applied to the motor
	// The following three CM must have exactly those 3 numbers DO NOT CHANGE THIS
	MOTOR_CM_CURRENT =  0b001, // 1								///< The current setpoint is applied to the motor by the current controller
	MOTOR_CM_VELOCITY = 0b011, // 3								///< The velocity setpoint is applied to the motor while keeping the current limits
	MOTOR_CM_POSITION = 0b111, // 7								///< The position setpoint is applied to the motor while keeping the velocity and current limits
}control_mode_e;

typedef enum {
	MOTOR_FSM_INIT = 0,
	MOTOR_FSM_CALIBRATION,
	MOTOR_FSM_STOP,
	MOTOR_FSM_OPEN_LOOP_CONTROL,
	MOTOR_FSM_CLOSE_LOOP_CONTROL,
} motor_state_machine_state_e;

typedef enum {
	CALIB_STAGE_WAITING = 0,
	CALIB_STAGE_WAITING2,
	CALIB_STAGE_RIP_FREE,
	CALIB_STAGE_POLARITY_NEG,
	CALIB_STAGE_POLARITY_POS,
	CALIB_STAGE_ABS_END_NEG,
	CALIB_STAGE_ABS_END_POS,
	CALIB_STAGE_NEG_TO_POS_PASS,
	CALIB_STAGE_POS_TO_NEG_PASS,
	CALIB_STAGE_GO_BACK_TO_START,
	CALIB_STAGE_ERROR,
	CALIB_STAGE_FINISHED,
} calibration_stage_e;

typedef struct
{
	float32_t k_p;		///< The overall gain for the PID controller		u(t) = K_p*[e(t) + k_i*e(t)*1/s + K_d*e(t)*s]
	float32_t k_i;		///< The integral gain for the PID controller		K_i = 1/T_i
	float32_t k_d;		///< The differential gain for the PID controller	K_d = T_d
	
	float32_t k_t;		///< The gain for the back calculation k_t = 1/T_t, a rule of thumb is T_t = T_i if no D part available, else T_t = sqrt(T_i*T_d)
	float32_t n;		///< Filter coefficient, Usual values are 8 to 20. The bigger N get's the higher the cutoff frequency of the low pass will be, as tau = k_d/N = T_d/N. Set N to zero to deactivate the filter
	float32_t hyst;		///< Hysteresis to avoid oscillating if we are close enough to the target
} sam5_control_settings_t;

typedef struct {
	uint8_t control_mode : 3;									///< Control mode of the motor controller
	uint8_t stop_at_contact : 1;								///< If this bit is 1 the motor will be stopped at contact
	uint8_t continue_at_end : 1;								///< If this bit is 1 the motor will NOT stop at the movement range
	uint8_t use_endstop_for_end : 1;							///< If this bit is 1 the motor will use the endstop to detect the end of the movement range
	uint8_t use_endstop_for_refine : 1;							///< If this bit is 1 the motor will use the endstop to refine the current position with when the endstop is triggered
	uint8_t use_aggressive_contact_detection: 1;				///< If this bit is 1 the motor will not stop so easily on a contact and be a bit more aggressive
} dc_motor_controller_input_flags_t;

typedef struct {
	float32_t angle;											///< Angle of the motor [rad]
	float32_t velocity;											///< Velocity of the motor [rad/s]
	float32_t current;											///< Motor current [A]
	float32_t voltage;											///< Motor voltage [V]
	dc_motor_controller_input_flags_t flags;
} dc_motor_controller_inputs_t;

typedef struct {
	uint8_t hardware_state : 2;
	uint8_t contact_detected : 1;
	uint8_t min_angle_reached : 1;
	uint8_t max_angle_reached : 1;
	uint8_t target_angle_reached : 1;
	uint8_t stopped_due_to_contact : 1;
	uint8_t motor_stopped : 1;
} dc_motor_controller_state_flags_a_t;

typedef struct {
	uint8_t state_machine : 3;
	uint8_t calib_stage : 4;
	uint8_t endstop_active : 1;
} dc_motor_controller_state_flags_b_t;

typedef struct {
	float32_t angle;											///< Angle of the motor after gearbox [rad]
	float32_t velocity;											///< Velocity of the motor after gearbox [rad/s]
	float32_t current;											///< Motor current [A]
	float32_t voltage;											///< Motor voltage [V]
	float32_t temperature;										///< Temp above ambient [K]
	dc_motor_controller_state_flags_a_t flags_a;				///< Flags that describe different states
	dc_motor_controller_state_flags_b_t flags_b;				///< Flags that describe different states
} dc_motor_controller_state_t;

typedef struct {
	float32_t angle_min;										///< [rad] Lowest positive value of the angle of the motor after gearbox
	float32_t angle_max;										///< [rad] Highest value of the angle of the motor after gearbox
	float32_t velocity_max;										///< [rad/s] Highest absolute value of the velocity after gearbox applied to the motor
	float32_t current_max;										///< [A] Highest absolute value of the current applied to the motor
	float32_t voltage_max;										///< [V] Highest ABSOLUTE value of the voltage applied to the motor
}  dc_motor_controller_limits_t;

typedef struct {
	uint8_t flip_outputs : 1;
	uint8_t started_calib_at_pos : 1;
	uint8_t pos_end_finished : 1;
	uint8_t neg_end_finished : 1;
	uint8_t endstop_at_pos : 1;
	uint8_t searching_endstop : 1;
	uint8_t endstop_trig_calibrated : 1;
	uint8_t endstop_untrig_calibrated : 1;
} dc_motor_controller_calibration_flags_t;

typedef enum {
	CALIB_ERROR_NONE,
	CALIB_ERROR_NO_MOVEMENT_MEASURED,
	CALIB_ERROR_NO_CURRENT_MEASURED,
	CALIB_ERROR_DETECTED_SPEED_TOO_SMALL,
	CALIB_ERROR_ANGLE_RANGE_TOO_SMALL,
} dc_motor_controller_calibration_error_code_e;

typedef struct {
	float32_t endstop_angle_trigger;
	float32_t endstop_angle_untrigger;
	int16_t ocm_raw_offset;										///< Raw offset of the current sensors [LSB]
	uint8_t error_code;
	dc_motor_controller_calibration_flags_t flags;
} dc_motor_controller_calibration_result_t;

typedef struct {
	float32_t angle[3];
	float32_t current[3];
} dc_motor_controller_combined_state_t;

typedef struct {
	dc_motor_controller_state_flags_a_t flags_a[3];
	dc_motor_controller_state_flags_b_t flags_b[3];
} dc_motor_controller_combined_state_flags_t;

typedef struct {
	uint8_t run_motor_controllers : 1;
	uint8_t heartbeat : 1;
	uint8_t spare : 6;
} control_flags_t;


enum{
	MOTOR_PROTOCOL_COMMON_WRITE_CONTROL_FLAGS = 0,
	MOTOR_PROTOCOL_COMMON_READ_COMBINED_STATE =  1,
	MOTOR_PROTOCOL_COMMON_READ_COMBINED_STATE_FLAGS = 2,
	MOTOR_PROTOCOL_COMMON_WRITE_SUPPLY_VOLTAGE = 3,
	MOTOR_PROTOCOL_COMMON_SPARE_4 = 4,
	MOTOR_PROTOCOL_COMMON_SPARE_5 = 5,
	MOTOR_PROTOCOL_COMMON_SPARE_6 = 6,
	MOTOR_PROTOCOL_COMMON_SPARE_7 = 7,
	MOTOR_PROTOCOL_COMMON_SPARE_8 = 8,
	MOTOR_PROTOCOL_COMMON_SPARE_9 = 9,
};

#define MOTOR_PROTOCOL_NUM_GENERAL_COMMANDS 10

enum{
	MOTOR_PROTOCOL_WRITE_INPUT_FLAGS = 0,
	MOTOR_PROTOCOL_WRITE_INPUT_ANGLE =  1,
	MOTOR_PROTOCOL_WRITE_INPUT_VELOCITY = 2,
	MOTOR_PROTOCOL_WRITE_INPUT_CURRENT = 3,
	MOTOR_PROTOCOL_WRITE_INPUT_VOLTAGE = 4,
	MOTOR_PROTOCOL_WRITE_INPUT = 5,
	MOTOR_PROTOCOL_READ_INPUT = 6,
	
	MOTOR_PROTOCOL_READ_STATE = 7,
	MOTOR_PROTOCOL_READ_STATE_FLAGS_A = 8,
	MOTOR_PROTOCOL_READ_STATE_FLAGS_B = 9,
	
	MOTOR_PROTOCOL_WRITE_LIMITS = 10,
	MOTOR_PROTOCOL_READ_LIMITS = 11,
	MOTOR_PROTOCOL_READ_CALIBRATION_RESULT = 12,
	
	MOTOR_PROTOCOL_WRITE_CURRENT_CONTROLLER = 13,
	MOTOR_PROTOCOL_WRITE_VELOCITY_CONTROLLER = 14,
	MOTOR_PROTOCOL_WRITE_POSITION_CONTROLLER = 15,
	MOTOR_PROTOCOL_READ_CURRENT_CONTROLLER = 16,
	MOTOR_PROTOCOL_READ_VELOCITY_CONTROLLER = 17,
	MOTOR_PROTOCOL_READ_POSITION_CONTROLLER = 18,
	
	MOTOR_PROTOCOL_SPARE_1 = 19,
};

#define MOTOR_PROTOCOL_NUM_COMMANDS_PER_MOTOR 20

#define MOTOR_PROTOCOL_IS_GENERAL_COMMAND(command) (command < MOTOR_PROTOCOL_NUM_GENERAL_COMMANDS)
#define MOTOR_PROTOCOL_GET_MOTR_INDEX(command) ((command - MOTOR_PROTOCOL_NUM_GENERAL_COMMANDS) / MOTOR_PROTOCOL_NUM_COMMANDS_PER_MOTOR)
#define MOTOR_PROTOCOL_GET_COMMAND_TYPE(command) ((command - MOTOR_PROTOCOL_NUM_GENERAL_COMMANDS) % MOTOR_PROTOCOL_NUM_COMMANDS_PER_MOTOR)
#define MOTOR_PROTOCOL_BUILD_COMMAND(motor_index, command_type) (MOTOR_PROTOCOL_NUM_GENERAL_COMMANDS + command_type + motor_index * MOTOR_PROTOCOL_NUM_COMMANDS_PER_MOTOR)

#endif // MOTOR_CONTROL_H_