#include "job_system.h"
#include "sam5_i2c_master.h"
#include "main_control.h"
#if BAT_VLTG_AVAILABLE
#include "sam5_adc_batvlt.h"
#endif


uint32_t periodic_counter = 0;

void job_system_init(){
	///* Enable APB SERCOM units APB communication
	hri_mclk_set_APBBMASK_TC3_bit(MCLK);
	hri_gclk_write_PCHCTRL_reg(GCLK, TC3_GCLK_ID, GCLK_PCHCTRL_GEN_GCLK0_Val | (1 << GCLK_PCHCTRL_CHEN_Pos));
	
	// Check if no software reset is ongoing
	if(!hri_tc_is_syncing(TC3, TC_SYNCBUSY_SWRST)){
		// If TC3 is enable -> disable it
		if(hri_tc_get_CTRLA_ENABLE_bit(TC3)){
			hri_tc_clear_CTRLA_ENABLE_bit(TC3);
		}
		// Start software reset
		hri_tc_set_CTRLA_SWRST_bit(TC3);
	}
	// Wait for software to finish
	hri_tc_wait_for_sync(TC3, TC_SYNCBUSY_SWRST);
	
	hri_tc_write_CTRLA_reg(TC3,
	0 << TC_CTRLA_CAPTEN0_Pos
	| 0 << TC_CTRLA_CAPTEN1_Pos
	| TC_CTRLA_PRESCALER_DIV16_Val << TC_CTRLA_PRESCALER_Pos
	| 0x0 << TC_CTRLA_MODE_Pos);
	
	hri_tc_write_CTRLB_reg(TC3,
	0 << TC_CTRLBSET_ONESHOT_Pos
	| 0 << TC_CTRLBSET_DIR_Pos);
	
	hri_tc_write_INTEN_reg(TC3,
	0 << TC_INTENSET_MC0_Pos
	| 0 << TC_INTENSET_MC1_Pos
	| 1 << TC_INTENSET_OVF_Pos);	// Enable Overflow interrupt
	
	hri_tc_write_WAVE_reg(TC3, 0x1 << TC_WAVE_WAVEGEN_Pos);	// Set wavegen to Match frequency (Overflow on match CC0)
	
	hri_tccount16_set_CC_CC_bf(TC3, 0, 6250); // 1 kHz frequency with prescaler 16 and CPU Frequency 100 MHz
	
	NVIC_SetPriority(TC3_IRQn, NVIC_PRIORITY_JOBSYSTEM_TC);
	
	periodic_counter = 0;
	
	///* Enable TC OVF interrupt
	NVIC_DisableIRQ(TC3_IRQn);
	NVIC_ClearPendingIRQ(TC3_IRQn);
	NVIC_EnableIRQ(TC3_IRQn);
	
	hri_tc_set_CTRLA_ENABLE_bit(TC3);
}


/**
* brief: Timer overflow interrupt handler -> Execute periodic jobs particulary the main_control_loop

*/
void TC3_Handler()
{
	++periodic_counter;
	hri_tc_clear_INTFLAG_OVF_bit(TC3);
	
	sam5_i2c_master_loop();
	
	if(periodic_counter % 10 == 0){
		main_control_loop();
	}
	if((periodic_counter + 2) % 500 == 0){
		// Offset by 2 to avoid same loop as normal main_control loop to share the load more evenly
		main_control_heartbeat();
	}
    #if BAT_VLTG_AVAILABLE
	// Do initial measurement way faster than continuous one
	uint32_t bat_vltg_period = sam5_bat_vltg_is_measurement_valid() ? 100 : 5;
	if((periodic_counter + 3) % bat_vltg_period == 0){
		sam5_bat_vltg_trigger_measurement();
    }
	if((periodic_counter + 4) % bat_vltg_period == 0){
		sam5_bat_vltg_send_measurment();
    }
    #endif
}
