#ifndef SIMPLE_STATE_MACHINE_H__
#define SIMPLE_STATE_MACHINE_H__

#include <stdint.h>

#define EVENT_BASE 3

enum{
    ENTRY_SIG = 1,
    EXIT_SIG = 2
};

//Erwan Todo: Find the different option of this signal and their signification, debugging?
typedef uint8_t simple_state_machine_signal_t;
typedef struct simple_state_machine_event_t simple_state_machine_event_t;
typedef struct simple_state_machine_t simple_state_machine_t;

typedef void (*simple_state_machine_state_handler_t)(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void *user_struct);

struct simple_state_machine_event_t
{
    simple_state_machine_signal_t signal_;
};

struct simple_state_machine_t
{
   simple_state_machine_state_handler_t state_handler__;
};

void simpleStateMachineInit_(volatile simple_state_machine_t *me, simple_state_machine_state_handler_t initial_state, volatile void *user_struct);
void simpleStateMachineTran_(volatile simple_state_machine_t *me, simple_state_machine_state_handler_t target, volatile void *user_struct);
void simpleStateMachineDispatch_(volatile simple_state_machine_t *me, simple_state_machine_event_t const *event, volatile void *user_struct);

#endif // SIMPLE_STATE_MACHINE_H__