#include "simple_state_machine.h"

static simple_state_machine_event_t const entryEvt = { ENTRY_SIG };
static simple_state_machine_event_t const exitEvt = { EXIT_SIG };

void simpleStateMachineInit_(volatile simple_state_machine_t *me, simple_state_machine_state_handler_t initial_state, volatile void *user_struct)
{
   me->state_handler__ = initial_state;
   simpleStateMachineDispatch_(me, &entryEvt, user_struct);
}

void simpleStateMachineTran_(volatile simple_state_machine_t *me, simple_state_machine_state_handler_t target, volatile void *user_struct)
{
   simpleStateMachineDispatch_(me, &exitEvt, user_struct);
   me->state_handler__ = target; 
   simpleStateMachineDispatch_(me, &entryEvt, user_struct);
}

void simpleStateMachineDispatch_(volatile simple_state_machine_t *me, simple_state_machine_event_t const *event, volatile void *user_struct)
{
   (*me->state_handler__)(me, event, user_struct);
}