
/*
 * sam5_adc_batvlt.h
 *
 * Created: 2023/12/19 23:04:29
 *  Author: Jonas Weigand
 */ 
#ifndef SAM5_ADC_BATVLT_H_
#define SAM5_ADC_BATVLT_H_

#include <stdint.h>
#include <arm_neon.h>

#define _ADCn(num) ADC##num
#define ADCn(num) _ADCn(num)

#define BAT_VLTG_ROLLING_AVERAGE_SIZE 100


uint8_t sam5_bat_vltg_conf();

void sam5_bat_vltg_trigger_measurement();

uint8_t sam5_bat_vltg_send_measurment();

uint8_t sam5_bat_vltg_is_measurement_valid();

extern float32_t bat_vltg;

#endif /* SAM5_ADC_BATVLT_H_ */
