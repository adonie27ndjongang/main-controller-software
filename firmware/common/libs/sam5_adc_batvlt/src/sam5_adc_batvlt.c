
/*
* sam5_adc_batvlt.c
*
* Created: 2023/12/19 23:05:16
*  Author: Jonas Weigand
*/
#include "sam5_adc_batvlt.h"
#include "sam5_i2c_master.h"
#include "hardware_defines.h"
#include "main_control.h"
#include "motor_protocol.h"

#include <atmel_start.h>
#include <string.h>
#include <utils.h>
#include <stdio.h>

static Adc *const p_bat_vltg_adc = ADCn(BAT_VLTG_ADCn);
float32_t bat_vltg;
static uint32_t measurements_count;

uint8_t sam5_bat_vltg_conf()
{
	/// Initialize ADC
	/// Initialize clock
	hri_mclk_set_APBDMASK_ADC1_bit(MCLK);
	hri_gclk_write_PCHCTRL_reg(GCLK, ADC1_GCLK_ID, GCLK_PCHCTRL_GEN_GCLK0_Val | (1 << GCLK_PCHCTRL_CHEN_Pos));

	/// Check if software reset of ADC is in progress
	if (!hri_adc_is_syncing(ADC1, ADC_SYNCBUSY_SWRST)) {
		/// If ADC is already enabled
		if (hri_adc_get_CTRLA_reg(ADC1, ADC_CTRLA_ENABLE)) {
			/// * Disable ADC
			hri_adc_clear_CTRLA_ENABLE_bit(ADC1);
			/// * Wait until the ADC is disabled
			hri_adc_wait_for_sync(ADC1, ADC_SYNCBUSY_ENABLE);
		}
		/// Perform software reset of ADC
		hri_adc_write_CTRLA_reg(ADC1, ADC_CTRLA_SWRST);
	}
	/// Wait until the software reset is finished
	hri_adc_wait_for_sync(ADC1, ADC_SYNCBUSY_SWRST);

	uint32_t register_value = 0;

	/// Write CTRLA register
	register_value = 1	<< ADC_CTRLA_R2R_Pos                         /// * Enable Rail-to-Rail operation for greater common mode voltage range
	| ADC_CTRLA_PRESCALER_DIV8_Val << ADC_CTRLA_PRESCALER_Pos;        /// * Divide hardware clock by 2 (lowest possible for maximum speed)
	
	hri_adc_write_CTRLA_reg(ADC1, register_value);

	/// Write CTRLB register. This
	/// Disable window monitor mode
	///	Set result resolution to 12 bit
	///	Disable digital correction logic
	///	Set ADC to single conversion mode
	///	The ADC conversion result is right-adjusted in the RESULT register
	hri_adc_write_CTRLB_reg(ADC1, 0);
	
	/// Write EVCTRL register to disable all events
	hri_adc_write_EVCTRL_reg(ADC1, 0);
	
	/// Write DBGCTRL register so the ADC is halted when the CPU is halted by an external debugger.
	hri_adc_write_DBGCTRL_reg(ADC1, 0);
	
	/// Write INPUTCTRL register
	register_value = 0 << ADC_INPUTCTRL_DSEQSTOP_Pos    /// Disable automatic stopping of DMA sequencing
	| 0x18 << ADC_INPUTCTRL_MUXNEG_Pos                  /// Set negative reference to internal ground
	| 0 << ADC_INPUTCTRL_DIFFMODE_Pos                   /// Disable differential mode
	| 0	<< ADC_INPUTCTRL_MUXPOS_Pos;                    /// Set input channel to pin 0 (Will be updated as soon, as a read out happens)
	hri_adc_write_INPUTCTRL_reg(ADC1, register_value);
	
	/// Write REFCTRL register
	register_value =	1 << ADC_REFCTRL_REFCOMP_Pos    /// Reference buffer offset compensation is enabled.
	| BAT_VLTG_ADC_REFERENCE << ADC_REFCTRL_REFSEL_Pos;    /// M_OCM_ADC_REFERENCE as reference
	hri_adc_write_REFCTRL_reg(ADC1, register_value);
	
	/// Write AVGCTRL register
	register_value = 2 << ADC_AVGCTRL_ADJRES_Pos		/// Set division coefficient to 4 (2^2)
	| 2	<< ADC_AVGCTRL_SAMPLENUM_Pos;					/// Divide accumulated samples by Get 2 sample
	hri_adc_write_AVGCTRL_reg(ADC1, register_value);
	
	/// Write SMPCTRL register
	register_value = 1	<< ADC_SAMPCTRL_OFFCOMP_Pos      /// Enable comparator offset compensation (fixed 4 clock cycles sampling time, minimum due to electronic characteristics: 2.4 cycles)
	| 0	<< ADC_SAMPCTRL_SAMPLEN_Pos;                     /// Must be disabled, by being set to 0, with offset compensation enabled
	hri_adc_write_SAMPCTRL_reg(ADC1, register_value);
	
	/// Write calibration values to CALIB register
	hri_adc_write_CALIB_reg(ADC1,
	ADC_CALIB_BIASREFBUF(
	(*(uint32_t *) ADC1_FUSES_BIASREFBUF_ADDR >> ADC1_FUSES_BIASREFBUF_Pos))
	| ADC_CALIB_BIASR2R((*(uint32_t *) ADC1_FUSES_BIASR2R_ADDR >> ADC1_FUSES_BIASR2R_Pos))
	| ADC_CALIB_BIASCOMP((*(uint32_t *) ADC1_FUSES_BIASCOMP_ADDR >> ADC1_FUSES_BIASCOMP_Pos)));

	hri_adc_set_CTRLA_ENABLE_bit(ADC1);
	bat_vltg = 40; // Initialize with really high value as this is safer for the motors that we assume a really high voltage and then reduce it down to the actual voltage
	measurements_count = 0;
	return 0;
}

void sam5_bat_vltg_trigger_measurement(){
	hri_adc_write_INPUTCTRL_MUXPOS_bf(p_bat_vltg_adc, BAT_VLTG_ADC_AINn);

	hri_adc_set_SWTRIG_START_bit(p_bat_vltg_adc);
}

uint8_t sam5_bat_vltg_send_measurment(){
	if(p_bat_vltg_adc->STATUS.bit.ADCBUSY){
		//ADC still busy
		return 0;
	}
	const uint16_t result = hri_adc_read_RESULT_reg(p_bat_vltg_adc);
	const float32_t new_measurement = BAT_VLTG_ADC_REFERENCE * (result / ADC_RESOLUTION) * BAT_VLTG_DIVIDER_RATIO + BAT_VLTG_ZERO_OFFSET;
	bat_vltg = 0.95f * bat_vltg + 0.05f * new_measurement;
	++measurements_count;
	if(sam5_bat_vltg_is_measurement_valid()){
		for(uint8_t i = 0; i < MAIN_CONTROL_NUM_MOTOR_CONTROLLER; ++i){
			sam5_i2c_master_com_job_t job = {
				.command = MOTOR_PROTOCOL_COMMON_WRITE_SUPPLY_VOLTAGE,
				.slave_address = 40 + i,
				.direction = I2C_MASTER_DIR_MASTER_TO_SLAVE,
				.num_bytes_to_transfer = sizeof(bat_vltg),
				.callback = NULL,
				.period_ms = 0
			};
			memcpy((uint8_t*) job.buffer, &bat_vltg, sizeof(bat_vltg));
			sam5_i2c_master_register_com_job(&job);
		}
	}
	return 1;
}

uint8_t sam5_bat_vltg_is_measurement_valid(){
	return measurements_count > 100;
}