#ifndef SAM5_FEEDBACK_MOTOR_IMPL_H_
#define SAM5_FEEDBACK_MOTOR_IMPL_H_

#include "sam5_feedback_motor.h"
#include "hardware_defines.h"

#ifndef FEEDBACK_MOTOR_PIN
#error "Feedback motor pin was not defined"
#endif

#endif /* SAM5_FEEDBACK_MOTOR_IMPL_H_ */