#ifndef SAM5_FEEDBACK_MOTOR_H_
#define SAM5_FEEDBACK_MOTOR_H_

#include <atmel_start.h>
#include <peripheral_clk_config.h>
#include <stdio.h>


/**
* \brief Function to initialize the feedback motor controller functionality
*/
void initialize_feedback_motor();

/**
* \brief Function to let the feedback motor vibrate for a specified duration [20 ms]
*
* \param vibrate_duration Vibration duration in multiples of 20 ms
*/
void vibrate_motor(uint32_t vibrate_duration_20ms);
#endif /* SAM5_FEEDBACK_MOTOR_H_ */