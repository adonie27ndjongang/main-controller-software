#include "sam5_usart_console_commands.h"
#include "../sam5_usart_console_impl.h"
#include "sam5_i2c_master.h"
#include "motor_protocol.h"
#include "main_control.h"
#include "sam5_adc_batvlt.h"

#include <stdio.h>
#include <string.h>

const char sam5_usart_console_motor_desc[] = "configure motors, e.g. their limits";
const char sam5_usart_console_motor_help[] =
"Usages:\r\n"
"########################################"
"Limit configuration\r\n"
"motor <motor_controller> <motor> limits\r\n"
"  Query the motor limits\r\n"
"  Specify the motor by it's motor controller index and motor index\r\n"
"motor <motor_controller> <motor> limits <pos_min> <pos_max> <vel_max> <current_max> <voltage_min> <voltage_max>\r\n"
"  Set all limits as once, accepting floating point numbers\r\n"
"  Specify the motor by it's motor controller index and motor index\r\n"
"  The units are:\r\n"
"    pos_min     - rad\r\n"
"    pos_max     - rad\r\n"
"    vel_max     - rad/s\r\n"
"    current_max - A\r\n"
"    voltage_min - V\r\n"
"    voltage_max - V\r\n"
"\r\n"
"########################################"
"PID configuration\r\n"
"motor <motor_controller> <motor> pid current/velocity/position\r\n"
"  Get the PID setup of the specific controller\r\n"
"  Specify the motor by it's motor controller index and motor index\r\n"
"motor <motor_controller> <motor> pid current/velocity/position <k_p> <k_i> <k_d> <k_t> <n> <hyst>\r\n"
"  Set the PID setup of the specific controller\r\n"
"  Specify the motor by it's motor controller index and motor index\r\n"
"\r\n"
"########################################"
"Informations\r\n"
"motor <motor_controller> <motor> inputs\r\n"
"  Querry the current inputs of the motor\r\n"
"  Specify the motor by it's motor controller index and motor index\r\n"
"motor inputs\r\n"
"  Querry the current inputs of ALL the motors"
"motor <motor_controller> <motor> state\r\n"
"  Querry the current state of the motor\r\n"
"  Specify the motor by it's motor controller index and motor index\r\n"
"motor state\r\n"
"  Querry the current state of ALL the motors"
;

static void simple_success_callback(bool success, const sam5_i2c_master_com_job_t* job);
static void limits_received_callback(bool success, const sam5_i2c_master_com_job_t* job);
static void controller_received_callback(bool success, const sam5_i2c_master_com_job_t* job);
static void inputs_received_callback(bool success, const sam5_i2c_master_com_job_t* job);
static void state_received_callback(bool success, const sam5_i2c_master_com_job_t* job);
static void calibration_received_callback(bool success, const sam5_i2c_master_com_job_t* job);

uint8_t sam5_usart_console_motor(const int argc, const char** argv){
	if(argc < 2){
		printf("Unrecognized signature, look at \"help motor\" for more information\r\n");
		return 0;
	}
	uint8_t run_for_all_motors = 0;
	if(argc == 2 && strcmp("limits", argv[1]) == 0){
		run_for_all_motors = 1;
	}
	else if(argc == 3 && strcmp("pid", argv[1]) == 0){
		if(strcmp("current", argv[2]) != 0 && strcmp("velocity", argv[2]) != 0 && strcmp("cascaded_velocity", argv[2]) != 0 && strcmp("cascaded_current", argv[2]) != 0){
			printf("Unrecognized PID controller name \"%s\"\r\n", argv[2]);
			return 0;
		}
		run_for_all_motors = 1;
	}
	else if(argc == 2 && strcmp("inputs", argv[1]) == 0){
		run_for_all_motors = 1;
	}
	else if(argc == 2 && strcmp("state", argv[1]) == 0){
		run_for_all_motors = 1;
	}
	else if(argc == 2 && strcmp("calib", argv[1]) == 0){
		run_for_all_motors = 1;
	}
	if(run_for_all_motors){
		ASSERT(argc <= 3);
		uint8_t ret = 0;
		int new_argc = argc + 2;
		char* new_argv[5];
		char motor_controller[2] = {0};
		char motor[2] = {0};
		new_argv[0] = (char*) argv[0];
		new_argv[1] = motor_controller;
		new_argv[2] = motor;
		for(uint8_t i = 1; i < argc; ++i){
			new_argv[i+2] = (char*) argv[i];
		}
		for(uint8_t i = 0; i < MAIN_CONTROL_NUM_MOTORS; ++i){
			motor_controller[0] = '1' + g_main_control.motors[i].controller_index;
			motor[0] = '1' + g_main_control.motors[i].motor_index;
			ret |= sam5_usart_console_motor(new_argc, (const char**) new_argv);
		}
		return ret;
	}
	if(argc < 4){
		printf("Unrecognized signature, look at \"help motor\" for more information\r\n");
		return 0;
	}
	
	int32_t motor_controller_index, motor_index;
	if(parse_integer_parameter(argv[1], &motor_controller_index) == 0){
		printf("Could not parse motor_controller index from \"%s\"\r\n", argv[1]);
		return 0;
	}
	if(parse_integer_parameter(argv[2], &motor_index) == 0){
		printf("Could not parse motor index from \"%s\"\r\n", argv[2]);
		return 0;
	}
	
	// Transform between the counting printed on PCBs that start at 1 to the software representation that starts counting at 0
	--motor_controller_index;
	--motor_index;

	sam5_i2c_master_com_job_t job;
	job.period_ms = 0;
	job.slave_address = 40 + motor_controller_index;
	// Set base command number - final offset will be added later
	job.command = MOTOR_PROTOCOL_NUM_GENERAL_COMMANDS + motor_index * MOTOR_PROTOCOL_NUM_COMMANDS_PER_MOTOR;
	
	if(strcmp("limits", argv[3]) == 0){
		if(argc == 4){
			job.direction = I2C_MASTER_DIR_SLAVE_TO_MASTER;
			job.num_bytes_to_transfer = sizeof(dc_motor_controller_limits_t);
			job.slave_address = 40 + motor_controller_index;
			job.command += MOTOR_PROTOCOL_READ_LIMITS;
			job.callback = limits_received_callback;
			job.period_ms = 0;
		}
		else if(argc == 9){
			dc_motor_controller_limits_t limits;
			if(parse_float_parameter(argv[4], &limits.angle_min) == 0){
				printf("Could not evaluate \"%s\" as a floating number\r\n", argv[4]);
				return 0;
			}
			if(parse_float_parameter(argv[5], &limits.angle_max) == 0){
				printf("Could not evaluate \"%s\" as a floating number\r\n", argv[5]);
				return 0;
			}
			if(parse_float_parameter(argv[6], &limits.velocity_max) == 0){
				printf("Could not evaluate \"%s\" as a floating number\r\n", argv[6]);
				return 0;
			}
			if(parse_float_parameter(argv[7], &limits.current_max) == 0){
				printf("Could not evaluate \"%s\" as a floating number\r\n", argv[7]);
				return 0;
			}
			if(parse_float_parameter(argv[9], &limits.voltage_max) == 0){
				printf("Could not evaluate \"%s\" as a floating number\r\n", argv[9]);
				return 0;
			}
			job.direction = I2C_MASTER_DIR_MASTER_TO_SLAVE;
			memcpy(job.buffer, &limits, sizeof(dc_motor_controller_limits_t));
			job.num_bytes_to_transfer = sizeof(dc_motor_controller_limits_t);
			job.command += MOTOR_PROTOCOL_WRITE_LIMITS;
			job.callback = simple_success_callback;
		}
		else{
			printf("Invalid signature for motor limit, look at help motor\r\n");
			return 0;
		}
		
	}
	else if(strcmp("pid", argv[3])==0){
		if(argc < 5){
			printf("Please specify the PID controller that you are interested in\r\n");
			printf("  current\r\n");
			printf("  velocity\r\n");
			printf("  cascaded_velocity\r\n");
			printf("  cascaded_current\r\n");
			return 0;
		}
		uint8_t pid_controller_offset = 0;
		if(strcmp("current", argv[4]) == 0){
			pid_controller_offset = 0;
		}
		else if(strcmp("velocity", argv[4]) == 0){
			pid_controller_offset = 1;
		}
		else if(strcmp("position", argv[4]) == 0){
			pid_controller_offset = 2;
		}
		else {
			printf("Unrecognized PID controller name \"%s\"\r\n", argv[4]);
			return 0;
		}
		if(argc == 5){
			job.direction = I2C_MASTER_DIR_SLAVE_TO_MASTER;
			job.num_bytes_to_transfer = sizeof(sam5_control_settings_t);
			job.command += MOTOR_PROTOCOL_READ_CURRENT_CONTROLLER + pid_controller_offset;
			job.callback = controller_received_callback;
			job.period_ms = 0;
		}
		else if(argc == 7){
			sam5_control_settings_t settings;
			if(parse_float_parameter(argv[5], &settings.k_p) == 0){
				printf("Could not evaluate \"%s\" as a floating number\r\n", argv[5]);
				return 0;
		}
		if(parse_float_parameter(argv[6], &settings.k_i) == 0){
			printf("Could not evaluate \"%s\" as a floating number\r\n", argv[6]);
			return 0;
}
if(parse_float_parameter(argv[6], &settings.k_d) == 0){
	printf("Could not evaluate \"%s\" as a floating number\r\n", argv[6]);
	return 0;
}
if(parse_float_parameter(argv[6], &settings.k_d) == 0){
	printf("Could not evaluate \"%s\" as a floating number\r\n", argv[6]);
	return 0;
}
if(parse_float_parameter(argv[6], &settings.k_d) == 0){
	printf("Could not evaluate \"%s\" as a floating number\r\n", argv[6]);
	return 0;
}
if(parse_float_parameter(argv[6], &settings.k_d) == 0){
	printf("Could not evaluate \"%s\" as a floating number\r\n", argv[6]);
	return 0;
}
			job.direction = I2C_MASTER_DIR_MASTER_TO_SLAVE;
			memcpy(job.buffer, &settings, sizeof(sam5_control_settings_t));
			job.num_bytes_to_transfer = sizeof(sam5_control_settings_t);
			job.command += MOTOR_PROTOCOL_WRITE_CURRENT_CONTROLLER + pid_controller_offset;
			job.callback = simple_success_callback;
		}
		else{
			printf("Invalid signature for motor pid, look at help motor\r\n");
			return 0;
		}
	}
	else if(strcmp("inputs", argv[3]) == 0){
		job.direction = I2C_MASTER_DIR_SLAVE_TO_MASTER;
		job.num_bytes_to_transfer = sizeof(dc_motor_controller_inputs_t);
		job.command += MOTOR_PROTOCOL_READ_INPUT;
		job.callback = inputs_received_callback;
		job.period_ms = 0;
	}
	else if(strcmp("state", argv[3]) == 0){
		job.direction = I2C_MASTER_DIR_SLAVE_TO_MASTER;
		job.num_bytes_to_transfer = sizeof(dc_motor_controller_state_t);
		job.command += MOTOR_PROTOCOL_READ_STATE;
		job.callback = state_received_callback;
		job.period_ms = 0;
	}
	else if(strcmp("calib", argv[3]) == 0){
		job.direction = I2C_MASTER_DIR_SLAVE_TO_MASTER;
		job.num_bytes_to_transfer = sizeof(dc_motor_controller_calibration_result_t);
		job.command += MOTOR_PROTOCOL_READ_CALIBRATION_RESULT;
		job.callback = calibration_received_callback;
		job.period_ms = 0;
	}
	else {
		printf("Unrecognized command \"%s\" in motor\r\n", argv[3]);
		return 0;
	}
	// Return 1 if registering was successfull -> no automatic calling of sam5_usart_console_await_next_command()
	return sam5_i2c_master_register_com_job(&job) == 0 ? 1 : 0;
}


static void simple_success_callback(bool success, const sam5_i2c_master_com_job_t* job){
	if(success){
		printf("Transmission successful\r\n");
		}else{
		printf("Transmission failed\r\n");
	}
	sam5_usart_console_await_next_command();
}

static void identify_and_print_motor(const sam5_i2c_master_com_job_t *job){
	uint8_t motor_controller_index = job->slave_address - 40;
	uint8_t motor_index = MOTOR_PROTOCOL_GET_MOTR_INDEX(job->command);
	for(uint8_t i = 0; i < MAIN_CONTROL_NUM_MOTORS; ++i){
		if(g_main_control.motors[i].i2c_address == job->slave_address && g_main_control.motors[i].motor_index == motor_index){
			switch(i){
				case 0: printf("%d.%d Thumb   (Motor Controller %d - Motor %d)\r\n", motor_controller_index + 1, motor_index + 1, motor_controller_index + 1, motor_index + 1); break;
				case 1: printf("%d.%d Index   (Motor Controller %d - Motor %d)\r\n", motor_controller_index + 1, motor_index + 1, motor_controller_index + 1, motor_index + 1); break;
				case 2: printf("%d.%d Middle  (Motor Controller %d - Motor %d)\r\n", motor_controller_index + 1, motor_index + 1, motor_controller_index + 1, motor_index + 1); break;
				case 3: printf("%d.%d Ring    (Motor Controller %d - Motor %d)\r\n", motor_controller_index + 1, motor_index + 1, motor_controller_index + 1, motor_index + 1); break;
				case 4: printf("%d.%d Pinky   (Motor Controller %d - Motor %d)\r\n", motor_controller_index + 1, motor_index + 1, motor_controller_index + 1, motor_index + 1); break;
				case 5: printf("%d.%d Misc    (Motor Controller %d - Motor %d)\r\n", motor_controller_index + 1, motor_index + 1, motor_controller_index + 1, motor_index + 1); break;
				default: break;
			}
			return;
		}
	}
	printf("%d.%d Unknown (Motor Controller %d - Motor %d)\r\n", motor_controller_index, motor_index, motor_controller_index, motor_index);
}

static void limits_received_callback(bool success, const sam5_i2c_master_com_job_t* job){
	identify_and_print_motor(job);
	if(!success){
		printf("Transmission failed\r\n");
		sam5_usart_console_await_next_command();
		return;
	}
	dc_motor_controller_limits_t *limits = (dc_motor_controller_limits_t *)job->buffer;
	print_float("  angle_max: ", limits->angle_max, " [rad]\r\n");
	print_float("  angle_min: ", limits->angle_min, " [rad]\r\n");
	print_float("    vel_max: ", limits->velocity_max, " [rad/s]\r\n");
	print_float("curretn_max: ", limits->current_max, " [A]\r\n");
	print_float("voltage_max: ", limits->voltage_max, " [V]\r\n");
	sam5_usart_console_await_next_command();
}

static void controller_received_callback(bool success, const sam5_i2c_master_com_job_t* job){
	identify_and_print_motor(job);
	if(!success){
		printf("Transmission failed\r\n");
		sam5_usart_console_await_next_command();
		return;
	}
	sam5_control_settings_t *settings = (sam5_control_settings_t*)job->buffer;
	switch(job->command % MOTOR_PROTOCOL_NUM_COMMANDS_PER_MOTOR){
		case MOTOR_PROTOCOL_READ_CURRENT_CONTROLLER: printf("Current Controller\r\n"); break;
		case MOTOR_PROTOCOL_READ_VELOCITY_CONTROLLER: printf("Cascaded Velocity Controller\r\n"); break;
		case MOTOR_PROTOCOL_READ_POSITION_CONTROLLER: printf("Cascaded Current Controller\r\n"); break;
		default: printf("Unknown Controller - Results are probably wrong\r\n");
	}
	print_float(" K_p: ", settings->k_p, "\r\n");
	print_float(" K_i: ", settings->k_i, "\r\n");;
	sam5_usart_console_await_next_command();
}

static void inputs_received_callback(bool success, const sam5_i2c_master_com_job_t* job){
	identify_and_print_motor(job);
	if(!success){
		printf("Transmission failed\r\n");
		sam5_usart_console_await_next_command();
		return;
	}
	dc_motor_controller_inputs_t *inputs = (dc_motor_controller_inputs_t *)job->buffer;
	switch(inputs->flags.control_mode){
		case MOTOR_CM_STOPPED: printf("  Control Mode: MOTOR_STOPPED\r\n"); break;
		case MOTOR_CM_CALIB_PREP_POS: printf("  Control Mode: CALIB_PREP_POS\r\n"); break;
		case MOTOR_CM_CALIB_PREP_NEG: printf("  Control Mode: CALIB_PREP_NEG\r\n"); break;
		case MOTOR_CM_CALIBRATION: printf("  Control Mode: CALIBRATION\r\n"); break;
		case MOTOR_CM_VOLTAGE: printf("  Control Mode: VOLTAGE_CONTROL\r\n"); break;
		case MOTOR_CM_CURRENT: printf("  Control Mode: CURRENT_CONTROL\r\n"); break;
		case MOTOR_CM_VELOCITY: printf("  Control Mode: VELOCITY_CONTROL\r\n"); break;
		case MOTOR_CM_POSITION: printf("  Control Mode: POSITION_CONTROL\r\n"); break;
		default: printf("  Control Mode: UNKNOWN\r\n"); break;
	}
	printf("        At end: %s\r\n", inputs->flags.continue_at_end ? "continue" : "stop");
	printf("    At Contact: %s\r\n", inputs->flags.stop_at_contact ? "stop" : "continue");
	printf(" Contact Modus: %s\r\n", inputs->flags.use_aggressive_contact_detection ? "aggressive" : "normal");
	printf(" Endstop Usage: ");
	if(inputs->flags.use_endstop_for_end){
		printf("%s\r\n", inputs->flags.use_endstop_for_refine ? "Both" : "End only");
	}
	else{
		printf("%s\r\n", inputs->flags.use_endstop_for_refine ? "Refine only" : "Nothing");
	}
	print_float("         Angle: ", inputs->angle, " [rad]\r\n");
	print_float("      Velocity: ", inputs->velocity, " [rad/s]\r\n");
	print_float("       Current: ", inputs->current, " [A]\r\n");
	print_float("       Voltage: ", inputs->voltage, " [V]\r\n");
	print_float("Supply Voltage: ", bat_vltg, " [V]\r\n");
	sam5_usart_console_await_next_command();
}

static void state_received_callback(bool success, const sam5_i2c_master_com_job_t* job){
	CRITICAL_SECTION_ENTER()
	identify_and_print_motor(job);
	if(!success){
		printf("Transmission failed\r\n");
		sam5_usart_console_await_next_command();
		atomic_leave_critical(&__atomic);
		return;
	}
	dc_motor_controller_state_t *state = (dc_motor_controller_state_t *)job->buffer;
	
	print_float("      Angle: ", state->angle, " [rad]\r\n");
	print_float("   Velocity: ", state->velocity, "[rad/s]\r\n");
	print_float("    Current: ", state->current, "[A]\r\n");
	print_float("    Voltage: ", state->voltage, "[V]\r\n");
	print_float("       Temp: ", state->temperature, "[K]\r\n");
	printf("    Stopped: %s\r\n", state->flags_a.motor_stopped ? (state->flags_a.stopped_due_to_contact ? "yes (Caused by contact)" : "yes") : "no");
	printf("    Contact: %s\r\n", state->flags_a.contact_detected ? "yes" : "no");
	printf(" Target pos: %s\r\n", state->flags_a.target_angle_reached ? "reached" : "not reached");
	printf("    Pos end: %s\r\n", state->flags_a.max_angle_reached ? "reached" : "not reached");
	printf("    Neg end: %s\r\n", state->flags_a.min_angle_reached ? "reached" : "not reached");
	printf("    Endstop: %s\r\n", state->flags_b.endstop_active ? "active" : "inactive");
	printf("   Hardware: ");
	switch(state->flags_a.hardware_state){
		case 0x0: printf("Off\r\n"); break;
		case 0x1: printf("On\r\n"); break;
		case 0x2: printf("Error 1 (On expected but off detected)\r\n"); break;
		case 0x3: printf("Error 2 (Off expected but on detected)\r\n"); break;
		default: printf("Unknown\r\n"); break;
	}
	printf("   State: ");
	switch(state->flags_b.state_machine){
		case MOTOR_FSM_INIT: printf("Init\r\n"); break;
		case MOTOR_FSM_CALIBRATION: printf("Calibration\r\n"); break;
		case MOTOR_FSM_STOP: printf("Stop\r\n"); break;
		case MOTOR_FSM_OPEN_LOOP_CONTROL: printf("Open Loop\r\n"); break;
		case MOTOR_FSM_CLOSE_LOOP_CONTROL: printf("Closed Loop\r\n"); break;
		default: printf("Unknown\r\n"); break;
	}
	
	printf("Calib Stage: ");
	switch(state->flags_b.calib_stage){
		case CALIB_STAGE_WAITING: printf("Waiting\r\n"); break;
		case CALIB_STAGE_WAITING2: printf("Waiting 2\r\n"); break;
		case CALIB_STAGE_RIP_FREE: printf("�Rip free\r\n"); break;
		case CALIB_STAGE_POLARITY_NEG: printf("Polarity NEG\r\n"); break;
		case CALIB_STAGE_POLARITY_POS: printf("Polarity POS\r\n"); break;
		case CALIB_STAGE_ABS_END_NEG: printf("Absolute end NEG\r\n"); break;
		case CALIB_STAGE_ABS_END_POS: printf("Absolute end POS\r\n"); break;
		case CALIB_STAGE_NEG_TO_POS_PASS: printf("Neg to pos pass\r\n"); break;
		case CALIB_STAGE_POS_TO_NEG_PASS: printf("Pos to neg pass\r\n"); break;
		case CALIB_STAGE_GO_BACK_TO_START: printf("Go back to start\r\n"); break;
		case CALIB_STAGE_ERROR: printf("Error detected - use calib to see which\r\n");
		case CALIB_STAGE_FINISHED: printf("Finished\r\n"); break;
		default: printf("Unknown\r\n"); break;
	}
	
	sam5_usart_console_await_next_command();
	CRITICAL_SECTION_LEAVE()
}

static void calibration_received_callback(bool success, const sam5_i2c_master_com_job_t* job){
	CRITICAL_SECTION_ENTER()
	identify_and_print_motor(job);
	if(!success){
		printf("Transmission failed\r\n");
		sam5_usart_console_await_next_command();
		atomic_leave_critical(&__atomic);
		return;
	}
	dc_motor_controller_calibration_result_t *calib = (dc_motor_controller_calibration_result_t *)job->buffer;
	
	printf("Calibration:\r\n");
	printf(" Outputs: %s\r\n", calib->flags.flip_outputs ? "flipped" : "normal");
	printf(" Started: %s\r\n", calib->flags.started_calib_at_pos ? "at positive end" : "at negative end");
	if(calib->flags.searching_endstop){
		printf(" Endstop: Searching...\r\n");
	}
	else{
		printf(" Endstop: %s\r\n", calib->flags.endstop_at_pos ? "at positive end" : "at negative end");
		if(calib->flags.endstop_trig_calibrated){
			print_float("    Trig: ", calib->endstop_angle_trigger, "[rad]\r\n");
		}
		else{
			printf("    Trig: Uncalibrated\r\n");
		}
		if(calib->flags.endstop_untrig_calibrated){
			print_float("  Untrig: ", calib->endstop_angle_untrigger, "[rad]\r\n");
		}
		else{
			printf("  Untrig: Uncalibrated\r\n");
		}
	}
	printf("   Error: ");
	switch(calib->error_code){
		case CALIB_ERROR_NONE: printf("None\r\n"); break;
		case CALIB_ERROR_NO_MOVEMENT_MEASURED: printf("No movement measured\r\n"); break;
		case CALIB_ERROR_NO_CURRENT_MEASURED: printf("No current measured\r\n"); break;
		case CALIB_ERROR_DETECTED_SPEED_TOO_SMALL: printf("Measured Speed too small\r\n"); break;
		case CALIB_ERROR_ANGLE_RANGE_TOO_SMALL: printf("Angle range too small\r\n"); break;
	}
	
	sam5_usart_console_await_next_command();
	CRITICAL_SECTION_LEAVE()
}
