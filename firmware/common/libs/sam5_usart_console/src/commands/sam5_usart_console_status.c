#include "sam5_usart_console_commands.h"
#include "../sam5_usart_console_impl.h"
#include "sam5_bmp3_wrapper.h"
#include "main_control.h"

#include <stdio.h>
#include <string.h>

const char sam5_usart_console_status_desc[] = "show the status of certain sub systems";
const char sam5_usart_console_status_help[] =
"Usage:\r\n"
"status <subsystem>\r\n"
"  Print status of various subsystems\r\n"
"  Available subsystems are:\r\n"
"    bmp3  - Status of the BMP3XX pressure sensors in the fingertips and palm\r\n"
"    main  - Status of the main control loop\r\n"
"    motor - Status of the motors - Not implemented yet\r\n"
;

static void bmp3_simple_finished_callback();

uint8_t sam5_usart_console_status(const int argc, const char** argv){
	if(argc == 1){
		printf("Please specify subsystem you want to see the status of\r\n");
		printf("Use \"help status\" to see a list of available subsytems\r\n");
		return 0;
	}
	if(strcmp("bmp3", argv[1]) == 0){
		#if BMP3_AVAILABLE
		sam5_bmp3_wrapper_request_print_status(bmp3_simple_finished_callback);
		return 1;
		#else
		printf("BMP3 System was not activated at compile time - status request will be ignored\r\n");
		return 0;
		#endif
	}
	else if(strcmp("main", argv[1]) == 0){
		printf(" Motor Running: %s\r\n", main_control_control_flags.run_motor_controllers ? "Yes" : "No");
		printf("   Input Types:\r\n");
		for(uint8_t i = 0; i < CONTROL_NUM_INPUT_SOURCES; ++i){
			
			const uint8_t active = g_main_control.active_input == &g_main_control.inputs[i];
			switch(i){
				case CONTROL_INPUT_SOURCE_CONSOLE:	printf("   %c - Console: ", active ? 'X' : ' '); break;
				case CONTROL_INPUT_SOURCE_USART:	printf("   %c - USART  : ", active ? 'X' : ' '); break;
				case CONTROL_INPUT_SOURCE_EMG:		printf("   %c - EMG    : ", active ? 'X' : ' '); break;
				default:							printf("   %c - Unknown: ", active ? 'X' : ' '); break;
			}
			switch(g_main_control.inputs[i].type){
				case CONTROL_INPUT_TYPE_STOP: printf("Stop\r\n"); break;
				case CONTROL_INPUT_TYPE_GRASP: printf("Grasp\r\n"); break;
				case CONTROL_INPUT_TYPE_DIRECT: printf("Direct Finger\r\n"); break;
				case CONTROL_INPUT_TYPE_CALIB: printf("Calibrate\r\n"); break;
				default: printf("Unknown\r\n"); break;
			}
		}
		if(g_main_control.flags.doing_reset){
			printf("## Doing Reset - Forced Calibration Mode ##\r\n");
		}
		if(g_main_control.flags.doing_startup){
			printf("## Doing Startup - All set Inputs are ignored ##\r\n");
		}
		if(g_main_control.flags.grasp_executor_running){
			ASSERT(g_main_control.active_input != NULL);
			printf("## Grasp Executor Running ##\r\n");
			printf("Current State: ");
			const main_control_grasp_t * initial_grasp = main_control_get_grasp(g_main_control.grasp_executor.initial_grasp_index);
			if(initial_grasp != NULL){
				printf("Initial Grasp: %s\r\n", initial_grasp->name);
			}
			if (g_main_control.grasp_executor.active_grasp != NULL)
			{
				printf(" Active Grasp: %s\r\n", g_main_control.grasp_executor.active_grasp->name);
			}
			printf("        State: ");
			switch(g_main_control.grasp_executor.state){
				case CONTROL_GRASP_EXEC_STATE_INACTIVE: printf("Inactive\r\n"); break;
				case CONTROL_GRASP_EXEC_STATE_STOP: printf("Stop\r\n"); break;
				case CONTROL_GRASP_EXEC_STATE_PRE: printf("Pre Contact\r\n"); break;
				case CONTROL_GRASP_EXEC_STATE_POST: printf("Post Contact\r\n"); break;
				default: printf("Unknown\r\n"); break;
			}
			printf("Auto Continue: %s\r\n", g_main_control.active_input->grasp_no_auto_continue ? "No" : "Yes");
			printf("      Current: %d\r\n", g_main_control.active_input->grasp_contact_current_factor);
			printf("     Velocity: %d\r\n", g_main_control.active_input->grasp_speed_factor);
			printf("      Contact: ");
			for(uint8_t i = 0; i < MAIN_CONTROL_NUM_MOTORS; ++i){
				printf("%c, ", g_main_control.grasp_executor.contact_detected[i] ? '1' : '0');
			}
			printf("\r\n");
		}
		else if(g_main_control.flags.calib_executor_running){
			ASSERT(g_main_control.active_input != NULL);
			printf("## Calib Executor Running ##\r\n");
			printf("        State: ");
			switch(g_main_control.calib_executor.state){
				case CONTROL_CALIB_EXEC_STATE_INACTIVE: printf("Inactive\r\n"); break;
				case CONTROL_CALIB_EXEC_STATE_WAIT_BATVLT: printf("Wait for bat voltage measurement\r\n"); break;
				case CONTROL_CALIB_EXEC_STATE_PREPARE: printf("Prepare\r\n"); break;
				case CONTROL_CALIB_EXEC_STATE_GROUPS: printf("Groups\r\n"); break;
				case CONTROL_CALIB_EXEC_STATE_FINISHED: printf("Finished\r\n"); break;
				default: printf("Unknown\r\n"); break;
			}
		}
		printf(" Slave resets: %s\r\n", g_main_control.flags.accept_reset_requests ? "Will Execute on request" : "Requests rejected");
		printf("  All stopped: %s\r\n", g_main_control.flags.all_stopped_or_reached ? "Yes" : "No");
		
		return 0;
	}
	else if(strcmp("motor", argv[1]) == 0){
		return 0;
	}
	printf("Unrecognized status subsystem %s\r\n", argv[1]);
	printf("Use \"help status\" for usage tips\r\n");
	return 0;
}

static void bmp3_simple_finished_callback(){
	sam5_usart_console_await_next_command();
}
