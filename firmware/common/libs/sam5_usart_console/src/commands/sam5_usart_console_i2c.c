#include "sam5_usart_console_commands.h"
#include "../sam5_usart_console_impl.h"
#include "sam5_i2c_master.h"

#include <stdio.h>
#include <string.h>

const char sam5_usart_console_i2c_desc[] = 
"control and query information about I2C slaves"
;

const char sam5_usart_console_i2c_help[] =
"Usages:\r\n"
"i2c address <address1> <address2>\r\n"
"  Change the address of an I2C slave. This will work nearly instantaneously within only a few microseconds\r\n"
"  Make sure to not set duplicate addresses on the bus, this will cause problems\r\n"
"  If you are unsure there is something on the bus, you can use \"i2c test\" to exclude addresses\r\n"
"i2c test <address>\r\n"
"  Test the connection to an I2C slave by sending a byte and receiving the same byte again\r\n"
"  If the sent byte matches the received one this will print success\r\n"
"  Failure might be caused by a broken connection or a wrong address\r\n"
"i2c commands <address>\r\n"
"  Query the number of available I2C commands from a slave\r\n"
;


static void i2c_simple_success_callback(bool success, const uint8_t* ptr);
static void i2c_success_callback_print_uint8(bool success, const uint8_t* ptr);

uint8_t sam5_usart_console_i2c(const int argc, const char** argv){
	if(argc < 2){
		printf("At least one parameter is necessary - have a look at \"help i2c\"\r\n");
		return 0;
	}
	// i2c change_addr
	if(strcmp("address", argv[1]) == 0){
		if(argc != 4){
			printf("Did not pass 2 parameters to \"i2c address\"\r\n");
			return 0;
		}
		int32_t address1, address2;
		if(parse_integer_parameter(argv[2], &address1) == 0){
			printf("Could not evaluate \"%s\" as an I2C address\r\n", argv[2]);
			return 0;
		}
		if(parse_integer_parameter(argv[3], &address2) == 0){
			printf("Could not evaluate \"%s\" as an I2C address\r\n", argv[3]);
			return 0;
		}
		printf("Changing the address of slave %ld to %ld\r\n", address1, address2);
		if(sam5_i2c_master_change_slave_address((uint8_t) address1, (uint8_t) address2, i2c_simple_success_callback) != 0){
			printf("Invalid address - did not send\r\n");
			return 0;
		}
		// Return 1 if registering was successfull -> no automatic calling of sam5_usart_console_await_next_command()
		return 1;
	}
	// i2c test
	else if(strcmp("test", argv[1]) == 0){
		if(argc != 3){
			printf("Did not pass a parameter to \"i2c test\"\r\n");
			return 0;
		}
		int32_t address;
		if(parse_integer_parameter(argv[2], &address) == 0){
			printf("Could not evaluate \"%s\" as an I2C address\r\n", argv[2]);
			return 0;
		}
		printf("Testing Connection to slave %ld\r\n", address);
		if(sam5_i2c_master_test_connection((uint8_t) address, i2c_simple_success_callback) != 0){
			printf("Invalid address - did not send\r\n");
			return 0;
		}
		// Return 1 if registering was successfull -> no automatic calling of sam5_usart_console_await_next_command()
		return 1;
	}
	// i2c commands
	else if(strcmp("commands", argv[1]) == 0){
		if(argc != 3){
			printf("Did not pass a parameter to \"i2c commands\"\r\n");
			return 0;
		}
		int32_t address;
		if(parse_integer_parameter(argv[2], &address) == 0){
			printf("Could not evaluate \"%s\" as an I2C address\r\n", argv[2]);
			return 0;
		}
		printf("Querying num commands of slave %ld\r\n", address);
		if(sam5_i2c_master_query_num_commands((uint8_t) address, i2c_success_callback_print_uint8) != 0){
			printf("Invalid address - did not send\r\n");
			return 0;
		}
		// Return 1 if registering was successfull -> no automatic calling of sam5_usart_console_await_next_command()
		return 1;
	}
	else{
		printf("Unrecognized action \"%s\"\r\n - Aborting", argv[1]);
		return 0;
	}
}

static void i2c_simple_success_callback(bool success, const uint8_t* ptr){
	if(success)
	{
		printf("Success\r\n");
	}
	else
	{
		printf("Failure\r\n");
	}
	sam5_usart_console_await_next_command();
}

static void i2c_success_callback_print_uint8(bool success, const uint8_t* ptr){
	if(success)
	{
		printf("Received: %u\r\n", *ptr);
	}
	else
	{
		printf("Failure\r\n");
	}
	sam5_usart_console_await_next_command();
}