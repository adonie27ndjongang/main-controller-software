#include "sam5_usart_console_commands.h"
#include "../sam5_usart_console_impl.h"
#include "main_control.h"
#include "motor_protocol.h"



#include <stdio.h>
#include <string.h>


const char sam5_usart_console_grasp_desc[] = "control grasps of the hand";
const char sam5_usart_console_grasp_help[] =
"Usages:\r\n"
"grasp list\r\n"
"  List all available grasps and how they interconnect\r\n"
"\r\n"
"grasp <index>\r\n"
"  Execute a grasp with the configured velocity, current and auto continue\r\n"
"  <index> Index of the grasp as shown in \"grasp list\"\r\n"
"\r\n"
"grasp <index> <velocity> <current>\r\n"
"  Execute a grasp with the auto continue, and temporary velocity and current \r\n"
"  <index> Index of the grasp as shown in \"grasp list\"\r\n"
"  <velocity> between 0 and 100\r\n"
"  <current> between 0 and 100\r\n"
"\r\n"
"grasp config <vel, cur, auto> <value>\r\n"
"  Configure parameters \r\n"
"  <value> In [0-100], [0-100], [bool] respectively\r\n"
"\r\n"
"grasp config\r\n"
"  See the current configuration\r\n"
"\r\n"
"grasp reset\r\n"
"  Change configurable options back to defaults\r\n"
"\r\n"
;

static main_control_input_t grasp_input;
static uint8_t configured_vel = 25;
static uint8_t configured_cur = 25;
static uint8_t configured_auto_continue = 1;

uint8_t sam5_usart_console_grasp(const int argc, const char** argv){
	if(argc == 1){
		printf("Invalid syntax - check \"help grasp\"\r\n");
		return 0;
	}
	if(strcmp("list", argv[1]) == 0){
		for(uint8_t i = 0; i < main_control_num_grasps; ++i){
			const main_control_grasp_t * grasp = main_control_get_grasp(i);
			printf("%d:\t%s", i, grasp->name);
			for(uint8_t j = 0; j < 15; ++j){
				if(grasp->following_grasp != NULL){
					printf(" -> %s", grasp->following_grasp->name);
					grasp = grasp->following_grasp;
				}
				else{
					printf("\r\n");
					break;
				}
			}
		}
		return 0;
	}
	if(strcmp("reset", argv[1]) == 0){
		configured_vel = 25;
		configured_cur = 25;
		configured_auto_continue = 1;
		return 0;
	}
	if(strcmp("config", argv[1]) == 0){
		if(argc < 4){
			printf("Configured values:\r\n");
			printf("       Velocity: %d [0-100]\r\n", configured_vel);
			printf("        Current: %d [0-100]\r\n", configured_cur);
			printf("  Auto Continue: %s\r\n", configured_auto_continue ? "yes" : "no");
			return 0;
		}
		int32_t value_d;
		uint8_t value_b;
		if(strcmp("vel", argv[2]) == 0){
			if(parse_integer_parameter(argv[3], &value_d) == 0){
				printf("Could not parse value to configure from \"%s\"\r\n", argv[3]);
				return 0;
			}
			if(value_d < 0 || value_d >= MAIN_CONTROL_SPEED_FACTOR_NUM_STEPS){
				printf("Warning! - Velocity out of bounds - shall be within [0, %d]\r\n", MAIN_CONTROL_SPEED_FACTOR_NUM_STEPS - 1);
			}
			configured_vel = value_d;
		}
		else if(strcmp("cur", argv[2]) == 0){
			if(parse_integer_parameter(argv[3], &value_d) == 0){
				printf("Could not parse value to configure from \"%s\"\r\n", argv[3]);
				return 0;
			}
			if(value_d < 0 || value_d >= MAIN_CONTROL_CURRENT_FACTOR_NUM_STEPS){
				printf("Warning! - Current out of bounds - shall be within [0, %d]\r\n", MAIN_CONTROL_CURRENT_FACTOR_NUM_STEPS - 1);
			}
			configured_cur = value_d;
		}
		else if(strcmp("auto", argv[2]) == 0){
			if(parse_bool_parameter(argv[3], &value_b) == 0){
				printf("Could not parse value to configure from \"%s\"\r\n", argv[3]);
				return 0;
			}
			configured_auto_continue = value_b;
		}
		else {
			printf("\"%s\" is not a value that can be configured\r\n", argv[2]);
		}
		return 0;
	}
	int32_t grasp_index;
	if(parse_integer_parameter(argv[1], &grasp_index) == 0){
		printf("Could not parse grasp index from \"%s\"\r\n", argv[1]);
		return 0;
	}
	if(grasp_index < 0 || main_control_num_grasps <= grasp_index){
		printf("Grasp index must be within [0 - %lu]\r\n", main_control_num_grasps);
		return 0;
	}
	grasp_input.type = CONTROL_INPUT_TYPE_GRASP;
	grasp_input.grasp_index = grasp_index;
	grasp_input.grasp_speed_factor = configured_vel;
	grasp_input.grasp_contact_current_factor = configured_cur;
	grasp_input.grasp_no_auto_continue = configured_auto_continue == 0;
	
	if(argc >= 4){
		int32_t temp_vel;
		int32_t temp_cur;
		if(parse_integer_parameter(argv[2], &temp_vel) == 0){
			printf("Could not parse value to configure from \"%s\"\r\n", argv[2]);
			return 0;
		}
		if(temp_vel < 0 || temp_vel >= MAIN_CONTROL_SPEED_FACTOR_NUM_STEPS){
			printf("Warning! - Velocity out of bounds - shall be within [0, %d]\r\n", MAIN_CONTROL_SPEED_FACTOR_NUM_STEPS - 1);
		}
		if(parse_integer_parameter(argv[3], &temp_cur) == 0){
			printf("Could not parse value to configure from \"%s\"\r\n", argv[3]);
			return 0;
		}
		if(temp_cur < 0 || temp_cur >= MAIN_CONTROL_CURRENT_FACTOR_NUM_STEPS){
			printf("Warning! - Current out of bounds - shall be within [0, %d]\r\n", MAIN_CONTROL_CURRENT_FACTOR_NUM_STEPS - 1);
		}
		grasp_input.grasp_speed_factor = temp_vel;
		grasp_input.grasp_contact_current_factor = temp_cur;
	}

	main_control_set_input(CONTROL_INPUT_SOURCE_CONSOLE, &grasp_input);
	return 0;
}