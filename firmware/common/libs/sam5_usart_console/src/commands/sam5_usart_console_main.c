#include "sam5_usart_console_commands.h"
#include "../sam5_usart_console_impl.h"
#include "main_control.h"

#include <stdio.h>
#include <string.h>


const char sam5_usart_console_main_desc[] = "control the main loop and the grasps performed by the hand";
const char sam5_usart_console_main_help[] =
"Usages:\r\n"
"main <input_type>\r\n"
"  Set the input type to one of the following values: console, usart, emg\r\n"
"\r\n"
"main stop\r\n"
"  Will switch to manual mode and set the grasp to 0\r\n"
"\r\n"
"main reset <bool>\r\n"
"  Change if reset will actually be executed - set this to false if you want to flash the motor controllers\r\n"
;

uint8_t sam5_usart_console_main(const int argc, const char** argv){
	if(argc < 2){
		printf("At least one parameter is necessary - have a look at \"help main\"\r\n");
		return 0;
	}
	main_control_input_t input;
	// main manual
	if(strcmp("console", argv[1]) == 0){
		main_control_set_active_input(CONTROL_INPUT_SOURCE_CONSOLE);
		return 0;
	}
	// main usart
	else if(strcmp("usart", argv[1]) == 0){
		main_control_set_active_input(CONTROL_INPUT_SOURCE_USART);
		return 0;
	}
	// main emg
	else if(strcmp("emg", argv[1]) == 0){
		main_control_set_active_input(CONTROL_INPUT_SOURCE_EMG);
		return 0;
	}
	// main stop
	else if(strcmp("stop", argv[1]) == 0){
		input.type = CONTROL_INPUT_TYPE_STOP;
		main_control_set_input(CONTROL_INPUT_SOURCE_CONSOLE, &input);
		main_control_set_active_input(CONTROL_INPUT_SOURCE_CONSOLE);
		return 0;
	}
	if(argc < 3){
		printf("Unrecognized command signature - have a  look at \"help main\"\r\n");
		return 0;
	}
	// main reset <bool>
	if(strcmp("reset", argv[1]) == 0){
		uint8_t do_reset;
		if(parse_bool_parameter(argv[2], &do_reset) == 0){
			printf("Could not parse do_reset from \"%s\"\r\n", argv[2]);
			return 0;
		}
		g_main_control.flags.accept_reset_requests = do_reset;
		return 0;
	}
	if(g_main_control.active_input != &g_main_control.inputs[CONTROL_INPUT_SOURCE_CONSOLE]){
		printf("Warning: Console control is inactive, the changes will be applied and get active as soon as console control is entered.\r\n");
	}
	
	return 0;
}

