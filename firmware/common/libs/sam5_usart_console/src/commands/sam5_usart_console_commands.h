#ifndef _SAM5_USART_CONSOLE_COMMANDS_H_
#define _SAM5_USART_CONSOLE_COMMANDS_H_

#include <stdint.h>

extern const char sam5_usart_console_help_desc[];
extern const char sam5_usart_console_help_help[];
uint8_t sam5_usart_console_help(const int argc, const char** argv);

extern const char sam5_usart_console_reset_desc[];
extern const char sam5_usart_console_reset_help[];
uint8_t sam5_usart_console_reset(const int argc, const char** argv);

extern const char sam5_usart_console_s_desc[];
extern const char sam5_usart_console_s_help[];
uint8_t sam5_usart_console_s(const int argc, const char** argv);

extern const char sam5_usart_console_stop_desc[];
extern const char sam5_usart_console_stop_help[];
uint8_t sam5_usart_console_stop(const int argc, const char** argv);

extern const char sam5_usart_console_status_desc[];
extern const char sam5_usart_console_status_help[];
uint8_t sam5_usart_console_status(const int argc, const char** argv);

extern const char sam5_usart_console_bmp3_desc[];
extern const char sam5_usart_console_bmp3_help[];
uint8_t sam5_usart_console_bmp3(const int argc, const char** argv);

extern const char sam5_usart_console_i2c_desc[];
extern const char sam5_usart_console_i2c_help[];
uint8_t sam5_usart_console_i2c(const int argc, const char** argv);

extern const char sam5_usart_console_main_desc[];
extern const char sam5_usart_console_main_help[];
uint8_t sam5_usart_console_main(const int argc, const char** argv);

extern const char sam5_usart_console_finger_desc[];
extern const char sam5_usart_console_finger_help[];
uint8_t sam5_usart_console_finger(const int argc, const char** argv);

extern const char sam5_usart_console_grasp_desc[];
extern const char sam5_usart_console_grasp_help[];
uint8_t sam5_usart_console_grasp(const int argc, const char** argv);

extern const char sam5_usart_console_motor_desc[];
extern const char sam5_usart_console_motor_help[];
uint8_t sam5_usart_console_motor(const int argc, const char** argv);

#endif //_SAM5_USART_CONSOLE_COMMANDS_H_

//sam5_usart_console_register_command("bmp3", "search for not connected sensors and initialize them", sam5_usart_console_bmp3);
