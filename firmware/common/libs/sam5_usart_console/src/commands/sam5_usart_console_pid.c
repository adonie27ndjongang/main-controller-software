#include "sam5_usart_console_commands.h"
#include "../sam5_usart_console_impl.h"
#include "sam5_i2c_master.h"
#include "motor_protocol.h"
#include "main_control.h"
#include "sam5_pid_tuner.h"

#include <stdio.h>
#include <string.h>

const char sam5_usart_console_pid_desc[] = "calibrate PID controllers of the motors";
const char sam5_usart_console_pid_help[] =
"Usages:\r\n"
"pid off\r\n"
"  Switch PID tuner off (same as pid mode off)\r\n"
"pid mode [off, current, position]\r\n"
"  Select which pid controller to tune\r\n"
"  You can also use \"cur\" and \"pos\" as shorthand versions\r\n"
"pid amplitude [small, medium, big]"
"  Select between the absolute amplitude the setpoint will vary\r\n"
"  Amplitude:  Small Medium   Big"
"  Current:     0.05   0.10  0.20 Ampere\r\n"
"  Velocity:       5     10    20 rad/s\r\n"
"  Position:      10     20    24 rad\r\n"
"pid motor <motor_controller> <motor>\r\n"
"  Specify the motor that will be moving by it's motor controller index and motor index\r\n"
;

uint8_t sam5_usart_console_pid(const int argc, const char** argv){
	if(strcmp("off", argv[1]) == 0){
		sam5_pid_tuner_set_mode(SAM5_PID_TUNER_MODE_OFF);
	}
	else if(strcmp("motor", argv[1]) == 0 && argc == 4){
		
		int32_t motor_controller_index, motor_index;
		if(parse_integer_parameter(argv[2], &motor_controller_index) == 0){
			printf("Could not parse motor_controller index from \"%s\"\r\n", argv[2]);
			return 0;
		}
		if(parse_integer_parameter(argv[3], &motor_index) == 0){
			printf("Could not parse motor index from \"%s\"\r\n", argv[3]);
			return 0;
		}
		// Transform between the counting printed on PCBs that start at 1 to the software representation that starts counting at 0
		--motor_controller_index;
		--motor_index;
		if(sam5_pid_tuner_set_active_motor(motor_controller_index, motor_index) == 1){
			printf("Unknown motor %ld.%ld\r\n",  motor_controller_index + 1, motor_index + 1);
		}
	}
	else if((strcmp("amplitude", argv[1]) == 0 || strcmp("ampl", argv[1]) == 0) && argc == 3){
		if(strcmp("small", argv[2]) == 0){
			sam5_pid_tuner_set_amplitude(SAM5_PID_TUNER_AMPLITUDE_SMALL);
		}
		else if(strcmp("medium", argv[2]) == 0){
			sam5_pid_tuner_set_amplitude(SAM5_PID_TUNER_AMPLITUDE_MEDIUM);
		}
		else if(strcmp("big", argv[2]) == 0){
			sam5_pid_tuner_set_amplitude(SAM5_PID_TUNER_AMPLITUDE_BIG);
		}
		else {
			printf("Unrecognized amplitude \"%s\"", argv[2]);
		}
	}
	else if(strcmp("mode", argv[1]) == 0 && argc == 3){
		if(strcmp("off", argv[2]) == 0){
			sam5_pid_tuner_set_mode(SAM5_PID_TUNER_MODE_OFF);
		}
		else if((strcmp("current", argv[2]) == 0) || (strcmp("cur", argv[2]) == 0)){
			sam5_pid_tuner_set_mode(SAM5_PID_TUNER_MODE_CURRENT);
		}
		else if((strcmp("position", argv[2]) == 0) || (strcmp("pos", argv[2]) == 0)){
			sam5_pid_tuner_set_mode(SAM5_PID_TUNER_MODE_POSITION);
		}
		else {
			printf("Unrecognized mode \"%s\"\r\n", argv[2]);
		}
	}
	else{
		printf("Unrecognized signature, look at \"help pid\" for more information\r\n");
	}
	return 0;
}