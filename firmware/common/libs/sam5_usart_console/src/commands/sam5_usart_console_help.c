#include "sam5_usart_console_commands.h"
#include "../sam5_usart_console_impl.h"

#include <stdio.h>
#include <string.h>

const char sam5_usart_console_help_desc[] = "provide help for the user";
const char sam5_usart_console_help_help[] =
"Usage:\r\n"
" help           - Show this message\r\n"
" help list      - List all available commands\r\n"
" help [command] - Show help to the specified command\r\n"
;

uint8_t sam5_usart_console_help(const int argc, const char** argv){
	printf("\r\n");
	if(argc == 1){
		printf(sam5_usart_console_help_help);
	}
	else if(strcmp("list", argv[1]) == 0){
		uint32_t longest_name_len = 0;
		for(uint32_t i = 0; i < g_num_commands; ++i){
			const uint32_t len = strlen(g_commands[i].name);
			if(len > longest_name_len){
				longest_name_len = len;
			}
		}
		for(uint32_t i = 0; i < g_num_commands; ++i){
			printf("  %s", g_commands[i].name);
			for(uint32_t j = strlen(g_commands[i].name); j < longest_name_len; ++j){
				printf(" ");
			}
			printf(" - %s\r\n", g_commands[i].description);
		}
	}
	else {
		for(uint32_t i = 0; i < g_num_commands; ++i){
			if(strcmp(g_commands[i].name, argv[1]) == 0){
				printf(g_commands[i].help_text);
				return 0;
			}
		}
		printf("Unrecognized command %s\r\n", argv[1]);
		printf("Please use help list to see available commands \r\n");
		return 0;
	}
	return 0;
}
