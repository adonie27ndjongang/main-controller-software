/*
* main_control.c
*
* Created: 25.05.2024 12:20:01
*  Author: Erwan Ndjongang
*/
#include "sam5_control_interface_emg.h"


//First thing is to understand the files main_control_grasp_executor.c and main_control_graps.c
//After try to set the hand to a grasp position: the UART grasp control should help me
//deactivate 4 fingers, to only work with one finger

/**********Variables*********************/
static main_control_input_t grasping_input;
uint8_t debug_index = 0;
uint8_t debug_grasp_sf = 0;

static main_control_input_t grasp_input;
static uint8_t configured_vel = 25;
static uint8_t configured_cur = 25;
static uint8_t configured_auto_continue = 0;
static main_control_input_t finger_input;



/**********Functions**************************/
/**  @brief: activate_grasp_from_EMG
    @param[in]: Grasp index, speed_factor
	@param[out]: None
*/
void activate_grasp_from_EMG(uint8_t grasp_index, float32_t grasp_speed_factor){
	

	//The type of the grasp	
	#ifndef ERWAN_TEST
	grasping_input.type = CONTROL_INPUT_TYPE_GRASP;
	
	//grasp attributes
	grasping_input.grasp_index = grasp_index;
	grasping_input.grasp_speed_factor = 25;
	grasping_input.grasp_contact_current_factor = configured_cur;
	grasping_input.grasp_no_auto_continue = configured_auto_continue;
	
	bool speed = (grasp_speed_factor > 1) && (grasp_speed_factor < 0);
	bool index_grasp = grasp_index == g_main_control.active_input->grasp_index;
	//if(grasping_input->grasp_index != g_main_control.active_input->grasp_index){}
	if(speed || index_grasp){}
	
	else{
		
	
		main_control_set_input(CONTROL_INPUT_SOURCE_EMG, &grasping_input);
	}
	#endif
	//Caveats: discuss it with Michael: I think the control loop will execute the grasping, am i wrong? The outbounds should not be checkk, since only precised command are sent from the EMG
}

