#ifndef SAM5_USART_CONTROL_INTERFACE_IMPL_H_
#define SAM5_USART_CONTROL_INTERFACE_IMPL_H_

#include "sam5_usart_control_interface.h"


typedef enum {
	CONTROL_REQ_NONE = 0,
	CONTROL_REQ_READ_TACTILE = 0x1,
	CONTROL_REQ_READ_CURRENT = 0x2,
	CONTROL_REQ_READ_ANGLE = 0x3,
	CONTROL_REQ_GRASP_BASE = 0xA,
	CONTROL_REQ_OPEN_ALL = 0x1F,
} sam5_usart_control_request_type_e;


#define CONTROL_REQ_RX(type) (0xE0 | type)
#define CONTROL_REQ_TX(type) (0xA0 | type)

#endif //SAM5_USART_CONTROL_INTERFACE_H_