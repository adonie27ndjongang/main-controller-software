extern "C" {
#include "sam5_sw_crc8.h"
}

#include <gtest/gtest.h>
#include <stdint.h>
#include <stdio.h>

const uint8_t data[9] = {'1', '2', '3', '4', '5', '6', '7', '8', '9'};
const uint8_t zeroData[20] = {0};

void printByte(uint8_t byte) {
	for (int8_t i = 7; i >= 0; --i) {
		if ((byte >> i) & 1) {
			printf("1");
		} else {
			printf("0");
		}
	}
}

void printBytes(const uint8_t *bytes, uint32_t length, bool binary = false) {
	for (uint32_t i = 0; i < length; ++i) {
		if (binary)
			printByte(bytes[i]);
		else
			printf("%02x", bytes[i]);
		printf(" ");
	}
	printf("\r\n");
}

TEST(ErrorDetectionTest, AlgorithmCorrectness) {
	EXPECT_EQ(0xfb, sam5_sw_crc8_CRC(data, 9));
}

TEST(ErrorDetectionTest, TableCorrectness) {
	uint8_t polynom = 0x07;
	for (uint32_t i = 0; i < 256; ++i) {
		uint8_t crc = (uint8_t)i;
		for (uint32_t bit = 0; bit < 8; ++bit) {
			if ((crc & 0x80) != 0) {
				crc <<= 1;
				crc ^= polynom;
			} else {
				crc <<= 1;
			}
		}
		EXPECT_EQ(crc, sam5_sw_crc8_CrcTable[i]);
		EXPECT_EQ(crc, sam5_sw_crc8_CRC_continue((uint8_t *)&i, 1, 0));
	}
}

TEST(ErrorCorrectionTest, General) {
	constexpr uint8_t maxNumBytes = 14;
	uint8_t data[maxNumBytes] = {0};
	uint8_t tested[256];
	for (uint32_t i = 0; i < 256; ++i) {
		tested[i] = 0;
	}
	for (uint8_t i = 0; i < maxNumBytes * 8; ++i) {
		for (uint8_t j = 0; j < maxNumBytes; ++j) {
			data[j] = 0;
		}
		uint8_t byte = i / 8;
		uint8_t bit = i % 8;
		data[byte] = 1 << bit;
		uint8_t crcFlipped = sam5_sw_crc8_CRC(data, maxNumBytes);
		uint8_t crcOrigin = sam5_sw_crc8_CRC(zeroData, maxNumBytes);
		uint8_t crc = crcFlipped ^ crcOrigin;
		tested[crc] = 1;
		uint8_t bitPos = sam5_sw_crc8_EccTable[crc];

		EXPECT_LT(bitPos, maxNumBytes * 8);
		EXPECT_EQ(byte, bitPos / 8);
		EXPECT_EQ(bit, bitPos % 8);
	}
	for (uint32_t i = 0; i < 256; ++i) {
		if (tested[i] == 1)
			continue;
		if (i == 0 | i == 0b1 | i == 0b10 | i == 0b100 | i == 0b1000 | i == 0b10000 |
			i == 0b100000 | i == 0b1000000 | i == 0b10000000)
			EXPECT_EQ(sam5_sw_crc8_EccTable[i], 0xF0);
		else {
			EXPECT_EQ(sam5_sw_crc8_EccTable[i], 0xFF);
		}
	}
}

void fillTestData(uint8_t *data, uint32_t length, uint8_t value, uint8_t modifier) {
	for (uint32_t i = 0; i < length; ++i) {
		data[i] = value + i * modifier;
	}
}

void flipBit(uint8_t *data, uint32_t bit) {
	uint8_t bytePos = bit / 8;
	uint8_t bitPos = bit % 8;
	data[bytePos] ^= 1 << bitPos;
}

TEST(ErrorCorrectionTest, SingleBitDataFlip) {
	uint8_t testData[20] = {0};
	for (uint32_t length = 1; length < 20; ++length) {
		for (uint32_t bit = 0; bit < length * 8; ++bit) {
			fillTestData(testData, length, 0x31, bit);
			uint8_t crc = sam5_sw_crc8_CRC(testData, length);
			flipBit(testData, bit);
			uint8_t flippedBit = testData[bit / 8];

			if (length <= 14) {
				EXPECT_EQ(crc, sam5_sw_crc8_Applied(testData, length, crc));
				EXPECT_EQ(flippedBit ^ testData[bit / 8], 1 << bit % 8);
			} else {
				EXPECT_EQ(0, sam5_sw_crc8_Applied(testData, length, crc));
			}
		}
	}
}

TEST(ErrorCorrectionTest, SingleBitHeaderFlip) {
	uint8_t testData[20] = {0};
	for (uint32_t length = 1; length < 20; ++length) {
		for (uint32_t bit = 0; bit < 8; ++bit) {
			fillTestData(testData, length, 0xb5, bit);
			uint8_t crc = sam5_sw_crc8_CRC(testData, length);
			uint8_t crcFlipped = crc;
			flipBit(&crcFlipped, bit);

			if (length <= 14) {
				EXPECT_EQ(crc, sam5_sw_crc8_Applied(testData, length, crcFlipped));
			} else {
				EXPECT_EQ(0, sam5_sw_crc8_Applied(testData, length, crcFlipped));
			}
		}
	}
}

TEST(ErrorCorrectionTest, DoubleBitDataFlip) {
	uint8_t testData[20] = {0};
	for (uint32_t length = 1; length < 20; ++length) {
		for (uint32_t bit = 1; bit < length * 8; ++bit) {
			fillTestData(testData, length, 0xf0, bit);
			uint8_t crc = sam5_sw_crc8_CRC(testData, length);
			flipBit(testData, bit - 1);
			flipBit(testData, bit);
			EXPECT_EQ(0, sam5_sw_crc8_Applied(testData, length, crc));
		}
	}
}

TEST(ErrorCorrectionTest, DoubleBitHeaderFlip) {
	uint8_t testData[20] = {0};
	for (uint32_t length = 1; length < 20; ++length) {
		for (uint32_t bit = 1; bit < 8; ++bit) {
			fillTestData(testData, length, 0xf0, bit);
			uint8_t crc = sam5_sw_crc8_CRC(testData, length);
			flipBit(&crc, bit - 1);
			flipBit(&crc, bit);
			EXPECT_EQ(0, sam5_sw_crc8_Applied(testData, length, crc));
		}
	}
}

TEST(ErrorCorrectionTest, DoubleBitBothFlip) {
	uint8_t testData[20] = {0};
	for (uint32_t length = 1; length <= 14; ++length) {
		for (uint32_t dataBit = 0; dataBit < length * 8; ++dataBit) {
			for (uint32_t headerBit = 0; headerBit < 8; ++headerBit) {
				fillTestData(testData, length, headerBit, dataBit);
				uint8_t crc = sam5_sw_crc8_CRC(testData, length);
				flipBit(&crc, headerBit);
				flipBit(testData, dataBit);
				EXPECT_EQ(0, sam5_sw_crc8_Applied(testData, length, crc));
			}
		}
	}
}
