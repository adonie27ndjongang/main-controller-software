#include "main_control_impl.h"
#include <stdio.h>


static const main_control_grasp_t main_control_grasps[] = {
	//########################################################
	{	// 0
		.name = "Open A",
		.target_position = {
			0.0,	// Thumb
			0.0,	// Index
			0.0,	// Middle
			0.0,	// Ring
			0.0,	// Pinky
			0.0,	// Thumb Abduction
		},
		.speed_factors = {
			1.0,	// Thumb
			1.0,	// Index
			1.0,	// Middle
			1.0,	// Ring
			1.0,	// Pinky
			1.0,	// Thumb Abduction
		},
		.following_grasp = NULL,
		.flags = {
			.push_after_contact = 0,
			.use_aggressive_contact_detection = 1,
		},
	},
	{	// 1
		.name = "Open B",
		.target_position = {
			0.0,	// Thumb
			0.0,	// Index
			0.0,	// Middle
			0.0,	// Ring
			0.0,	// Pinky
			1.0,	// Thumb Abduction
		},
		.speed_factors = {
			1.0,	// Thumb
			1.0,	// Index
			1.0,	// Middle
			1.0,	// Ring
			1.0,	// Pinky
			1.0,	// Thumb Abduction
		},
		.following_grasp = NULL,
		.flags = {
			.push_after_contact = 0,
			.use_aggressive_contact_detection = 1,
		},
	},
	//########################################################
	{	// 2
		.name = "Pinch Prep",
		.target_position = {
			0.0,	// Thumb
			0.0,	// Index
			0.0,	// Middle
			0.0,	// Ring
			0.0,	// Pinky
			0.2,	// Thumb Abduction
		},
		.speed_factors = {
			1.0,	// Thumb
			1.0,	// Index
			1.0,	// Middle
			1.0,	// Ring
			1.0,	// Pinky
			1.0,	// Thumb Abduction
		},
		.following_grasp = &main_control_grasps[3],
		.flags = {
			.use_aggressive_contact_detection = 1,
		},
	},
	{	// 3
		.name = "Pinch",
		.target_position = {
			0.5,	// Thumb
			0.45,	// Index
			0.0,	// Middle
			0.0,	// Ring
			0.0,	// Pinky
			0.0,	// Thumb Abduction
		},
		.speed_factors = {
			1.0,	// Thumb
			1.0,	// Index
			1.0,	// Middle
			1.0,	// Ring
			1.0,	// Pinky
			0.0,	// Thumb Abduction
		},
		.following_grasp = NULL,
		.flags = {
			.push_after_contact = 1,
			.use_aggressive_contact_detection = 0,
		},
	},
	//########################################################
	{	// 4
		.name = "Kapandji 1 Prep",
		.target_position = {
			0.0,	// Thumb
			0.0,	// Index
			0.0,	// Middle
			0.0,	// Ring
			0.0,	// Pinky
			0.50,	// Thumb Abduction
		},
		.speed_factors = {
			1.0,	// Thumb
			1.0,	// Index
			1.0,	// Middle
			1.0,	// Ring
			1.0,	// Pinky
			1.0,	// Thumb Abduction
		},
		.following_grasp = &main_control_grasps[5],
		.flags = {
			.use_aggressive_contact_detection = 1,
		},
	},
	{	// 5
		.name = "Kapandji 1",
		.target_position = {
			0.9,	// Thumb
			0.35,	// Index
			0.0,	// Middle
			0.0,	// Ring
			0.0,	// Pinky
			0.0,	// Thumb Abduction
		},
		.speed_factors = {
			1.0,	// Thumb
			1.0,	// Index
			0.0,	// Middle
			0.0,	// Ring
			0.0,	// Pinky
			0.0,	// Thumb Abduction
		},
		.following_grasp = NULL,
		.flags = {
			.push_after_contact = 0,
			.use_aggressive_contact_detection = 1,
		},
	},
	//########################################################
	{	// 6
		.name = "Kapandji 2 Prep",
		.target_position = {
			0.0,	// Thumb
			0.0,	// Index
			0.0,	// Middle
			0.0,	// Ring
			0.0,	// Pinky
			0.3,	// Thumb Abduction
		},
		.speed_factors = {
			1.0,	// Thumb
			1.0,	// Index
			1.0,	// Middle
			1.0,	// Ring
			1.0,	// Pinky
			1.0,	// Thumb Abduction
		},
		.following_grasp = &main_control_grasps[7],
		.flags = {
			.use_aggressive_contact_detection = 1,
		},
	},
	{	// 7
		.name = "Kapandji 2",
		.target_position = {
			0.7,	// Thumb
			0.45,	// Index
			0.0,	// Middle
			0.0,	// Ring
			0.0,	// Pinky
			0.0,	// Thumb Abduction
		},
		.speed_factors = {
			1.0,	// Thumb
			0.6,	// Index
			0.0,	// Middle
			0.0,	// Ring
			0.0,	// Pinky
			0.0,	// Thumb Abduction
		},
		.following_grasp = NULL,
		.flags = {
			.push_after_contact = 0,
			.use_aggressive_contact_detection = 1,
		},
	},
	//########################################################
	{	// 8
		.name = "Kapandji 3 Prep",
		.target_position = {
			0.0,	// Thumb
			0.0,	// Index
			0.0,	// Middle
			0.0,	// Ring
			0.0,	// Pinky
			0.22,	// Thumb Abduction
		},
		.speed_factors = {
			1.0,	// Thumb
			1.0,	// Index
			1.0,	// Middle
			1.0,	// Ring
			1.0,	// Pinky
			1.0,	// Thumb Abduction
		},
		.following_grasp = &main_control_grasps[9],
		.flags = {
			.use_aggressive_contact_detection = 1,
		},
	},
	{	// 9
		.name = "Kapandji 3",
		.target_position = {
			0.55,	// Thumb
			0.49,	// Index
			0.0,	// Middle
			0.0,	// Ring
			0.0,	// Pinky
			0.0,	// Thumb Abduction
		},
		.speed_factors = {
			1.0,	// Thumb
			0.6,	// Index
			0.0,	// Middle
			0.0,	// Ring
			0.0,	// Pinky
			0.0,	// Thumb Abduction
		},
		.following_grasp = NULL,
		.flags = {
			.push_after_contact = 0,
			.use_aggressive_contact_detection = 1,
		},
	},
	//########################################################
	{	// 10
		.name = "Kapandji 4 Prep",
		.target_position = {
			0.0,	// Thumb
			0.0,	// Index
			0.0,	// Middle
			0.0,	// Ring
			0.0,	// Pinky
			0.0,	// Thumb Abduction
		},
		.speed_factors = {
			1.0,	// Thumb
			1.0,	// Index
			1.0,	// Middle
			1.0,	// Ring
			1.0,	// Pinky
			1.0,	// Thumb Abduction
		},
		.following_grasp = &main_control_grasps[11],
		.flags = {
			.use_aggressive_contact_detection = 1,
		},
	},
	{	// 11
		.name = "Kapandji 4",
		.target_position = {
			0.6,	// Thumb
			0.0,	// Index
			0.52,	// Middle
			0.0,	// Ring
			0.0,	// Pinky
			0.0,	// Thumb Abduction
		},
		.speed_factors = {
			1.0,	// Thumb
			0.0,	// Index
			1.0,	// Middle
			0.0,	// Ring
			0.0,	// Pinky
			0.0,	// Thumb Abduction
		},
		.following_grasp = NULL,
		.flags = {
			.push_after_contact = 0,
			.use_aggressive_contact_detection = 1,
		},
	},
	//########################################################
	{	// 12
		.name = "Power Prep",
		.target_position = {
			0.0,	// Thumb
			0.0,	// Index
			0.0,	// Middle
			0.0,	// Ring
			0.0,	// Pinky
			0.0,	// Thumb Abduction
		},
		.speed_factors = {
			1.0,	// Thumb
			1.0,	// Index
			1.0,	// Middle
			1.0,	// Ring
			1.0,	// Pinky
			1.0,	// Thumb Abduction
		},
		.following_grasp = &main_control_grasps[13],
		.flags = {
			.use_aggressive_contact_detection = 1,
		},
	},
	{	// 13
		.name = "Power",
		.target_position = {
			1.0,	// Thumb
			1.0,	// Index
			1.0,	// Middle
			1.0,	// Ring
			1.0,	// Pinky
			0.0,	// Thumb Abduction
		},
		.speed_factors = {
			0.4,	// Thumb
			1.0,	// Index
			1.0,	// Middle
			1.0,	// Ring
			1.0,	// Pinky
			0.0,	// Thumb Abduction
		},
		.following_grasp = NULL,
		.flags = {
			.push_after_contact = 1,
			.use_aggressive_contact_detection = 0,
		},
	},
	//########################################################
	{	// 14
		.name = "Lateral Prep",
		.target_position = {
			0.0,	// Thumb
			0.8,	// Index
			0.8,	// Middle
			0.8,	// Ring
			0.8,	// Pinky
			1.0,	// Thumb Abduction
		},
		.speed_factors = {
			1.0,	// Thumb
			1.0,	// Index
			1.0,	// Middle
			1.0,	// Ring
			1.0,	// Pinky
			1.0,	// Thumb Abduction
		},
		.following_grasp = &main_control_grasps[15],
		.flags = {
			.use_aggressive_contact_detection = 1,
		},
	},
	{	// 15
		.name = "Lateral",
		.target_position = {
			0.85,	// Thumb
			0.0,	// Index
			0.0,	// Middle
			0.0,	// Ring
			0.0,	// Pinky
			0.0,	// Thumb Abduction
		},
		.speed_factors = {
			1.0,	// Thumb
			0.0,	// Index
			0.0,	// Middle
			0.0,	// Ring
			0.0,	// Pinky
			0.0,	// Thumb Abduction
		},
		.following_grasp = NULL,
		.flags = {
			.push_after_contact = 1,
			.use_aggressive_contact_detection = 0,
		},
	},
	//########################################################
	{	// 16
		.name = "Top Prep",
		.target_position = {
			0.0,	// Thumb
			0.0,	// Index
			0.0,	// Middle
			0.0,	// Ring
			0.0,	// Pinky
			0.2,	// Thumb Abduction
		},
		.speed_factors = {
			1.0,	// Thumb
			1.0,	// Index
			1.0,	// Middle
			1.0,	// Ring
			1.0,	// Pinky
			1.0,	// Thumb Abduction
		},
		.following_grasp = &main_control_grasps[17],
		.flags = {
			.use_aggressive_contact_detection = 1,
		},
	},
	{	// 17
		.name = "Top",
		.target_position = {
			1.0,	// Thumb
			1.0,	// Index
			1.0,	// Middle
			1.0,	// Ring
			1.0,	// Pinky
			0.0,	// Thumb Abduction
		},
		.speed_factors = {
			1.0,	// Thumb
			1.0,	// Index
			1.0,	// Middle
			1.0,	// Ring
			1.0,	// Pinky
			0.0,	// Thumb Abduction
		},
		.following_grasp = NULL,
		.flags = {
			.push_after_contact = 1,
			.use_aggressive_contact_detection = 0,
		},
	},
	//########################################################
	{	// 18
		.name = "Spherical Prep",
		.target_position = {
			0.0,	// Thumb
			0.0,	// Index
			0.0,	// Middle
			0.0,	// Ring
			0.0,	// Pinky
			0.0,	// Thumb Abduction
		},
		.speed_factors = {
			1.0,	// Thumb
			1.0,	// Index
			1.0,	// Middle
			1.0,	// Ring
			1.0,	// Pinky
			1.0,	// Thumb Abduction
		},
		.following_grasp = &main_control_grasps[19],
		.flags = {
			.use_aggressive_contact_detection = 1,
		},
	},
	{	// 19
		.name = "Spherical",
		.target_position = {
			1.0,	// Thumb
			1.0,	// Index
			1.0,	// Middle
			1.0,	// Ring
			1.0,	// Pinky
			0.0,	// Thumb Abduction
		},
		.speed_factors = {
			0.8,	// Thumb
			0.8,	// Index
			0.6,	// Middle
			0.8,	// Ring
			1.0,	// Pinky
			0.0,	// Thumb Abduction
		},
		.following_grasp = NULL,
		.flags = {
			.push_after_contact = 1,
			.use_aggressive_contact_detection = 0,
		},
	},
	
	//########################################################
	{	// 20
		.name = "Tripod Prep",
		.target_position = {
			0.0,	// Thumb
			0.0,	// Index
			0.0,	// Middle
			0.0,	// Ring
			0.0,	// Pinky
			0.12,	// Thumb Abduction
		},
		.speed_factors = {
			1.0,	// Thumb
			1.0,	// Index
			1.0,	// Middle
			1.0,	// Ring
			1.0,	// Pinky
			1.0,	// Thumb Abduction
		},
		.following_grasp = &main_control_grasps[21],
		.flags = {
			.use_aggressive_contact_detection = 1,
		},
	},
	{	// 21
		.name = "Tripod",
		.target_position = {
			0.52,	// Thumb
			0.48,	// Index
			0.52,	// Middle
			0.0,	// Ring
			0.0,	// Pinky
			0.0,	// Thumb Abduction
		},
		.speed_factors = {
			0.9,	// Thumb
			0.9,	// Index
			1.0,	// Middle
			0.0,	// Ring
			0.0,	// Pinky
			0.0,	// Thumb Abduction
		},
		.following_grasp = NULL,
		.flags = {
			.push_after_contact = 1,
			.use_aggressive_contact_detection = 0,
		},
	},
	//########################################################
	{	// 22
		.name = "Edge Prep",
		.target_position = {
			0.6,	// Thumb
			0.0,	// Index
			0.0,	// Middle
			0.0,	// Ring
			0.0,	// Pinky
			0.0,	// Thumb Abduction
		},
		.speed_factors = {
			1.0,	// Thumb
			1.0,	// Index
			1.0,	// Middle
			1.0,	// Ring
			1.0,	// Pinky
			1.0,	// Thumb Abduction
		},
		.following_grasp = &main_control_grasps[23],
		.flags = {
			.use_aggressive_contact_detection = 1,
		},
	},
	{	// 23
		.name = "Edge",
		.target_position = {
			0.8,	// Thumb
			0.4,	// Index
			0.4,	// Middle
			0.4,	// Ring
			0.4,	// Pinky
			0.0,	// Thumb Abduction
		},
		.speed_factors = {
			0.9,	// Thumb
			0.9,	// Index
			1.0,	// Middle
			1.0,	// Ring
			1.0,	// Pinky
			1.0,	// Thumb Abduction
		},
		.following_grasp = NULL,
		.flags = {
			.push_after_contact = 1,
			.use_aggressive_contact_detection = 0,
		},
	},
	//########################################################
	{	// 24
		.name = "Flip Prep 1",
		.target_position = {
			0.0,	// Thumb
			0.0,	// Index
			0.0,	// Middle
			0.0,	// Ring
			0.0,	// Pinky
			0.05,	// Thumb Abduction
		},
		.speed_factors = {
			1.0,	// Thumb
			1.0,	// Index
			1.0,	// Middle
			1.0,	// Ring
			1.0,	// Pinky
			1.0,	// Thumb Abduction
		},
		.following_grasp = &main_control_grasps[25],
		.flags = {
			.use_aggressive_contact_detection = 1,
		},
	},
	{	// 25
		.name = "Flip Prep 2",
		.target_position = {
			0.6,	// Thumb
			0.3,	// Index
			0.3,	// Middle
			0.3,	// Ring
			0.3,	// Pinky
			0.0,	// Thumb Abduction
		},
		.speed_factors = {
			0.9,	// Thumb
			0.9,	// Index
			1.0,	// Middle
			1.0,	// Ring
			1.0,	// Pinky
			0.0,	// Thumb Abduction
		},
		.following_grasp = &main_control_grasps[26],
		.flags = {
			.use_aggressive_contact_detection = 1,
		},
	},
	{	// 26
		.name = "Flip",
		.target_position = {
			0.60,	// Thumb
			0.55,	// Index
			0.55,	// Middle
			0.0,	// Ring
			0.0,	// Pinky
			0.0,	// Thumb Abduction
		},
		.speed_factors = {
			0.9,	// Thumb
			0.9,	// Index
			1.0,	// Middle
			0.0,	// Ring
			0.0,	// Pinky
			0.0,	// Thumb Abduction
		},
		.following_grasp = NULL,
		.flags = {
			.push_after_contact = 1,
			.use_aggressive_contact_detection = 0,
		},
	},
	{	// 27
		.name = "Nearly Open",
		.target_position = {
			0.3,	// Thumb
			0.3,	// Index
			0.3,	// Middle
			0.3,	// Ring
			0.3,	// Pinky
			0.0,	// Thumb Abduction
		},
		.speed_factors = {
			1.0,	// Thumb
			1.0,	// Index
			1.0,	// Middle
			1.0,	// Ring
			1.0,	// Pinky
			1.0,	// Thumb Abduction
		},
		.following_grasp = NULL,
		.flags = {
			.push_after_contact = 0,
			.use_aggressive_contact_detection = 1,
		},
	},
};
const uint32_t main_control_num_grasps = sizeof(main_control_grasps) / sizeof(main_control_grasp_t);

uint8_t check_all_grasps(){
	for(uint8_t i = 0; i < main_control_num_grasps; ++i){
		for(uint8_t j = 0; j < MAIN_CONTROL_NUM_MOTORS; ++j){
			if(main_control_grasps[i].speed_factors[j] < 0 || main_control_grasps[i].speed_factors[j] > 1){
				printf("Speed Factor of grasp %u not in between [0, 1]\r\n", i);
				return 1;
			}
			if(main_control_grasps[i].target_position[j] < 0 || main_control_grasps[i].target_position[j] > 1){
				printf("Target Position of grasp %u not in between [0, 1]\r\n", i);
				return 2;
			}
		}
	}
	return 0;
}

const main_control_grasp_t* main_control_get_grasp(uint8_t grasp_index){
	if(grasp_index >= main_control_num_grasps){
		return NULL;
	}
	return &main_control_grasps[grasp_index];
}
