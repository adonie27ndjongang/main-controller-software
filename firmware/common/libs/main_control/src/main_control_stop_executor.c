#include "main_control_impl.h"


static void start(main_control_t* control);

main_control_executor_t stop_executor = {
	.start = start,
	.execute = NULL,	
	.stop = NULL,
	};


static void start(main_control_t* control){
	for(uint8_t i = 0; i < MAIN_CONTROL_NUM_MOTORS; ++i){
		motor_i2c_send_control_mode_stop(&control->motors[i]);
	}	
}

