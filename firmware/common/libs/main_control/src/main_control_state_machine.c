#include "main_control_impl.h"

#include <stdio.h>
#include <string.h>
#include <math.h>


static void send_control_mode(main_control_motor_t* motor, uint8_t control_mode, uint8_t stop_at_contact, uint8_t use_aggressive_contact_detection);
static void send_control_mode_stop(main_control_motor_t* motor) { send_control_mode(motor, MOTOR_CM_STOPPED, 0, 0); }
static void send_setpoint(const main_control_motor_t* motor, uint8_t command, float32_t setpoint);
static void send_closing_current_setpoint(const main_control_motor_t* motor, float32_t setpoint);
static void send_complete_input(const main_control_motor_t* motor, dc_motor_controller_inputs_t* input);

static float32_t get_velocity_limit(const main_control_t* control, uint8_t motor_index);
static float32_t get_adapted_current_limit(const main_control_t* control, uint8_t motor_index, float32_t current_limit);
static float32_t get_prepare_target_angle(const main_control_t* control, uint8_t motor_index);
static float32_t get_final_target_angle(const main_control_t* control, uint8_t motor_index);

void main_control_loop(){
	simple_state_machine_event_t event = {.signal_ = MAIN_CONTROL_EVT_SIG_LOOP};
	simpleStateMachineDispatch_(&g_main_control.state_machine, &event, &g_main_control);
}

void main_control_state_init(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void * user_pointer){
}

void main_control_state_calibrate(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void * user_pointer)
{
	main_control_t* const control = (main_control_t*) user_pointer;
	switch(event->signal_)
	{
		case ENTRY_SIG:
		{
			//while(1);
			control->current_state = MAIN_FSM_CALIBRATE;
			control->flags.doing_calibration = 1;
			control->flags.doing_calibration_groups = 0;
			for(uint8_t i = 0; i < MAIN_CONTROL_NUM_MOTORS; ++i){
				if(control->motors[i].open_at_positive == 1){
					send_control_mode(&control->motors[i], MOTOR_CM_CALIB_PREP_POS, 0, 0);
				}
				else{
					send_control_mode(&control->motors[i], MOTOR_CM_CALIB_PREP_NEG, 0, 0);
				}
			}
			break;
		}
		case EXIT_SIG:
		{
			control->flags.doing_calibration = 0;
			break;
		}
		case MAIN_CONTROL_EVT_SIG_LOOP:{
			if(!control->flags.doing_calibration_groups){
				bool all_prepared = 1;
				for(uint8_t i = 0; i < MAIN_CONTROL_NUM_MOTORS; ++i){
					if(control->motors[i].flags_b.calib_stage != CALIB_STAGE_WAITING2){
						all_prepared = 0;
						break;
					}
				}
				if(all_prepared){
					control->flags.doing_calibration_groups = 1;
					control->current_calib_group = 0;
					for(uint8_t i = 0; i < MAIN_CONTROL_NUM_MOTORS; ++i){
						if(control->motors[i].calibration_group != control->current_calib_group){
							continue;
						}
						send_control_mode(&control->motors[i], MOTOR_CM_CALIBRATION, 0, 0);
					}
				}
			}
			else{
				bool all_finished = 1;
				bool group_finished = 1;
				for(uint8_t i = 0; i < MAIN_CONTROL_NUM_MOTORS; ++i){
					if(control->motors[i].flags_b.calib_stage != CALIB_STAGE_FINISHED){
						all_finished = false;
						if(control->motors[i].calibration_group == control->current_calib_group){
							group_finished = false;
							break;
						}
					}
				}
				if(all_finished){
					for(uint8_t i = 0; i < MAIN_CONTROL_NUM_MOTORS; ++i){
						const sam5_i2c_master_com_job_t job = {
							.command = MOTOR_PROTOCOL_BUILD_COMMAND(control->motors[i].motor_index, MOTOR_PROTOCOL_READ_LIMITS),
							.slave_address = control->motors[i].i2c_address,
							.direction = I2C_MASTER_DIR_SLAVE_TO_MASTER,
							.num_bytes_to_transfer = sizeof(dc_motor_controller_limits_t),
							.callback = &main_control_motor_i2c_callback_read_limits,
							.period_ms = 0
						};
						sam5_i2c_master_register_com_job(&job);
					}
					simpleStateMachineTran_(fsm, main_control_state_stop, user_pointer);
				}
				else if(group_finished){
					++control->current_calib_group;
					for(uint8_t i = 0; i < MAIN_CONTROL_NUM_MOTORS; ++i){
						if(control->motors[i].calibration_group != control->current_calib_group){
							continue;
						}
						send_control_mode(&control->motors[i], MOTOR_CM_CALIBRATION, 0, 0);
					}
				}
			}
		}
		case MAIN_CONTROL_EVT_SIG_GRASP_UPDATE:
		case MAIN_CONTROL_EVT_SIG_SPEED_UPDATE:
		case MAIN_CONTROL_EVT_SIG_ALL_STOPPED_OR_REACHED:
		case MAIN_CONTROL_EVT_SIG_I2C_ERROR:
		case MAIN_CONTROL_EVT_SIG_ERROR:
		default:
		break;
	}
}

void main_control_state_stop(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void * user_pointer)
{
	main_control_t* const control = (main_control_t*) user_pointer;
	switch(event->signal_)
	{
		case ENTRY_SIG:
		{
			control->current_state = MAIN_FSM_STOP;
			for(uint8_t i = 0; i < MAIN_CONTROL_NUM_MOTORS; ++i){
				send_control_mode_stop(&control->motors[i]);
			}
			control->active_grasp = NULL;
			break;
		}
		case EXIT_SIG:
		{
			break;
		}
		case MAIN_CONTROL_EVT_SIG_GRASP_UPDATE:
		{
			const main_control_grasp_t* const grasp = main_control_get_grasp();
			if(grasp == NULL){
				// EMG state STOP -> Do nothing
				break;
			}
			control->active_grasp = grasp;
			if(main_control_is_grasp_closing()){
				simpleStateMachineTran_(fsm, main_control_state_closing_prepare, user_pointer);
			}
			else{
				simpleStateMachineTran_(fsm, main_control_state_opening, user_pointer);
			}
			break;
		}
		
		case MAIN_CONTROL_EVT_SIG_LOOP:
		case MAIN_CONTROL_EVT_SIG_SPEED_UPDATE:
		case MAIN_CONTROL_EVT_SIG_ALL_STOPPED_OR_REACHED:
		case MAIN_CONTROL_EVT_SIG_I2C_ERROR:
		case MAIN_CONTROL_EVT_SIG_ERROR:
		default:
		break;
	}
}

void main_control_state_closing_prepare(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void * user_pointer)
{
	main_control_t* const control = (main_control_t*) user_pointer;
	ASSERT(control->active_grasp != NULL);
	switch(event->signal_){
		case ENTRY_SIG:
		{
			control->current_state = MAIN_FSM_CLOSE_PREPARE;
			uint8_t moving_finger_count = 0;
			for(uint8_t i = 0; i < MAIN_CONTROL_NUM_MOTORS; ++i){
				const main_control_motor_t* const motor = &control->motors[i];
				if((control->active_grasp->move_type[i] & MAIN_GRASP_MOVE_ONLY_PREP) == 0){
					send_control_mode_stop(&control->motors[i]);
					continue;
				}
				++moving_finger_count;
				dc_motor_controller_inputs_t input = {
					.flags = {
						.control_mode = MOTOR_CM_POSITION,
						.continue_at_end = 0,
						.stop_at_contact = 0,
						.use_endstop_for_end = MAIN_CONTROL_USE_ENDSTOP_FOR_END,
						.use_endstop_for_refine = MAIN_CONTROL_USE_ENDSTOP_FOR_REFINE,
						.use_aggressive_contact_detection = 1,
					},
					.angle = get_prepare_target_angle(control, i),
					.velocity = get_velocity_limit(control, i),
					.current = get_adapted_current_limit(control, i, MAIN_CONTROL_CURRENT_LIMIT_POSITION_CONTROL),
				};
				send_complete_input(motor, &input);
			}
			if(moving_finger_count == 0){
				simpleStateMachineTran_(fsm, main_control_state_closing_pre_contact, user_pointer);
			}
			break;
		}
		case MAIN_CONTROL_EVT_SIG_GRASP_UPDATE:
		{
			simpleStateMachineTran_(fsm, main_control_state_stop, user_pointer);
			simpleStateMachineDispatch_(fsm, event, user_pointer);
			break;
		}
		case MAIN_CONTROL_EVT_SIG_SPEED_UPDATE:
		{
			for(uint8_t i = 0; i < MAIN_CONTROL_NUM_MOTORS; ++i){
				if((control->active_grasp->move_type[i] & MAIN_GRASP_MOVE_ONLY_PREP) == 0){
					continue;
				}
				const main_control_motor_t* const motor = &control->motors[i];
				send_setpoint(motor, MOTOR_PROTOCOL_WRITE_INPUT_VELOCITY, get_velocity_limit(control, i));
			}
			break;
		}
		case MAIN_CONTROL_EVT_SIG_ALL_STOPPED_OR_REACHED:
		{
			simpleStateMachineTran_(fsm, main_control_state_closing_pre_contact, user_pointer);
			break;
		}
		case MAIN_CONTROL_EVT_SIG_ERROR:
		case MAIN_CONTROL_EVT_SIG_I2C_ERROR:
		{
			simpleStateMachineTran_(fsm, main_control_state_stop, user_pointer);
			break;
		}
		case MAIN_CONTROL_EVT_SIG_LOOP:
		case EXIT_SIG:
		default:
		break;
	}
}

void main_control_state_closing_pre_contact(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void * user_pointer)
{
	main_control_t* const control = (main_control_t*) user_pointer;
	ASSERT(control->active_grasp != NULL);
	switch(event->signal_)
	{
		case ENTRY_SIG:
		{
			control->current_state = MAIN_FSM_CLOSE_PRE_CONTACT;
			uint8_t moving_finger_count = 0;
			for(uint8_t i = 0; i < MAIN_CONTROL_NUM_MOTORS; ++i){
				if((control->active_grasp->move_type[i] & MAIN_GRASP_MOVE_ONLY_GRASP) == 0){
					send_control_mode_stop(&control->motors[i]);
					continue;
				}
				++moving_finger_count;
				const main_control_motor_t* const motor = &control->motors[i];
				dc_motor_controller_inputs_t input = {
					.flags = {
						.control_mode = MOTOR_CM_POSITION,
						.continue_at_end = 0,
						.stop_at_contact = 0,
						.use_endstop_for_end = MAIN_CONTROL_USE_ENDSTOP_FOR_END,
						.use_endstop_for_refine = MAIN_CONTROL_USE_ENDSTOP_FOR_REFINE,
						.use_aggressive_contact_detection = 0,
					},
					.angle = get_final_target_angle(control, i),
					.velocity = get_velocity_limit(control, i),
					.current = get_adapted_current_limit(control, i, MAIN_CONTROL_CURRENT_LIMIT_POSITION_CONTROL),
				};
				send_complete_input(motor, &input);
			}
			if(moving_finger_count == 0){
				simpleStateMachineTran_(fsm, main_control_state_stop, user_pointer);
			}
			break;
		}
		case MAIN_CONTROL_EVT_SIG_ALL_STOPPED_OR_REACHED:
		{
			//simpleStateMachineTran_(fsm, main_control_state_closing_post_contact, user_pointer);
			simpleStateMachineTran_(fsm, main_control_state_stop, user_pointer);
			break;
		}
		case MAIN_CONTROL_EVT_SIG_GRASP_UPDATE:
		{
			simpleStateMachineTran_(fsm, main_control_state_stop, user_pointer);
			simpleStateMachineDispatch_(fsm, event, user_pointer);
			break;
		}
		case MAIN_CONTROL_EVT_SIG_SPEED_UPDATE:
		{
			for(uint8_t i = 0; i < MAIN_CONTROL_NUM_MOTORS; ++i){
				if((control->active_grasp->move_type[i] & MAIN_GRASP_MOVE_ONLY_GRASP) == 0){
					continue;
				}
				const main_control_motor_t* const motor = &control->motors[i];
				send_setpoint(motor, MOTOR_PROTOCOL_WRITE_INPUT_VELOCITY, get_velocity_limit(control, i));
			}
			break;
		}
		case MAIN_CONTROL_EVT_SIG_ERROR:
		case MAIN_CONTROL_EVT_SIG_I2C_ERROR:
		{
			simpleStateMachineTran_(fsm, main_control_state_stop, user_pointer);
			break;
		}
		case MAIN_CONTROL_EVT_SIG_LOOP:
		case EXIT_SIG:
		default:
		break;
	}
}

void main_control_state_closing_post_contact(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void * user_pointer)
{
	main_control_t* const control = (main_control_t*) user_pointer;
	ASSERT(control->active_grasp != NULL);
	switch(event->signal_)
	{
		case ENTRY_SIG:
		{
			control->current_state = MAIN_FSM_CLOSE_POST_CONTACT;
			control->current_current = 0.001;
			for(uint8_t i = 0; i < MAIN_CONTROL_NUM_MOTORS; ++i){
				if((control->active_grasp->move_type[i] & MAIN_GRASP_MOVE_ONLY_GRASP) == 0){
					send_control_mode_stop(&control->motors[i]);
					continue;
				}
				const main_control_motor_t* motor = &control->motors[i];
				if(motor->flags_a.contact_detected){
					send_closing_current_setpoint(motor, 0);
					send_control_mode(&control->motors[i], MOTOR_CM_CURRENT, 0, 1);
				}
				else{
					send_control_mode_stop(&control->motors[i]);
				}
			}
			break;
		}
		case MAIN_CONTROL_EVT_SIG_GRASP_UPDATE:
		{
			simpleStateMachineTran_(fsm, main_control_state_stop, user_pointer);
			simpleStateMachineDispatch_(fsm, event, user_pointer);
			break;
		}
		case MAIN_CONTROL_EVT_SIG_LOOP:{
			// Update rate of 100 Hz -> 0.1 A/s increase at full contraception
			control->current_current += main_control_get_speed_factor() / 1000;
			if(control->current_current > (float32_t) MAIN_CONTROL_CURRENT_LIMIT_CURRENT_CONTROL){
				control->current_current = MAIN_CONTROL_CURRENT_LIMIT_CURRENT_CONTROL;
			}
			for(uint8_t i = 0; i < MAIN_CONTROL_NUM_MOTORS; ++i){
				const main_control_motor_t* motor = &control->motors[i];
				if((control->active_grasp->move_type[i] & MAIN_GRASP_MOVE_ONLY_GRASP) == 0){
					continue;
				}
				send_closing_current_setpoint(motor, get_adapted_current_limit(control, i, control->current_current));
			}
			break;
		}
		case MAIN_CONTROL_EVT_SIG_ERROR:
		case MAIN_CONTROL_EVT_SIG_I2C_ERROR:
		{
			simpleStateMachineTran_(fsm, main_control_state_stop, user_pointer);
			break;
		}
		case MAIN_CONTROL_EVT_SIG_ALL_STOPPED_OR_REACHED:{
			// All fingers moved to the end of the movement range
			simpleStateMachineTran_(fsm, main_control_state_stop, user_pointer);
			break;
		}
		case EXIT_SIG:
		default:
		break;
	}
}

void main_control_state_opening(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void * user_pointer)
{
	main_control_t* const control = (main_control_t*) user_pointer;
	ASSERT(control->active_grasp != NULL);
	switch(event->signal_)
	{
		case ENTRY_SIG:
		{
			control->current_state = MAIN_FSM_OPEN;
			
			for(uint8_t i = 0; i < MAIN_CONTROL_NUM_MOTORS; ++i){
				const main_control_motor_t* motor = &control->motors[i];
				// Open always every finger
				//if(main_control_grasps[main_control_get_grasp_index()][i] == 0.0){
				//	continue;
				//}
				dc_motor_controller_inputs_t input = {
					.flags = {
						.continue_at_end = 0,
						.stop_at_contact = 0,
						.control_mode = MOTOR_CM_POSITION,
						.use_endstop_for_end = MAIN_CONTROL_USE_ENDSTOP_FOR_END,
						.use_endstop_for_refine = MAIN_CONTROL_USE_ENDSTOP_FOR_REFINE,
						.use_aggressive_contact_detection = 1,
					},
					.angle = motor->open_at_positive ? motor->limits.angle_max : motor->limits.angle_min,
					.velocity = get_velocity_limit(control, i),
					.current = get_adapted_current_limit(control, i, MAIN_CONTROL_CURRENT_LIMIT_POSITION_CONTROL),
				};
				send_complete_input(motor, &input);
			}
			break;
		}
		case MAIN_CONTROL_EVT_SIG_GRASP_UPDATE:
		{
			simpleStateMachineTran_(fsm, main_control_state_stop, user_pointer);
			simple_state_machine_event_t event = {.signal_ = MAIN_CONTROL_EVT_SIG_GRASP_UPDATE};
			simpleStateMachineDispatch_(fsm, &event, user_pointer);
			break;
		}
		case MAIN_CONTROL_EVT_SIG_SPEED_UPDATE:
		{
			for(uint8_t i = 0; i < MAIN_CONTROL_NUM_MOTORS; ++i){
				// Open always every finger
				//if(main_control_grasps[main_control_get_grasp_index()][i] == 0.0){
				//	continue;
				//}
				send_setpoint(&control->motors[i], MOTOR_PROTOCOL_WRITE_INPUT_VELOCITY, get_velocity_limit(control, i));
			}
			break;
		}
		case MAIN_CONTROL_EVT_SIG_ERROR:
		case MAIN_CONTROL_EVT_SIG_I2C_ERROR:
		{
			simpleStateMachineTran_(fsm, main_control_state_stop, user_pointer);
			break;
		}
		case MAIN_CONTROL_EVT_SIG_ALL_STOPPED_OR_REACHED:
		{
			simpleStateMachineTran_(fsm, main_control_state_stop, user_pointer);
			break;
		}
		case EXIT_SIG:
		default:
		break;
	}
}


static void send_control_mode(main_control_motor_t* motor, uint8_t control_mode, uint8_t stop_at_contact, uint8_t use_aggressive_contact_detection){
	dc_motor_controller_input_flags_t flags = {
		.control_mode = control_mode,
		.continue_at_end = 0,
		.stop_at_contact = stop_at_contact,
		.use_endstop_for_end = MAIN_CONTROL_USE_ENDSTOP_FOR_END,
		.use_endstop_for_refine = MAIN_CONTROL_USE_ENDSTOP_FOR_REFINE,
		.use_aggressive_contact_detection = use_aggressive_contact_detection,
	};
	sam5_i2c_master_com_job_t job = {
		.command = MOTOR_PROTOCOL_BUILD_COMMAND(motor->motor_index, MOTOR_PROTOCOL_WRITE_INPUT_FLAGS),
		.slave_address = motor->i2c_address,
		.direction = I2C_MASTER_DIR_MASTER_TO_SLAVE,
		.num_bytes_to_transfer = 1,
		.callback = control_mode == MOTOR_CM_STOPPED ?  main_control_motor_i2c_callback_simple_success : main_control_motor_i2c_callback_activate_motor,
		.period_ms = 0,
	};
	job.buffer[0] = *((uint8_t*) &flags);
	sam5_i2c_master_register_com_job(&job);
}

static void send_closing_current_setpoint(const main_control_motor_t* motor, float32_t setpoint){
	float32_t buf = motor->open_at_positive == 1 ? -setpoint : setpoint;
	sam5_i2c_master_com_job_t job = {
		.command = MOTOR_PROTOCOL_BUILD_COMMAND(motor->motor_index, MOTOR_PROTOCOL_WRITE_INPUT_CURRENT),
		.slave_address = motor->i2c_address,
		.direction = I2C_MASTER_DIR_MASTER_TO_SLAVE,
		.num_bytes_to_transfer = sizeof(float32_t),
		.callback = main_control_motor_i2c_callback_simple_success,
		.period_ms = 0
	};
	memcpy((uint8_t*) job.buffer, &buf, sizeof(buf));
	sam5_i2c_master_register_com_job(&job);
}

static void send_setpoint(const main_control_motor_t* motor, uint8_t command, float32_t setpoint){
	sam5_i2c_master_com_job_t job = {
		.command = MOTOR_PROTOCOL_BUILD_COMMAND(motor->motor_index, command),
		.slave_address = motor->i2c_address,
		.direction = I2C_MASTER_DIR_MASTER_TO_SLAVE,
		.num_bytes_to_transfer = sizeof(float32_t),
		.callback = main_control_motor_i2c_callback_simple_success,
		.period_ms = 0
	};
	memcpy((uint8_t*) job.buffer, &setpoint, sizeof(setpoint));
	sam5_i2c_master_register_com_job(&job);
}

static void send_complete_input(const main_control_motor_t* motor, dc_motor_controller_inputs_t* input){
	sam5_i2c_master_com_job_t job = {
		.command = MOTOR_PROTOCOL_BUILD_COMMAND(motor->motor_index, MOTOR_PROTOCOL_WRITE_INPUT),
		.slave_address = motor->i2c_address,
		.direction = I2C_MASTER_DIR_MASTER_TO_SLAVE,
		.num_bytes_to_transfer = sizeof(dc_motor_controller_inputs_t),
		.callback = input->flags.control_mode == MOTOR_CM_STOPPED ?  main_control_motor_i2c_callback_simple_success : main_control_motor_i2c_callback_activate_motor,
		.period_ms = 0
	};
	memcpy((uint8_t*) job.buffer, input, sizeof(dc_motor_controller_inputs_t));
	sam5_i2c_master_register_com_job(&job);
}

static float32_t get_velocity_limit(const main_control_t* control, uint8_t motor_index){
	#if MAIN_CONTROL_ADAPT_VELOCITY_BASED_ON_GEAR
	const float32_t gear_factor = control->motors[motor_index].gear_ratio / control->highest_gear_ratio;
	return gear_factor *  control->active_grasp->speed_factors[motor_index] * main_control_get_speed_factor(&control->motors[motor_index]) * MAIN_CONTROL_VELOCITY_LIMIT;
	#else
	return control->active_grasp->speed_factors[motor_index] * main_control_get_speed_factor(&control->motors[motor_index]) * MAIN_CONTROL_VELOCITY_LIMIT;
	#endif
}

static float32_t get_adapted_current_limit(const main_control_t* control, uint8_t motor_index, float32_t current_limit){
	#if MAIN_CONTROL_ADAPT_CURRENT_BASED_ON_GEAR
	return current_limit * (control->highest_gear_ratio / control->motors[motor_index].gear_ratio);
	#else
	return current_limit;
	#endif
}

static float32_t get_prepare_target_angle(const main_control_t* control, uint8_t motor_index){
	const main_control_motor_t* motor = &control->motors[motor_index];
	if(motor->open_at_positive){
		return motor->limits.angle_max + control->active_grasp->prep_position[motor_index] * (motor->limits.angle_min - motor->limits.angle_max);
	}
	else{
		return motor->limits.angle_min + control->active_grasp->prep_position[motor_index] * (motor->limits.angle_max - motor->limits.angle_min);
	}
}

static float32_t get_final_target_angle(const main_control_t* control, uint8_t motor_index){
	const main_control_motor_t* motor = &control->motors[motor_index];
	if(motor->open_at_positive){
		return motor->limits.angle_max + control->active_grasp->target_position[motor_index] * (motor->limits.angle_min - motor->limits.angle_max);
	}
	else{
		return motor->limits.angle_min + control->active_grasp->target_position[motor_index] * (motor->limits.angle_max - motor->limits.angle_min);
	}
}