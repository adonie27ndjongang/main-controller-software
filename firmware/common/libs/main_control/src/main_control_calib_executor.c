#include "main_control_impl.h"
#include "sam5_adc_batvlt.h"


static void start(main_control_t* control);
static void execute(simple_state_machine_event_t const *event, main_control_t* control);
static void stop(main_control_t* control);

static void state_wait_batvlt(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void * user_pointer);
static void state_prepare(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void * user_pointer);
static void state_calibration_groups(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void * user_pointer);
static void state_finished(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void * user_pointer);


main_control_executor_t calib_executor = {
	.start = &start,
	.execute = &execute,
	.stop = &stop,
};

/** 
	@brief 
	@param[in] None
	@param[out] None
*/
static void start(main_control_t* control){
	simpleStateMachineInit_(&control->calib_executor.state_machine, state_wait_batvlt, control);
}

/** 
	@brief 
	@param[in] None
	@param[out] None
*/
static void execute(simple_state_machine_event_t const *event, main_control_t* control){
	simpleStateMachineDispatch_(&control->calib_executor.state_machine, event, control);
}

/** 
	@brief 
	@param[in] None
	@param[out] None
*/
static void stop(main_control_t* control){
	control->flags.calib_executor_running = 0;
	control->calib_executor.state = CONTROL_CALIB_EXEC_STATE_INACTIVE;
}

/** 
	@brief 
	@param[in] None
	@param[out] None
*/
static void state_wait_batvlt(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void * user_pointer)
{
	main_control_t* const control = (main_control_t*) user_pointer;
	switch(event->signal_)
	{
		case ENTRY_SIG:
		{
			control->calib_executor.state = CONTROL_CALIB_EXEC_STATE_WAIT_BATVLT;
			control->flags.calib_executor_running = 1;
			control->flags.calibration_finished = 0;
			for(uint8_t i = 0; i < MAIN_CONTROL_NUM_MOTORS; ++i){
				motor_i2c_send_control_mode_stop(&control->motors[i]);
			}
			break;
		}
		case EXIT_SIG:
		{
			break;
		}
		case MAIN_CONTROL_EVT_SIG_LOOP:{
			if(sam5_bat_vltg_is_measurement_valid()){
				simpleStateMachineTran_(fsm, state_prepare, user_pointer);
			}
			break;
		}
		case MAIN_CONTROL_EVT_SIG_ALL_STOPPED_OR_REACHED:
		case MAIN_CONTROL_EVT_SIG_NEW_INPUT:
		case MAIN_CONTROL_EVT_SIG_ERROR:
		default:
		break;
	}
}

/** 
	@brief 
	@param[in] None
	@param[out] None
*/
static void state_prepare(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void * user_pointer)
{
	main_control_t* const control = (main_control_t*) user_pointer;
	switch(event->signal_)
	{
		case ENTRY_SIG:
		{
			control->calib_executor.state = CONTROL_CALIB_EXEC_STATE_PREPARE;
			control->flags.calibration_finished = 0;
			for(uint8_t i = 0; i < MAIN_CONTROL_NUM_MOTORS; ++i){
				if(control->motors[i].open_at_positive == 1){
					motor_i2c_send_control_mode(&control->motors[i], MOTOR_CM_CALIB_PREP_POS, 0, 0);
				}
				else{
					motor_i2c_send_control_mode(&control->motors[i], MOTOR_CM_CALIB_PREP_NEG, 0, 0);
				}
			}
			break;
		}
		case EXIT_SIG:
		{
			break;
		}
		case MAIN_CONTROL_EVT_SIG_LOOP:{
			bool all_prepared = 1;
			for(uint8_t i = 0; i < MAIN_CONTROL_NUM_MOTORS; ++i){
				if(control->motors[i].state.flags_b.calib_stage != CALIB_STAGE_WAITING2){
					all_prepared = 0;
					break;
				}
			}
			if(all_prepared){
				simpleStateMachineTran_(fsm, state_calibration_groups, user_pointer);
				simpleStateMachineDispatch_(fsm, event, user_pointer);
			}
			break;
		}
		case MAIN_CONTROL_EVT_SIG_ALL_STOPPED_OR_REACHED:
		case MAIN_CONTROL_EVT_SIG_NEW_INPUT:
		case MAIN_CONTROL_EVT_SIG_ERROR:
		default:
		break;
	}
}


/** 
	@brief 
	@param[in] None
	@param[out] None
*/
static void state_calibration_groups(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void * user_pointer)
{
	main_control_t* const control = (main_control_t*) user_pointer;
	switch(event->signal_)
	{
		case ENTRY_SIG:
		{
			control->calib_executor.state = CONTROL_CALIB_EXEC_STATE_GROUPS;
			control->calib_executor.current_calib_group = 0;
			break;
		}
		case EXIT_SIG:
		{
			break;
		}
		case MAIN_CONTROL_EVT_SIG_LOOP:{
			bool all_finished = 1;
			bool group_finished = 1;
			for(uint8_t i = 0; i < MAIN_CONTROL_NUM_MOTORS; ++i){
				if(control->motors[i].state.flags_b.calib_stage != CALIB_STAGE_FINISHED){
					all_finished = false;
					if(control->motors[i].calibration_group == control->calib_executor.current_calib_group){
						group_finished = false;
						break;
					}
				}
			}
			if(all_finished){
				for(uint8_t i = 0; i < MAIN_CONTROL_NUM_MOTORS; ++i){
					const sam5_i2c_master_com_job_t job = {
						.command = MOTOR_PROTOCOL_BUILD_COMMAND(control->motors[i].motor_index, MOTOR_PROTOCOL_READ_LIMITS),
						.slave_address = control->motors[i].i2c_address,
						.direction = I2C_MASTER_DIR_SLAVE_TO_MASTER,
						.num_bytes_to_transfer = sizeof(dc_motor_controller_limits_t),
						.callback = &main_control_motor_i2c_callback_read_limits,
						.period_ms = 0
					};
					sam5_i2c_master_register_com_job(&job);
				}
				simpleStateMachineTran_(fsm, state_finished, user_pointer);
				break;
			}
			if(group_finished){
				++control->calib_executor.current_calib_group;
			}
			for(uint8_t i = 0; i < MAIN_CONTROL_NUM_MOTORS; ++i){
				if(control->motors[i].calibration_group != control->calib_executor.current_calib_group){
					continue;
				}
				motor_i2c_send_control_mode(&control->motors[i], MOTOR_CM_CALIBRATION, 0, 0);
			}
			break;
		}
		case MAIN_CONTROL_EVT_SIG_ALL_STOPPED_OR_REACHED:
		case MAIN_CONTROL_EVT_SIG_NEW_INPUT:
		case MAIN_CONTROL_EVT_SIG_ERROR:
		default:
		break;
	}
}

/** 
	@brief 
	@param[in] None
	@param[out] None
*/
static void state_finished(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void * user_pointer){

	main_control_t* const control = (main_control_t*) user_pointer;
	switch(event->signal_)
	{
		case ENTRY_SIG:
		{
			control->calib_executor.state = CONTROL_CALIB_EXEC_STATE_FINISHED;
			control->flags.calibration_finished = 1;
			control->flags.calib_executor_running = 0;
			break;
		}
		case EXIT_SIG:
		case MAIN_CONTROL_EVT_SIG_ALL_STOPPED_OR_REACHED:
		case MAIN_CONTROL_EVT_SIG_LOOP:
		case MAIN_CONTROL_EVT_SIG_NEW_INPUT:
		case MAIN_CONTROL_EVT_SIG_ERROR:
		default:
		break;
	}
}
