#include "main_control_impl.h"


static void start(main_control_t* control);
static void execute(simple_state_machine_event_t const *event, main_control_t* control);
static void stop(main_control_t* control);

static void state_stop(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void * user_pointer);
static void state_pre_contact(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void * user_pointer);
static void state_post_contact(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void * user_pointer);

static float32_t get_overall_speed_factor(const main_control_t* control);
static float32_t get_motor_velocity_limit(const main_control_t* control, uint8_t motor_index);
static float32_t get_overall_current_limit(const main_control_t* control);
static float32_t get_motor_current_limit(const main_control_t* control, uint8_t motor_index, float32_t current_limit);
static float32_t get_target_angle(const main_control_t* control, uint8_t motor_index);

static void activate_following_grasp(volatile simple_state_machine_t *fsm, main_control_t* control);

main_control_executor_t grasp_executor = {
	.start = &start,
	.execute = &execute,
	.stop = &stop,
};

//Erwan: Main control struct contains all possible inputs and outputs on the control loop mechanism

/** 
	@brief 
	@param[in] None
	@param[out] None
*/
static void start(main_control_t* control){
	control->grasp_executor.initial_grasp_index = 0xFF;
	simpleStateMachineInit_(&control->grasp_executor.state_machine, state_stop, control);
	simple_state_machine_event_t event = { .signal_ = MAIN_CONTROL_EVT_SIG_NEW_INPUT };
	simpleStateMachineDispatch_(&control->grasp_executor.state_machine, &event, control);
}

/** 
	@brief 
	@param[in] None
	@param[out] None
*/
static void execute(simple_state_machine_event_t const *event, main_control_t* control){
	simpleStateMachineDispatch_(&control->grasp_executor.state_machine, event, control);
}

/** 
	@brief 
	@param[in] None
	@param[out] None
*/
static void stop(main_control_t* control){
	control->grasp_executor.state = CONTROL_GRASP_EXEC_STATE_INACTIVE;
	control->flags.grasp_executor_running = 0;
}


//Erwan: This state machine handler is used to manage the start of the grasping task, since the actual state of grasping execution must be first checked.
//Two grasping can't be executed simultaneously
/** 
	@brief 
	@param[in] None
	@param[out] None
*/
static void state_stop(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void * user_pointer)
{
	main_control_t* const control = (main_control_t*) user_pointer;
	switch(event->signal_)
	{
		case ENTRY_SIG:
		{
			control->grasp_executor.state = CONTROL_GRASP_EXEC_STATE_STOP;
			//Erwan: Stop all the motor voltage to 0(stop them all)
			for(uint8_t i = 0; i < MAIN_CONTROL_NUM_MOTORS; ++i){
				motor_i2c_send_control_mode_stop(&control->motors[i]);
				control->grasp_executor.contact_detected[i] = 0;
			}
			control->grasp_executor.active_grasp = NULL;
			control->flags.grasp_executor_running = 0;
			break;
		}
		case EXIT_SIG:
		{
			control->flags.grasp_executor_running = 1;
			break;
		}
		case MAIN_CONTROL_EVT_SIG_NEW_INPUT:
		{
			if(control->grasp_executor.initial_grasp_index != control->active_input->grasp_index){
				control->grasp_executor.initial_grasp_index = control->active_input->grasp_index;
				const main_control_grasp_t* const grasp = main_control_get_grasp(control->active_input->grasp_index);
				if(grasp == NULL){
					// EMG state STOP -> Do nothing
					break;
				}
				control->grasp_executor.active_grasp = grasp;
				simpleStateMachineTran_(fsm, state_pre_contact, user_pointer);
			}
			break;
		}
		
		case MAIN_CONTROL_EVT_SIG_LOOP:
		case MAIN_CONTROL_EVT_SIG_ALL_STOPPED_OR_REACHED:
		case MAIN_CONTROL_EVT_SIG_ERROR:
		default:
		break;
	}
}

//Erwan: This handling function of the state machine, the hand is here in motor control mode
/** 
	@brief 
	@param[in] None
	@param[out] None
*/
static void state_pre_contact(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void * user_pointer)
{
	main_control_t* const control = (main_control_t*) user_pointer;
	ASSERT(control->grasp_executor.active_grasp != NULL);
	switch(event->signal_)
	{
		case ENTRY_SIG:
		{
			
			control->grasp_executor.state = CONTROL_GRASP_EXEC_STATE_PRE;
			uint8_t moving_finger_count = 0;
			for(uint8_t i = 0; i < MAIN_CONTROL_NUM_MOTORS; ++i){
				//Erwan: stop the motor if contact detected or the velocity is zero
				if(control->grasp_executor.active_grasp->speed_factors[i] == 0 || control->grasp_executor.contact_detected[i] == 1){
					motor_i2c_send_control_mode_stop(&control->motors[i]);
					continue;
				}
				++moving_finger_count;
				const main_control_motor_t* const motor = &control->motors[i];
				
				//Erwan: prepare the signal to send to the motor board
				dc_motor_controller_inputs_t input = {
					.flags = {
						.control_mode = MOTOR_CM_POSITION,
						.continue_at_end = 0,
						.stop_at_contact = 1,
						.use_endstop_for_end = MAIN_CONTROL_USE_ENDSTOP_FOR_END,
						.use_endstop_for_refine = MAIN_CONTROL_USE_ENDSTOP_FOR_REFINE,
						.use_aggressive_contact_detection = control->grasp_executor.active_grasp->flags.use_aggressive_contact_detection,
					},
					.angle = get_target_angle(control, i),
					.velocity = get_motor_velocity_limit(control, i),
					.current = get_motor_current_limit(control, i, MAIN_CONTROL_CURRENT_LIMIT_POSITION_CONTROL),
				};
				motor_i2c_send_complete_input(motor, &input);
			}
			//If no finger moves anymore start the next grasp
			if(moving_finger_count == 0){
				activate_following_grasp(fsm, control);
			}
			break;
		}
		case MAIN_CONTROL_EVT_SIG_ALL_STOPPED_OR_REACHED:
		{
			for(uint8_t i = 0; i < MAIN_CONTROL_NUM_MOTORS; ++i){
				if(control->motors[i].state.flags_a.motor_stopped && control->motors[i].state.flags_a.stopped_due_to_contact){
					control->grasp_executor.contact_detected[i] = 1;
				}
			}
			activate_following_grasp(fsm, control);
			break;
		}
		
		//New grasping trigger
		case MAIN_CONTROL_EVT_SIG_NEW_INPUT:
		{
			if(control->grasp_executor.initial_grasp_index != control->active_input->grasp_index){
				simpleStateMachineTran_(fsm, state_stop, user_pointer);
				simpleStateMachineDispatch_(fsm, event, user_pointer);
				break;
			}
			for(uint8_t i = 0; i < MAIN_CONTROL_NUM_MOTORS; ++i){
				if(control->grasp_executor.active_grasp->speed_factors[i] == 0){
					continue;
				}
				const main_control_motor_t* const motor = &control->motors[i];
				motor_i2c_send_setpoint(motor, MOTOR_PROTOCOL_WRITE_INPUT_VELOCITY, get_motor_velocity_limit(control, i));
			}
			break;
		}
		case MAIN_CONTROL_EVT_SIG_ERROR:
		{
			simpleStateMachineTran_(fsm, state_stop, user_pointer);
			break;
		}
		case MAIN_CONTROL_EVT_SIG_LOOP:
		case EXIT_SIG:
		default:
		break;
	}
}

/** 
	@brief 
	@param[in] None
	@param[out] None
*/
static void state_post_contact(volatile simple_state_machine_t *fsm, simple_state_machine_event_t const *event, volatile void * user_pointer)
{
	main_control_t* const control = (main_control_t*) user_pointer;
	ASSERT(control->grasp_executor.active_grasp != NULL);
	switch(event->signal_)
	{
		case ENTRY_SIG:
		{
			control->grasp_executor.state = CONTROL_GRASP_EXEC_STATE_POST;
			control->grasp_executor.current_current = 0.001;
			for(uint8_t i = 0; i < MAIN_CONTROL_NUM_MOTORS; ++i){
				const main_control_motor_t* motor = &control->motors[i];
				if(control->grasp_executor.contact_detected[i] == 0 || control->grasp_executor.active_grasp->speed_factors[i] == 0){
					motor_i2c_send_control_mode_stop(motor);
				}
				else {
					motor_i2c_send_closing_current_setpoint(motor, 0);
					motor_i2c_send_control_mode(&control->motors[i], MOTOR_CM_CURRENT, 0, 1);
				}
			}
			break;
		}
		case MAIN_CONTROL_EVT_SIG_NEW_INPUT:
		{
			if(control->grasp_executor.initial_grasp_index != control->active_input->grasp_index){
				simpleStateMachineTran_(fsm, state_stop, user_pointer);
				simpleStateMachineDispatch_(fsm, event, user_pointer);
			}
			break;
		}
		case MAIN_CONTROL_EVT_SIG_LOOP:{
			// Update rate of 100 Hz -> 1 A/s increase at full speed
			control->grasp_executor.current_current += get_overall_speed_factor(control) / 100;
			if(control->grasp_executor.current_current > get_overall_current_limit(control)){
				simpleStateMachineTran_(fsm, state_stop, user_pointer);
			}
			for(uint8_t i = 0; i < MAIN_CONTROL_NUM_MOTORS; ++i){
				const main_control_motor_t* motor = &control->motors[i];
				if(control->grasp_executor.contact_detected[i] == 0 || control->grasp_executor.active_grasp->speed_factors[i] == 0){
					continue;
				}
				motor_i2c_send_closing_current_setpoint(motor, get_motor_current_limit(control, i, control->grasp_executor.current_current));
			}
			break;
		}
		case MAIN_CONTROL_EVT_SIG_ERROR:
		{
			simpleStateMachineTran_(fsm, state_stop, user_pointer);
			break;
		}
		case MAIN_CONTROL_EVT_SIG_ALL_STOPPED_OR_REACHED:{
			// All fingers moved to the end of the movement range
			simpleStateMachineTran_(fsm, state_stop, user_pointer);
			break;
		}
		case EXIT_SIG:
		default:
		break;
	}
}

/** 
	@brief 
	@param[in] None
	@param[out] None
*/
static float32_t get_overall_speed_factor(const main_control_t* control){
	const uint8_t speed = control->active_input->grasp_speed_factor;
	// Convert Speed value into factor within [ 0 ; 1 ]
	// Speed 0									maps to 0
	// Speed 1									maps to min speed value (dynamically calculated to ensure slowest motor is turning with 500 [rad/s]
	// Speed ...								linearly interpolate
	// Speed (MAIN_CONTROL_SPEED_NUM_STEPS - 1)	maps to 1
	if(speed == 0){
		return 0;
	}
	if(speed >= MAIN_CONTROL_SPEED_FACTOR_NUM_STEPS){
		return  MAIN_CONTROL_VELOCITY_LIMIT;
	}
	return g_main_control.grasp_executor.min_velocity_factor + (1.f - g_main_control.grasp_executor.min_velocity_factor) * ((speed - 1.f) / (MAIN_CONTROL_SPEED_FACTOR_NUM_STEPS - 2.f));
}

/** 
	@brief 
	@param[in] None
	@param[out] None
*/
static float32_t get_motor_velocity_limit(const main_control_t* control, uint8_t motor_index){
	#if MAIN_CONTROL_ADAPT_VELOCITY_BASED_ON_GEAR
	const float32_t gear_factor = control->motors[motor_index].gear_ratio / control->grasp_executor.highest_gear_ratio;
	return gear_factor *  control->grasp_executor.active_grasp->speed_factors[motor_index] * get_overall_speed_factor(control)  * MAIN_CONTROL_VELOCITY_LIMIT;
	#else
	return control->grasp_executor.active_grasp->speed_factors[motor_index] * get_overall_speed_factor(control)  * MAIN_CONTROL_VELOCITY_LIMIT;
	#endif
}

/** 
	@brief 
	@param[in] None
	@param[out] None
*/
static float32_t get_overall_current_limit(const main_control_t* control){
	const uint8_t current = control->active_input->grasp_contact_current_factor;
	// Convert Speed value into factor within [ 0 ; 1 ]
	// Current 0									maps to 0
	// Current ...									linearly interpolate
	// Current (MAIN_CONTROL_SPEED_NUM_STEPS - 1)	maps to 1
	if(current == 0){
		return 0;
	}
	if(current >= MAIN_CONTROL_CURRENT_FACTOR_NUM_STEPS){
		return MAIN_CONTROL_CURRENT_LIMIT_CURRENT_CONTROL;
	}
	return ((current - 1.f) / (MAIN_CONTROL_CURRENT_FACTOR_NUM_STEPS - 2.f)) * MAIN_CONTROL_CURRENT_LIMIT_CURRENT_CONTROL;
}

/** 
	@brief 
	@param[in] None
	@param[out] None
*/
static float32_t get_motor_current_limit(const main_control_t* control, uint8_t motor_index, float32_t current_limit){
	#if MAIN_CONTROL_ADAPT_CURRENT_BASED_ON_GEAR
	return current_limit * (control->grasp_executor.highest_gear_ratio / control->motors[motor_index].gear_ratio);
	#else
	return current_limit;
	#endif
}

//Erwan: This function get the target angle for the control of a motor.(Position mode Control)
//input: main control struct, mototr index
//Output: angle in Percent
static float32_t get_target_angle(const main_control_t* control, uint8_t motor_index){
	const main_control_motor_t* motor = &control->motors[motor_index];
	return main_control_convert_percentage_to_position(motor, control->grasp_executor.active_grasp->target_position[motor_index]);
}

//Erwan: This function manages the grasping after the motors reached the desired position. Push after contact is executed in current mode.

/** 
	@brief 
	@param[in] None
	@param[out] None
*/
static void activate_following_grasp(volatile simple_state_machine_t *fsm, main_control_t* control){
	if(control->active_input->grasp_no_auto_continue){
		simpleStateMachineTran_(fsm, state_stop, control);
	}
	if(control->grasp_executor.active_grasp->following_grasp != NULL){
		control->grasp_executor.active_grasp = control->grasp_executor.active_grasp->following_grasp;
		simpleStateMachineTran_(fsm, state_pre_contact, control);
		return;
	}
	if(control->grasp_executor.active_grasp->flags.push_after_contact){
		simpleStateMachineTran_(fsm, state_post_contact, control);
		return;
	}
	simpleStateMachineTran_(fsm, state_stop, control);	
}