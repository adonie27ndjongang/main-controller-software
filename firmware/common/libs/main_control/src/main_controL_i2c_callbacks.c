#include "main_control_impl.h"
#include "motor_protocol.h"
#include "job_system.h"

#include <string.h>
#include <stdio.h>

#if MAIN_CONTROL_EMG_AVAILABLE
static void emg_failed();
#endif
static void motor_failed();

#if MAIN_CONTROL_EMG_AVAILABLE
void main_control_emg_i2c_callback(bool success, const sam5_i2c_master_com_job_t* job)
{
	if(!success){
		emg_failed();
		return;
	}
	// Convert EMG data to internal data:
	
	uint8_t speed;
	switch(job->buffer[1]){
		case 0: speed = 0; break;
		case 1: speed = (MAIN_CONTROL_SPEED_FACTOR_NUM_STEPS - 1) * (1.f / 3.f); break;
		case 2: speed = (MAIN_CONTROL_SPEED_FACTOR_NUM_STEPS - 1) * (2.f / 3.f); break;
		default: speed = MAIN_CONTROL_SPEED_FACTOR_NUM_STEPS - 1; break;
	}
	main_control_set_grasp(CONTROL_INPUT_TYPE_EMG, job->buffer[0], speed);
}
#endif

void main_control_motor_i2c_callback_simple_success(bool success, const sam5_i2c_master_com_job_t* job)
{
	if(main_control_control_flags.run_motor_controllers == 0){
		return;
	}
	if(!success){
		motor_failed();
		return;
	}
}

void main_control_motor_i2c_callback_activate_motor(bool success, const sam5_i2c_master_com_job_t* job)
{
	if(main_control_control_flags.run_motor_controllers == 0){
		return;
	}
	if(!success){
		motor_failed();
		return;
	}
	for(uint8_t i = 0; i < MAIN_CONTROL_NUM_MOTORS; ++i){
		main_control_motor_t* motor = &g_main_control.motors[i];
		if(job->slave_address == motor->i2c_address && MOTOR_PROTOCOL_GET_MOTR_INDEX(job->command) == motor->motor_index){
			motor->flags_a.motor_stopped = 0;
			motor->flags_a.target_angle_reached = 0;
			g_main_control.flags.all_stopped_or_reached = 0;
		}
	}
}

void main_control_motor_i2c_callback_read_combined_state_flags(bool success, const sam5_i2c_master_com_job_t* job)
{
	ASSERT(job->command == MOTOR_PROTOCOL_COMMON_READ_COMBINED_STATE_FLAGS);
	if(main_control_control_flags.run_motor_controllers == 0){
		return;
	}
	if(!success){
		motor_failed();
		return;
	}
	
	const dc_motor_controller_combined_state_flags_t* new_flags = (dc_motor_controller_combined_state_flags_t*) &job->buffer;
	const uint8_t motor_controller_index = job->slave_address - MOTOR_PROTOCOL_BASE_ADDRESS;
	ASSERT(motor_controller_index < MAIN_CONTROL_NUM_MOTOR_CONTROLLER);
	
	bool all_stopped_or_reached = true;
	
	for(uint8_t i = 0; i < MAIN_CONTROL_NUM_MOTORS; ++i){
		main_control_motor_t* const motor = &g_main_control.motors[i];
		if(motor->controller_index == motor_controller_index){
			motor->flags_a = new_flags->flags_a[motor->motor_index];
			motor->flags_b = new_flags->flags_b[motor->motor_index];
		}
		all_stopped_or_reached = all_stopped_or_reached && (motor->flags_a.motor_stopped || motor->flags_a.target_angle_reached);
	}
	if(g_main_control.flags.all_stopped_or_reached == false && all_stopped_or_reached == true){
		g_main_control.flags.all_stopped_or_reached = all_stopped_or_reached;
		simple_state_machine_event_t event = {.signal_ = MAIN_CONTROL_EVT_SIG_ALL_STOPPED_OR_REACHED};
		simpleStateMachineDispatch_(&g_main_control.state_machine, &event, &g_main_control);
	}
	else{
		g_main_control.flags.all_stopped_or_reached = all_stopped_or_reached;
	}
}

void main_control_motor_i2c_callback_read_combined_state(bool success, const sam5_i2c_master_com_job_t* job){
	ASSERT(job->command == MOTOR_PROTOCOL_COMMON_READ_COMBINED_STATE);
	if(main_control_control_flags.run_motor_controllers == 0){
		return;
	}
	if(!success){
		motor_failed();
		return;
	}
	const uint8_t motor_controller_index = job->slave_address - MOTOR_PROTOCOL_BASE_ADDRESS;
	const dc_motor_controller_combined_state_t* new_state = (const dc_motor_controller_combined_state_t*) job->buffer;
	for(uint8_t i = 0; i < MAIN_CONTROL_NUM_MOTORS; ++i){
		main_control_motor_t* const motor = &g_main_control.motors[i];
		if(motor->controller_index == motor_controller_index){
			motor->angle = new_state->angle[motor->motor_index];
			motor->current = new_state->current[motor->motor_index];
		}
	}
}

void main_control_motor_i2c_callback_read_limits(bool success, const sam5_i2c_master_com_job_t* job)
{
	ASSERT(MOTOR_PROTOCOL_GET_COMMAND_TYPE(job->command) == MOTOR_PROTOCOL_READ_LIMITS);
	if(main_control_control_flags.run_motor_controllers == 0){
		return;
	}
	if(!success){
		motor_failed();
		return;
	}
	for(uint8_t i = 0; i < MAIN_CONTROL_NUM_MOTORS; ++i){
		main_control_motor_t* const motor = &g_main_control.motors[i];
		if(motor->i2c_address == job->slave_address && motor->motor_index == MOTOR_PROTOCOL_GET_MOTR_INDEX(job->command)){
			CRITICAL_SECTION_ENTER()
			memcpy(&motor->limits, job->buffer, sizeof(dc_motor_controller_limits_t));
			CRITICAL_SECTION_LEAVE()
			return;
		}
	}
}

#if MAIN_CONTROL_EMG_AVAILABLE
static void emg_failed(){
	printf("I2C Error: EMG\r\n");
	if(g_main_control.active_input == CONTROL_INPUT_TYPE_EMG){
		simple_state_machine_event_t event = {.signal_=MAIN_CONTROL_EVT_SIG_I2C_ERROR};
		simpleStateMachineDispatch_(&g_main_control.state_machine, &event, &g_main_control);
	}
	request_reset_emg();
}
#endif

static void motor_failed(){
	printf("I2C Error: Motor\r\n");
	simple_state_machine_event_t event = {.signal_=MAIN_CONTROL_EVT_SIG_I2C_ERROR};
	simpleStateMachineDispatch_(&g_main_control.state_machine, &event, &g_main_control);
	request_reset_motors();
}
