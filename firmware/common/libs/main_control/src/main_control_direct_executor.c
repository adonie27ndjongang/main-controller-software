#include "main_control_impl.h"
#include <string.h>

static void start(main_control_t* control);
static void execute(simple_state_machine_event_t const * event, struct main_control_t* control);

main_control_executor_t direct_executor = {
	.start = &start,
	.execute = &execute,
	.stop = NULL,
};

static void start(main_control_t* control){
	for(uint8_t i = 0; i < MAIN_CONTROL_NUM_MOTORS; ++i){
		motor_i2c_send_control_mode_stop(&control->motors[i]);
	}
}

static void execute(simple_state_machine_event_t const * event, struct main_control_t* control){
	if(event->signal_ == MAIN_CONTROL_EVT_SIG_NEW_INPUT){
		for(uint8_t i = 0; i < MAIN_CONTROL_NUM_MOTORS; ++i){
			if(control->active_input->direct_finger_index == i){
				dc_motor_controller_inputs_t input_copy;
				memcpy(&input_copy, &control->active_input->direct_input, sizeof(dc_motor_controller_inputs_t));
				input_copy.angle = main_control_convert_percentage_to_position(&control->motors[i], input_copy.angle);
				input_copy.flags.use_endstop_for_end = MAIN_CONTROL_USE_ENDSTOP_FOR_END;
				input_copy.flags.use_endstop_for_refine = MAIN_CONTROL_USE_ENDSTOP_FOR_REFINE;
				motor_i2c_send_complete_input(&control->motors[i], &input_copy);
			}
		}	
	}
}