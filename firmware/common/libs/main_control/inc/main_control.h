/*
* main_control.h
*
* Created: 27.01.2023 19:00:38
*  Author: Michael Ratzel
*/


#ifndef MAIN_CONTROL_H_
#define MAIN_CONTROL_H_

#include <atmel_start.h>
#include <arm_neon.h>
#include "simple_state_machine.h"
#include "sam5_bmp3_wrapper.h"
#include "hardware_defines.h"
#include "motor_protocol.h"
#include "sam5_i2c_master.h"

#include "main_control_configuration.h"
#include "main_control_motor_i2c_callbacks.h"

struct main_control_t;
struct main_control_grasp_t;
/**
* \brief Definition of the events that can be dispatched to the I2C master state machine
*/
enum{
	MAIN_CONTROL_EVT_SIG_LOOP = EVENT_BASE,
	MAIN_CONTROL_EVT_SIG_NEW_INPUT,
	MAIN_CONTROL_EVT_SIG_ALL_STOPPED_OR_REACHED,
	MAIN_CONTROL_EVT_SIG_ERROR,
};

typedef struct {
	const uint8_t i2c_address;
	const uint8_t controller_index;
	const uint8_t motor_index;
	const uint8_t calibration_group;
	const uint8_t open_at_positive;
	const float32_t gear_ratio;
	
	// Take care not all of the data is always up to date
	dc_motor_controller_state_t state;
	dc_motor_controller_limits_t limits;
} main_control_motor_t;

typedef enum {
	CONTROL_INPUT_SOURCE_CONSOLE,
	CONTROL_INPUT_SOURCE_USART,
	CONTROL_INPUT_SOURCE_EMG,
	CONTROL_NUM_INPUT_SOURCES,	// This shall always be the last line to automatically assign the number of INPUT_SOURCES to it
} main_control_input_source_e;

typedef enum {
	CONTROL_INPUT_TYPE_STOP,
	CONTROL_INPUT_TYPE_GRASP,
	CONTROL_INPUT_TYPE_DIRECT,
	CONTROL_INPUT_TYPE_CALIB,
} main_control_input_type_e;

typedef enum {
	CONTROL_INDEX_THUMB = 0,
	CONTROL_INDEX_INDEX = 1,
	CONTROL_INDEX_MIDDLE = 2,
	CONTROL_INDEX_RING = 3,
	CONTROL_INDEX_PINKY = 4,
	CONTROL_INDEX_MISC = 5,
} main_control_finger_e;

typedef struct {
	main_control_input_type_e type;
	
	// Data for Input Type Grasp
	uint8_t grasp_index;			// 0xFF -> Stop, other are hard coded grasps
	uint8_t grasp_speed_factor;		// [0 - 100]
	uint8_t grasp_contact_current_factor;	// [0 - 100]
	uint8_t grasp_no_auto_continue;	// If 1 the hand will not move automatically to the following grasp
	
	// Data for Input Type Direct Ctrl
	uint8_t direct_finger_index;
	dc_motor_controller_inputs_t direct_input; // Note: Angle is given as a percentage between 0 and 1 (0 is open, 1 is closed) and NOT in radians.
	
} main_control_input_t;

typedef struct {
	void (*start)(struct main_control_t*);
	void (*execute)(simple_state_machine_event_t const *, struct main_control_t*);
	void (*stop)(struct main_control_t*);
} main_control_executor_t;

typedef enum {
	CONTROL_GRASP_EXEC_STATE_INACTIVE,
	CONTROL_GRASP_EXEC_STATE_STOP,
	CONTROL_GRASP_EXEC_STATE_PRE,
	CONTROL_GRASP_EXEC_STATE_POST,
} main_control_grasp_executer_state_e;

typedef enum {
	CONTROL_CALIB_EXEC_STATE_INACTIVE,
	CONTROL_CALIB_EXEC_STATE_WAIT_BATVLT,
	CONTROL_CALIB_EXEC_STATE_PREPARE,
	CONTROL_CALIB_EXEC_STATE_GROUPS,
	CONTROL_CALIB_EXEC_STATE_FINISHED,
} main_control_calib_executer_state_e;

typedef struct {
	uint32_t EMG_grasping_type;
	uint32_t EMG_grasping_type_old;
	float32_t EMG_speed_factor;
	uint8_t spare : 6;
} EMG_inputs;

typedef struct main_control_grasp_t {
	const float32_t speed_factors[6];							// 1 -> Full speed for this joing, 0 -> do not move this joint at all
	const float32_t target_position[6];							// 1 -> Fully closed, 0 -> Fully opened
	const struct main_control_grasp_t const * following_grasp;	// After this grasp: If null switch to pushing or stop, else switch to following_grasp
	const char name[31];										// A name that can be displayed on the console
	const struct{
		uint8_t push_after_contact : 1;							// Only relevant if following_grasp == NULL
		uint8_t use_aggressive_contact_detection : 1;
	} flags;
	
} main_control_grasp_t;
#define GRASP_FOLLOWED_BY_STOP		(main_control_grasp_t*) 0
#define GRASP_FOLLOWED_BY_PUSHING	(main_control_grasp_t*) 1

typedef struct main_control_t {
	main_control_motor_t motors[MAIN_CONTROL_NUM_MOTORS];
	main_control_input_t inputs[CONTROL_NUM_INPUT_SOURCES];
	main_control_input_t* active_input;
	const main_control_executor_t* active_executor;
	
	struct {
		uint8_t accept_reset_requests : 1;
		uint8_t motor_reset_requested : 1;
		uint8_t all_stopped_or_reached : 1;
		uint8_t doing_startup : 1;
		//Erwan: set it to zero to deactivate the calibration
		uint8_t doing_reset : 1;
		uint8_t calib_executor_running : 1;
		uint8_t grasp_executor_running : 1;
		uint8_t calibration_finished : 1;
	} flags;
	uint8_t error_counter;
	uint32_t periodic_counter;
	uint32_t no_reset_timestamp;
	
	struct {
		simple_state_machine_t state_machine;
		main_control_grasp_executer_state_e state;
		const main_control_grasp_t* active_grasp;
		
		float32_t min_velocity_factor;
		float32_t highest_gear_ratio;
		float32_t current_current;
		
		uint8_t contact_detected[MAIN_CONTROL_NUM_MOTORS];
		uint8_t initial_grasp_index;
	} grasp_executor;
	
	struct {
		simple_state_machine_t state_machine;
		main_control_calib_executer_state_e state;
		uint8_t current_calib_group;
	} calib_executor;
	
} main_control_t;

extern main_control_t g_main_control;
extern const uint32_t main_control_num_grasps;
extern control_flags_t main_control_control_flags;

const main_control_grasp_t* main_control_get_grasp(uint8_t grasp_index);

void main_control_init();

void main_control_heartbeat();
void main_control_loop();
void main_control_loop_low_priority();

void main_control_set_active_input(main_control_input_source_e new_active_input);
void main_control_set_input(main_control_input_source_e source, main_control_input_t* new_input);

float32_t main_control_convert_percentage_to_position(const main_control_motor_t* motor, float32_t percentage);
float32_t main_control_convert_position_to_percentage(const main_control_motor_t* motor, float32_t position);
void checkEMGinput();

#endif /* main_CONTROL_H_ */