/*
* sam5_ws2812.h
*
* Created: 11.01.2023
*  Author: Michael Ratzel
*/
#ifndef LIGHT_WS2812_H_
#define LIGHT_WS2812_H_


#include <atmel_start.h>

void sam5_ws2812_init(uint32_t pin);

///////////////////////////////////////////////////////////////////////
// Main function call
//
// Call with address to led color array (order is Green-Red-Blue)
// Numer of bytes to be transmitted is leds*3
// Make sure to wait at least 50us between each function call!!
///////////////////////////////////////////////////////////////////////
void sam5_ws2812_sendarray(const uint8_t *ledarray, int length, uint32_t pin);

#endif /* LIGHT_WS2812_H_ */
